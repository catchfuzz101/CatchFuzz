import sys
import os
import commands
import re
import random

def add_slash(string):
	new_string=""
	for i in string:
		if i in ["*", "[","]","\"","\""]:
			new_string+="\\"
		new_string+=i		
	return new_string

if(len(sys.argv)!=2):
    print("[*] Usage: python stage1.py bison")	
    print("[*] find main code!")
    exit()

filelist=os.listdir("./program/"+sys.argv[1]+"/src")
main_file_name=[]
found_main=False
for i in filelist:
	os.system("ctags ./program/"+sys.argv[1]+"/src/"+i)
	f=open("./tags",'r')
	while(1):
		tag_data=f.readline()
		if tag_data=="":
			f.close()
			break
		elif tag_data.startswith("main\t"):
			main_data=tag_data.split("\t")[2]
			if main_data.startswith("/^") and main_data.endswith("$/;\""):
				main_data=add_slash(main_data[1:].split("$/;\"")[0])
				found_main=True
				main_file=i
				main_file_name.append("./program/"+sys.argv[1]+"/src/"+i)
				f.close()
				break
if(found_main==False):
	print("ERROR! no main!\n")
	exit(-1)

os.unlink("./tags")
for i in main_file_name:
    print("[+] Found main file: "+i)
