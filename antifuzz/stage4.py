import sys
import os

if(len(sys.argv)!=2):
	print("[+] Usage: python stage4.py bison-3.3")
	exit()


filelist=os.listdir("./program/"+sys.argv[1]+"/antifuzz/")
error=[]
index=0
for i in filelist:
	f1=open("./program/"+sys.argv[1]+"/antifuzz/"+i,'r')
	f2=open("./program/"+sys.argv[1]+"/antifuzz/new_"+i,'w')
	data=""
	error_tmp=[]
	while(1):
		line=f1.readline()
		if line=="":
			f1.close()
			f2.write(data)
			f2.close()
			os.system("cp ./program/"+sys.argv[1]+"/antifuzz/new_"+i+" ./program/"+sys.argv[1]+"/antifuzz/"+i)
			os.system("rm ./program/"+sys.argv[1]+"/antifuzz/new_"+i)
			break
		elif line.strip().startswith("assert ("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"assert ("+line.strip().split("assert (")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		elif line.strip().startswith("assert("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"assert("+line.strip().split("assert(")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		
		elif line.strip().startswith("exit ("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"exit ("+line.strip().split("exit (")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		
		elif line.strip().startswith("exit("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"exit("+line.strip().split("exit(")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		
		elif line.strip().startswith("abort ("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"abort ("+line.strip().split("abort (")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		
		elif line.strip().startswith("abort("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"abort("+line.strip().split("abort(")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		
		elif line.strip().startswith("atexit ("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"atexit ("+line.strip().split("atexit (")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		
		elif line.strip().startswith("atexit("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"atexit("+line.strip().split("atexit(")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))

		elif line.strip().startswith("error ("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"error ("+line.strip().split("error (")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		
		elif line.strip().startswith("error("):
			index+=1
			tmp=line
			line="{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+"error("+line.strip().split("error(")[1]+"}\n"
			if not tmp.strip().endswith(";"):
				error.append((i,tmp,line))
		data+=line

print("[*] Result: Succeed in patching {0} points".format(index))
if(index!=0):
    print("*"*90)
    print("Needs Manual Editing!")
    print("*"*90)
    for i in error:
	    print("-"*30)
	    print("[*] File: ./program/"+sys.argv[1]+"/antifuzz/"+i[0])
	    print("[-] Before: "+i[1])
	    print("[+] After: "+i[2])
