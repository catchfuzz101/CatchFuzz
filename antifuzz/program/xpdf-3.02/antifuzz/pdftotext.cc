//========================================================================
//
// pdftotext.cc
//
// Copyright 1997-2003 Glyph & Cog, LLC
//
//========================================================================

#include <unistd.h>
#include <math.h>

#include <aconf.h>
unsigned char *start_addr;
unsigned char *end_addr;
int antifuzz_check=0;
int canary_xor;
int fake_arr[131]={0,309,74,74,208,427,580,39,460,269,88,585,975,998,990,994,908,440,437,801,69,13,502,527,536,990,636,584,925,50,77,485,440,975,741,266,96,908,135,612,550,219,342,959,219,342,476,69,56,96,107,273,427,1000,269,881,959,925,899,204,652,899,754,460,602,62,1000,247,658,602,273,741,801,88,50,208,371,550,421,120,998,536,881,962,266,56,485,658,62,476,247,284,576,580,13,739,205,279,284,636,962,576,527,1,1,966,39,248,966,107,204,502,248,205,994,754,739,312,585,77,120,421,584,371,612,437,309,135,279,652,312};
#include <stdio.h>
#include <stdint.h>
#include <regex.h>
#include <string.h>


#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "parseargs.h"
#include "GString.h"
#include "gmem.h"
#include "GlobalParams.h"
#include "Object.h"
#include "Stream.h"
#include "Array.h"
#include "Dict.h"
#include "XRef.h"
#include "Catalog.h"
#include "Page.h"
#include "PDFDoc.h"
#include "TextOutputDev.h"
#include "CharTypes.h"
#include "UnicodeMap.h"
#include "Error.h"
#include "config.h"

static void printInfoString(FILE *f, Dict *infoDict, char *key,
			    char *text1, char *text2, UnicodeMap *uMap);
static void printInfoDate(FILE *f, Dict *infoDict, char *key, char *fmt);

static int firstPage = 1;
static int lastPage = 0;
static GBool physLayout = gFalse;
static GBool rawOrder = gFalse;
static GBool htmlMeta = gFalse;
static char textEncName[128] = "";
static char textEOL[16] = "";
static GBool noPageBreaks = gFalse;
static char ownerPassword[33] = "\001";
static char userPassword[33] = "\001";
static GBool quiet = gFalse;
static char cfgFileName[256] = "";
static GBool printVersion = gFalse;
static GBool printHelp = gFalse;

static ArgDesc argDesc[] = {
  {"-f",       argInt,      &firstPage,     0,
   "first page to convert"},
  {"-l",       argInt,      &lastPage,      0,
   "last page to convert"},
  {"-layout",  argFlag,     &physLayout,    0,
   "maintain original physical layout"},
  {"-raw",     argFlag,     &rawOrder,      0,
   "keep strings in content stream order"},
  {"-htmlmeta", argFlag,   &htmlMeta,       0,
   "generate a simple HTML file, including the meta information"},
  {"-enc",     argString,   textEncName,    sizeof(textEncName),
   "output text encoding name"},
  {"-eol",     argString,   textEOL,        sizeof(textEOL),
   "output end-of-line convention (unix, dos, or mac)"},
  {"-nopgbrk", argFlag,     &noPageBreaks,  0,
   "don't insert page breaks between pages"},
  {"-opw",     argString,   ownerPassword,  sizeof(ownerPassword),
   "owner password (for encrypted files)"},
  {"-upw",     argString,   userPassword,   sizeof(userPassword),
   "user password (for encrypted files)"},
  {"-q",       argFlag,     &quiet,         0,
   "don't print any messages or errors"},
  {"-cfg",     argString,   cfgFileName,    sizeof(cfgFileName),
   "configuration file to use in place of .xpdfrc"},
  {"-v",       argFlag,     &printVersion,  0,
   "print copyright and version info"},
  {"-h",       argFlag,     &printHelp,     0,
   "print usage information"},
  {"-help",    argFlag,     &printHelp,     0,
   "print usage information"},
  {"--help",   argFlag,     &printHelp,     0,
   "print usage information"},
  {"-?",       argFlag,     &printHelp,     0,
   "print usage information"},
  {NULL}
};

int w0(unsigned char* buf, unsigned int len){
	printf("w0 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 0 % len;
    int index2 = 1 % len;
    int index3 = 2 % len;
    int index4 = 3 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3902515186) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1507252891) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 603338905) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1696904468) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1065828383) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3048701182) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1392146492) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3902515186) {
        ret = 12;
      }
    }
    if(buf[index1] > 56) {
      if(buf[index2] < 118) {
        if(buf[index3] - buf[index4] < 56) {
          if(buf[index3] + buf[index4] > 118) {
            if(((buf[index1] + buf[index2]) % 9) == 0) {
              if(((buf[index1] * buf[index2]) % 5) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 56) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }

  return ret;
}

int w1(unsigned char* buf, unsigned int len){
  printf("w1 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 1 % len;
    int index2 = 2 % len;
    int index3 = 3 % len;
    int index4 = 4 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3649224605) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2350045607) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3014389030) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2122141901) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1860222409) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2127411081) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3096905285) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3649224605) {
        ret = 12;
      }
    }
    if(buf[index1] > 209) {
      if(buf[index2] < 215) {
        if(buf[index3] - buf[index4] < 209) {
          if(buf[index3] + buf[index4] > 215) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 209) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w2(unsigned char* buf, unsigned int len){
	printf("w2 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 2 % len;
    int index2 = 3 % len;
    int index3 = 4 % len;
    int index4 = 5 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3060206730) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1823329745) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1798358539) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1130645893) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 334868504) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1095408358) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2855184194) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3060206730) {
        ret = 12;
      }
    }
    if(buf[index1] > 236) {
      if(buf[index2] < 125) {
        if(buf[index3] - buf[index4] < 236) {
          if(buf[index3] + buf[index4] > 125) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  64) {
                  if((buf[index1] ^ buf[index2]) == 236) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w3(unsigned char* buf, unsigned int len){
	  printf("w3 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 3 % len;
    int index2 = 4 % len;
    int index3 = 5 % len;
    int index4 = 6 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1306562184) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2309250971) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 702642080) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3193801888) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1618453275) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3908944833) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1426801106) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1306562184) {
        ret = 12;
      }
    }
    if(buf[index1] > 33) {
      if(buf[index2] < 13) {
        if(buf[index3] - buf[index4] < 33) {
          if(buf[index3] + buf[index4] > 13) {
            if(((buf[index1] + buf[index2]) % 6) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 33) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w4(unsigned char* buf, unsigned int len){
	  printf("w4 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 4 % len;
    int index2 = 5 % len;
    int index3 = 6 % len;
    int index4 = 7 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1317201472) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 4182791665) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1712271078) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3769200889) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3974150540) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1676806712) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 503006531) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1317201472) {
        ret = 12;
      }
    }
    if(buf[index1] > 225) {
      if(buf[index2] < 134) {
        if(buf[index3] - buf[index4] < 225) {
          if(buf[index3] + buf[index4] > 134) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 225) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w5(unsigned char* buf, unsigned int len){
	  printf("w5 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 5 % len;
    int index2 = 6 % len;
    int index3 = 7 % len;
    int index4 = 8 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 2680581394) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798508734) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1827993300) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 713768984) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4009010704) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1285272007) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4015010868) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 2680581394) {
        ret = 12;
      }
    }
    if(buf[index1] > 221) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 221) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 221) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w6(unsigned char* buf, unsigned int len){
	  printf("w6 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 6 % len;
    int index2 = 7 % len;
    int index3 = 8 % len;
    int index4 = 9 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1617115864) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1631690012) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3460463880) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3382779234) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1649578633) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 728322968) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2998716203) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1617115864) {
        ret = 12;
      }
    }
    if(buf[index1] > 132) {
      if(buf[index2] < 208) {
        if(buf[index3] - buf[index4] < 132) {
          if(buf[index3] + buf[index4] > 208) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  32) {
                  if((buf[index1] ^ buf[index2]) == 132) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w7(unsigned char* buf, unsigned int len){
	  printf("w7 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 7 % len;
    int index2 = 8 % len;
    int index3 = 9 % len;
    int index4 = 10 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1208351771) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2227323053) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1104939852) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4258576467) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 780873126) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2155013799) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1158125450) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1208351771) {
        ret = 12;
      }
    }
    if(buf[index1] > 18) {
      if(buf[index2] < 181) {
        if(buf[index3] - buf[index4] < 18) {
          if(buf[index3] + buf[index4] > 181) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  1) {
                  if((buf[index1] ^ buf[index2]) == 18) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w8(unsigned char* buf, unsigned int len){
	  printf("w8 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 8 % len;
    int index2 = 9 % len;
    int index3 = 10 % len;
    int index4 = 11 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 323927678) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798644434) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 2384399227) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 361926757) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 897612148) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 990255627) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3311835330) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 323927678) {
        ret = 12;
      }
    }
    if(buf[index1] > 100) {
      if(buf[index2] < 196) {
        if(buf[index3] - buf[index4] < 100) {
          if(buf[index3] + buf[index4] > 196) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 100) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w9(unsigned char* buf, unsigned int len){
	  printf("w9 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 9 % len;
    int index2 = 10 % len;
    int index3 = 11 % len;
    int index4 = 12 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 720174660) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2124116700) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3937385190) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1443256077) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1256522327) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1727523764) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2994088857) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 720174660) {
        ret = 12;
      }
    }
    if(buf[index1] > 249) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 249) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 8) == 0) {
                if((buf[index1] ^ buf[index2]) &  8) {
                  if((buf[index1] ^ buf[index2]) == 249) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}



typedef int (*obfuscation_call_heavyweight)(unsigned char* buf, unsigned int len);
obfuscation_call_heavyweight functions_array[10] = {
  
  w0,
  
  w1,
  
  w2,
  
  w3,
  
  w4,
  
  w5,
  
  w6,
  
  w7,
  
  w8,
  
  w9
};

/*https://github.com/francis-yli/kmeans_1d/blob/master/kmeans1D.c*/

float getDist(float p1, float p2) {
	float dist;
	dist = fabsf(p1 - p2); 
	return dist;
}

int getNearestDist(float array[], int p, int k) {
	float nearestDist = 300; 
	float dist; 
	int i;
	int label=-1;

	for (i = 0; i < k; i++) {
		dist = getDist(array[i], p);
		if (dist < nearestDist) {
			nearestDist = dist;
			label = i;
		}
	}
	return label;
}

float getMean(unsigned int vArray[], int lArray[], int label, int size) {
	float sum = 0.0;
	int counter = 0;
	float mean;
	int i = 0;

	for (; i < size; i++) {
		if (lArray[i] == label) {
			sum += vArray[i];
			counter++;
		}
	}

	if (counter != 0) {
		mean = sum / (float)counter;
	}
	else mean = -1;

	return mean;
}


void antifuzz(void){

	#define MAX_FILE_CONTENT_SIZE 512
	unsigned char* fileContentMult = (unsigned char*)"abcdefghijklmnopqrstuvwxyz";
firstPage=((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[1])/fake_arr[3])+fake_arr[5])-fake_arr[7])+fake_arr[9])/fake_arr[11])+fake_arr[13])*fake_arr[15])+fake_arr[17])*fake_arr[19])/fake_arr[21])/fake_arr[23])+fake_arr[25])-fake_arr[27])-fake_arr[29])-fake_arr[31])/fake_arr[33])-fake_arr[35])+fake_arr[37])-fake_arr[39])+fake_arr[41])*fake_arr[43])/fake_arr[45])/fake_arr[47])+fake_arr[49])/fake_arr[51])+fake_arr[53])-fake_arr[55])+fake_arr[57])/fake_arr[59])+fake_arr[61])/fake_arr[63]);
lastPage=((((((((((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[65])*fake_arr[67])-fake_arr[7])-fake_arr[69])*fake_arr[71])+fake_arr[73])*fake_arr[75])+fake_arr[77])*fake_arr[79])+fake_arr[81])-fake_arr[83])-fake_arr[73])+fake_arr[85])-fake_arr[87])*fake_arr[89])-fake_arr[91])*fake_arr[93])+fake_arr[15])-fake_arr[95])-fake_arr[97])*fake_arr[99])/fake_arr[101])*fake_arr[103])/fake_arr[105])+fake_arr[107])-fake_arr[109])/fake_arr[87])/fake_arr[111])+fake_arr[113])/fake_arr[63])/fake_arr[115])+fake_arr[117])/fake_arr[119])*fake_arr[121])/fake_arr[123])-fake_arr[125])/fake_arr[127])/fake_arr[129]);
	unsigned int x[0xFFFFF]={0x00,};
	unsigned int y[0xFFFFF]={0x00,};

	char pid_mem_file[50]={0x00,};
	char pid_mem_file_name[50]={0x00,};
	
  	pid_t pid=getpid();
  	sprintf(pid_mem_file, "/proc/%d/maps", pid);
	sprintf(pid_mem_file_name, "./maps_%d", pid);
  	int canary_index, canary_val;
  	while(1){
  		int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
  		if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
        	else{
            		fake_arr[0]=1;
           	 	break;
        	}
        	canary_val=rand()%1000;
		if(fake_arr[canary_index]!=canary_val){
			fake_arr[canary_index]=canary_val;
			break;
		}
  	}
  	FILE *fp1  = fopen(pid_mem_file, "r");
	FILE *fp2  = fopen(pid_mem_file_name, "w");
  
	char buffer[128];
	regex_t state;
  	regmatch_t pmatch[3];

	char addr1[18];
  	char addr2[18];
  	const char *pattern = "[rwx-]{3}+[s]";
  	const char *addr = "([0-9a-fA-F]{12,16})-([0-9a-fA-F]{12,16})";
  	strncpy(addr1,"0x",2);
  	strncpy(addr2,"0x",2);

  	memset(addr1+2, 0x00, sizeof(addr1)-2);
 	memset(addr2+2, 0x00, sizeof(addr2)-2);

	regcomp(&state, pattern, REG_EXTENDED);
  	int index;
  	while (fgets(buffer, 128, fp1) != NULL)
  	{	
      		fprintf(fp2, "%s", buffer);
		int status = regexec(&state, buffer, 1, pmatch, 0);
		if(status==0){
      			regfree(&state);
      			regcomp(&state, addr, REG_EXTENDED);
      			status = regexec(&state, buffer, 3, pmatch, 0);
      			if(status==0){		
        			strncpy(addr1+2,buffer+pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
        			strncpy(addr2+2,buffer+pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
		
        			start_addr=(unsigned char*)strtoll(addr1,NULL,16);
        			end_addr=(unsigned char*)strtoll(addr2,NULL,16);
			
				for (int i = 0; i < end_addr-start_addr; i++) {
					x[i]=i;
					y[i] = start_addr[i];
				}
    
				for(int i = 0; i <130; i++) {
		        		functions_array[0](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i <110; i++) {
		        		functions_array[1](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 90; i++) {
		        		functions_array[2](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 70; i++) {
		        		functions_array[3](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 50; i++) {
		        		functions_array[4](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}

		      		int k=0;
		      		for (int i = 0; i < end_addr-start_addr; i++){
					x[i]=i;
					int tmp_value=start_addr[i]-y[i];
					if(tmp_value>30 && tmp_value<150)y[k++]=tmp_value;
				}

		      		int labelArray[k];
		      		int orig_centerArray[5] = { 50,70,90,110,130 };
		      		float centerArray[5] = { 50,70,90,110,130 };
		      		float tmp_centerArray[5] = { 0, };
      
		     		int loop = 0;
				while(1) {
		        		loop++;
		        		for (int i = 0; i < k; i++) {
		          			int label = getNearestDist(centerArray, y[i], 5);
		          			labelArray[i] = label;
		       			}

		       			int finish = 0;

		       			for (int i = 0; i < 5; i++) {
		          			tmp_centerArray[i] = centerArray[i];
		          			centerArray[i] = getMean(y, labelArray, i, k);
		          			if (tmp_centerArray[i] == centerArray[i]) {
		            				finish += 1;
		          			}
		          
        				}
		       	 		if (finish == 5) {
		          			break;
		        		}
      				}

		      		float distance = 0;
		     	 	int tmp_index=5;
		      		for (int i = 0; i < tmp_index; i++) {
		        		if (centerArray[i] == -1)tmp_index -= 1;
		        		else distance+=(centerArray[i] - orig_centerArray[i])*(centerArray[i]-orig_centerArray[i]);
		      		}
		      		distance /=tmp_index;

				if(distance<10){
					antifuzz_check=1;
					for (int i = 0; i < end_addr-start_addr; i++) {
						start_addr[i]=0x80;
					}
				}
				break;
     			}
   		}
 	}
	regfree(&state);
	fclose(fp1);
	fclose(fp2);

       	unlink(pid_mem_file_name);

}

int main(int argc, char *argv[]) {
	antifuzz();
  PDFDoc *doc;
  GString *fileName;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
  GString *textFileName;
  GString *ownerPW, *userPW;
  TextOutputDev *textOut;
  FILE *f;
  UnicodeMap *uMap;
  Object info;
  GBool ok;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
  char *p;
  int exitCode;

  exitCode = 99;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

  // parse args
  ok = parseArgs(argDesc, &argc, argv);
  if (!ok || argc < 2 || argc > 3 || printVersion || printHelp) {
    fprintf(stderr, "pdftotext version %s\n", xpdfVersion);
    fprintf(stderr, "%s\n", xpdfCopyright);
    if (!printVersion) {
      printUsage("pdftotext", "<PDF-file> [<text-file>]", argDesc);
    }
    goto err0;
  }
  fileName = new GString(argv[1]);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

  // read config file
  globalParams = new GlobalParams(cfgFileName);
  if (textEncName[0]) {
    globalParams->setTextEncoding(textEncName);
  }
  if (textEOL[0]) {
    if (!globalParams->setTextEOL(textEOL)) {
      fprintf(stderr, "Bad '-eol' value on command line\n");
    }
  }
  if (noPageBreaks) {
    globalParams->setTextPageBreaks(gFalse);
  }
  if (quiet) {
    globalParams->setErrQuiet(quiet);
  }

  // get mapping to output encoding
  if (!(uMap = globalParams->getTextEncoding())) {
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(-1, "Couldn't get text encoding");}
    delete fileName;
    goto err1;
  }

  // open PDF file
  if (ownerPassword[0] != '\001') {
    ownerPW = new GString(ownerPassword);
  } else {
    ownerPW = NULL;
  }
  if (userPassword[0] != '\001') {
    userPW = new GString(userPassword);
  } else {
    userPW = NULL;
  }
  doc = new PDFDoc(fileName, ownerPW, userPW);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
  if (userPW) {
    delete userPW;
  }
  if (ownerPW) {
    delete ownerPW;
  }
  if (!doc->isOk()) {
    exitCode = 1;
    goto err2;
  }

  // check for copy permission
  if (!doc->okToCopy()) {
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(-1, "Copying of text from this document is not allowed.");}
    exitCode = 3;
    goto err2;
  }

  // construct text file name
  if (argc == 3) {
    textFileName = new GString(argv[2]);
  } else {
    p = fileName->getCString() + fileName->getLength() - 4;
    if (!strcmp(p, ".pdf") || !strcmp(p, ".PDF")) {
      textFileName = new GString(fileName->getCString(),
				 fileName->getLength() - 4);
    } else {
      textFileName = fileName->copy();
    }
    textFileName->append(htmlMeta ? ".html" : ".txt");
  }

  // get page range
  if (firstPage < 1) {
    firstPage = 1;
  }
  if (lastPage < 1 || lastPage > doc->getNumPages()) {
    lastPage = doc->getNumPages();
  }

  // write HTML header
  if (htmlMeta) {
    if (!textFileName->cmp("-")) {
      f = stdout;
    } else {
      if (!(f = fopen(textFileName->getCString(), "wb"))) {
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(-1, "Couldn't open text file '%s'", textFileName->getCString());}
	exitCode = 2;
	goto err3;
      }
    }
    fputs("<html>\n", f);
    fputs("<head>\n", f);
    doc->getDocInfo(&info);
    if (info.isDict()) {
      printInfoString(f, info.getDict(), "Title", "<title>", "</title>\n",
		      uMap);
      printInfoString(f, info.getDict(), "Subject",
		      "<meta name=\"Subject\" content=\"", "\">\n", uMap);
      printInfoString(f, info.getDict(), "Keywords",
		      "<meta name=\"Keywords\" content=\"", "\">\n", uMap);
      printInfoString(f, info.getDict(), "Author",
		      "<meta name=\"Author\" content=\"", "\">\n", uMap);
      printInfoString(f, info.getDict(), "Creator",
		      "<meta name=\"Creator\" content=\"", "\">\n", uMap);
      printInfoString(f, info.getDict(), "Producer",
		      "<meta name=\"Producer\" content=\"", "\">\n", uMap);
      printInfoDate(f, info.getDict(), "CreationDate",
		    "<meta name=\"CreationDate\" content=\"%s\">\n");
      printInfoDate(f, info.getDict(), "LastModifiedDate",
		    "<meta name=\"ModDate\" content=\"%s\">\n");
    }
    info.free();
    fputs("</head>\n", f);
    fputs("<body>\n", f);
    fputs("<pre>\n", f);
    if (f != stdout) {
      fclose(f);
    }
  }

  // write text file
  textOut = new TextOutputDev(textFileName->getCString(),
			      physLayout, rawOrder, htmlMeta);
  if (textOut->isOk()) {
    doc->displayPages(textOut, firstPage, lastPage, 72, 72, 0,
		      gFalse, gTrue, gFalse);
  } else {
    delete textOut;
    exitCode = 2;
    goto err3;
  }
  delete textOut;

  // write end of HTML file
  if (htmlMeta) {
    if (!textFileName->cmp("-")) {
      f = stdout;
    } else {
      if (!(f = fopen(textFileName->getCString(), "ab"))) {
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(-1, "Couldn't open text file '%s'", textFileName->getCString());}
	exitCode = 2;
	goto err3;
      }
    }
    fputs("</pre>\n", f);
    fputs("</body>\n", f);
    fputs("</html>\n", f);
    if (f != stdout) {
      fclose(f);
    }
  }

  exitCode = 0;

  // clean up
 err3:
  delete textFileName;
 err2:
  delete doc;
  uMap->decRefCnt();
 err1:
  delete globalParams;
 err0:

  // check for memory leaks
  Object::memCheck(stderr);
  gMemReport(stderr);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

  {if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return exitCode;}
}

static void printInfoString(FILE *f, Dict *infoDict, char *key,
			    char *text1, char *text2, UnicodeMap *uMap) {
  Object obj;
  GString *s1;
  GBool isUnicode;
  Unicode u;
  char buf[8];
  int i, n;

  if (infoDict->lookup(key, &obj)->isString()) {
    fputs(text1, f);
    s1 = obj.getString();
    if ((s1->getChar(0) & 0xff) == 0xfe &&
	(s1->getChar(1) & 0xff) == 0xff) {
      isUnicode = gTrue;
      i = 2;
    } else {
      isUnicode = gFalse;
      i = 0;
    }
    while (i < obj.getString()->getLength()) {
      if (isUnicode) {
	u = ((s1->getChar(i) & 0xff) << 8) |
	    (s1->getChar(i+1) & 0xff);
	i += 2;
      } else {
	u = s1->getChar(i) & 0xff;
	++i;
      }
      n = uMap->mapUnicode(u, buf, sizeof(buf));
      fwrite(buf, 1, n, f);
    }
    fputs(text2, f);
  }
  obj.free();
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
}

static void printInfoDate(FILE *f, Dict *infoDict, char *key, char *fmt) {
  Object obj;
  char *s;

  if (infoDict->lookup(key, &obj)->isString()) {
    s = obj.getString()->getCString();
    if (s[0] == 'D' && s[1] == ':') {
      s += 2;
    }
    fprintf(f, fmt, s);
  }
  obj.free();
}
