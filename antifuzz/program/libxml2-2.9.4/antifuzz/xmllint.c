/*
 * xmllint.c : a small tester program for XML input.
 *
 * See Copyright for the status of this software.
 *
 * daniel@veillard.com
 */

#include "libxml.h"

unsigned char *start_addr;
unsigned char *end_addr;
int antifuzz_check=0;
int canary_xor;
int fake_arr[1551]={0,615,643,857,296,18,711,348,862,65,81,905,244,534,463,576,566,605,951,874,130,27,177,191,459,707,385,121,223,36,60,825,752,631,963,120,777,989,401,2,814,198,503,637,755,712,134,746,685,498,351,710,183,706,883,693,451,19,880,324,899,139,779,251,989,647,287,783,878,439,610,754,583,326,704,130,824,63,781,558,271,998,26,472,116,441,558,906,133,836,426,648,821,185,358,598,554,319,929,227,77,704,40,571,9,588,973,636,379,896,126,988,370,672,927,680,542,967,943,790,72,233,702,841,772,824,193,953,128,583,182,380,106,760,101,844,783,870,742,413,836,433,637,351,288,937,958,737,264,876,4,209,705,31,148,448,1000,585,421,868,756,867,156,751,491,419,653,478,58,247,598,909,794,914,269,93,409,149,928,238,613,399,201,362,890,696,122,183,967,889,523,336,932,4,352,668,611,762,27,739,931,132,277,903,349,61,215,204,147,665,856,252,5,55,762,452,239,98,602,890,563,370,496,349,410,278,908,73,516,490,257,163,917,104,478,753,325,151,631,484,273,667,946,515,371,476,118,96,699,489,19,311,340,426,453,822,980,171,559,438,857,485,139,243,449,327,42,123,799,372,746,520,14,303,256,69,736,996,95,284,524,421,417,333,236,383,162,184,373,453,710,649,2,432,666,346,972,463,991,560,176,70,423,580,621,987,390,215,926,259,816,973,243,396,981,796,577,855,808,541,137,947,36,100,428,513,620,995,994,606,29,310,999,875,568,910,301,20,730,591,675,860,240,394,823,244,761,352,66,115,695,883,987,378,712,504,309,679,444,671,310,431,80,361,152,878,912,929,55,612,438,95,910,579,686,316,584,669,424,481,470,670,592,632,765,40,413,340,164,325,618,635,634,256,476,533,995,449,57,127,940,148,810,966,298,188,546,57,120,683,100,479,918,125,663,323,872,276,111,794,354,686,689,29,687,918,473,742,982,898,965,609,875,357,589,500,65,614,252,645,360,195,846,442,450,501,238,84,838,364,105,927,328,429,102,268,362,200,545,597,305,589,18,461,73,135,713,133,490,517,644,211,115,718,806,684,891,42,16,727,887,199,439,740,35,182,560,692,484,586,791,990,501,44,486,546,796,322,465,552,367,241,760,174,407,911,320,554,945,806,597,1,616,291,750,957,97,938,864,777,688,811,266,926,895,653,709,592,580,176,32,764,499,400,561,674,913,659,255,493,297,301,174,354,679,977,396,682,997,610,47,847,671,331,209,156,51,932,903,272,142,780,350,685,196,304,210,765,178,702,551,401,132,634,348,617,538,281,260,761,28,119,21,12,753,309,901,54,13,13,801,529,436,566,11,877,96,1000,696,451,224,991,638,994,462,555,826,744,909,518,916,516,342,58,69,865,248,622,121,697,785,410,168,300,536,192,22,315,831,116,800,177,914,91,369,810,393,980,902,47,272,384,664,356,966,492,754,549,600,412,353,261,773,75,974,379,682,535,149,757,873,646,315,77,576,820,646,808,219,618,56,527,868,306,514,561,850,114,318,5,539,218,535,666,572,30,211,581,818,508,191,298,708,961,822,572,493,933,959,601,714,264,595,919,656,169,652,334,970,862,356,447,237,948,432,729,34,83,399,189,59,137,549,838,820,51,804,908,544,726,934,321,698,196,135,466,217,892,936,590,565,547,727,804,316,616,763,459,52,338,44,3,489,178,198,759,636,126,728,206,468,417,247,470,346,81,515,388,957,945,921,79,188,805,125,424,786,486,869,970,276,770,447,565,416,454,433,358,950,363,930,803,258,786,884,152,758,56,242,922,323,24,1,74,614,50,650,698,793,173,768,35,985,223,717,789,420,699,412,799,322,650,555,595,571,385,41,134,649,728,386,931,49,893,383,71,3,242,606,556,877,656,922,117,604,197,153,568,718,999,454,689,117,17,368,101,422,708,71,907,498,393,676,355,180,409,969,575,911,850,797,474,114,578,487,375,590,797,200,288,500,371,204,913,226,415,259,343,550,350,482,150,867,128,63,741,669,775,942,551,513,111,39,772,896,265,870,763,683,80,853,511,265,800,382,873,659,32,529,539,327,367,281,28,376,487,333,563,564,921,151,271,833,613,684,523,556,435,841,884,466,894,998,985,906,582,150,801,435,482,608,131,639,102,202,703,534,369,622,959,91,881,506,167,194,536,146,239,10,687,343,946,213,519,481,240,437,72,197,978,79,255,585,602,769,745,442,888,905,814,291,274,471,395,54,858,789,224,771,935,508,49,825,717,479,652,24,305,741,624,184,9,670,895,579,701,628,831,179,320,293,853,30,329,935,491,744,499,521,22,283,471,858,283,907,779,12,407,665,677,303,719,723,85,262,10,668,544,692,596,344,930,919,480,575,14,98,936,300,97,103,365,617,287,299,619,605,604,569,146,767,89,123,277,968,750,635,969,502,771,341,105,519,951,400,382,944,705,865,899,874,23,843,848,787,676,324,342,759,673,443,314,190,538,988,107,847,502,647,963,274,109,504,229,740,366,131,503,888,39,596,756,662,928,229,225,889,773,145,949,720,569,749,145,533,887,326,965,582,823,331,266,441,864,83,462,949,550,452,940,107,521,624,269,467,190,93,297,284,445,937,416,332,360,701,942,894,869,900,248,673,781,732,837,222,818,23,785,364,736,334,768,375,769,693,755,338,833,119,299,898,564,645,767,990,923,477,210,33,341,633,237,601,846,163,103,94,633,395,793,757,368,719,924,915,473,706,21,492,851,431,732,848,856,384,720,388,524,474,752,948,958,74,41,68,67,892,639,20,376,225,118,623,623,739,603,612,68,67,944,876,59,380,826,488,638,448,630,881,968,361,153,167,545,745,791,961,344,726,997,733,180,169,387,852,222,319,626,313,982,803,691,461,642,235,678,306,643,185,662,408,512,84,161,780,514,89,273,790,202,657,567,591,422,511,450,977,313,775,179,697,258,530,657,75,843,672,34,996,663,205,542,587,219,61,916,161,181,472,444,355,78,278,743,485,86,268,688,630,557,893,213,387,621,372,164,311,26,109,934,690,711,860,695,314,337,206,587,17,644,82,950,127,420,609,203,304,262,570,506,336,723,703,260,104,373,678,443,173,758,567,386,654,318,321,16,751,332,70,60,233,468,192,235,527,821,805,390,923,628,227,749,218,328,419,162,31,690,947,714,203,620,764,217,195,863,581,664,541,423,953,122,632,226,851,52,241,577,863,38,707,901,480,205,136,912,175,236,251,488,347,584,743,816,674,902,78,570,737,852,181,943,329,713,337,194,642,675,517,353,50,600,547,467,855,900,677,787,363,972,588,136,366,559,811,82,38,608,365,142,924,437,976,296,586,976,978,465,557,880,394,917,578,733,261,872,691,730,171,611,844,347,729,94,680,709,512,66,648,428,429,33,770,11,520,201,357,974,938,915,603,436,378,654,615,147,86,496,85,981,933,891,619,408,667,106,445,530,415,477,518,175,189,257,837,168,552,293,626,193,199};
#include <string.h>
#include <stdint.h>
#include <regex.h>
#include <string.h>


#include <stdarg.h>
#include <assert.h>

#if defined (_WIN32) && !defined(__CYGWIN__)
#if defined (_MSC_VER) || defined(__BORLANDC__)
#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")
#define gettimeofday(p1,p2)
#endif /* _MSC_VER */
#endif /* _WIN32 */

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_TIME_H
#include <time.h>
#endif

#ifdef __MINGW32__
#define _WINSOCKAPI_
#include <wsockcompat.h>
#include <winsock2.h>
#undef XML_SOCKLEN_T
#define XML_SOCKLEN_T unsigned int
#endif

#ifdef HAVE_SYS_TIMEB_H
#include <sys/timeb.h>
#endif

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_SYS_MMAN_H
#include <sys/mman.h>
/* seems needed for Solaris */
#ifndef MAP_FAILED
#define MAP_FAILED ((void *) -1)
#endif
#endif
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_LIBREADLINE
#include <readline/readline.h>
#ifdef HAVE_LIBHISTORY
#include <readline/history.h>
#endif
#endif

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/HTMLparser.h>
#include <libxml/HTMLtree.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/debugXML.h>
#include <libxml/xmlerror.h>
#ifdef LIBXML_XINCLUDE_ENABLED
#include <libxml/xinclude.h>
#endif
#ifdef LIBXML_CATALOG_ENABLED
#include <libxml/catalog.h>
#endif
#include <libxml/globals.h>
#include <libxml/xmlreader.h>
#ifdef LIBXML_SCHEMATRON_ENABLED
#include <libxml/schematron.h>
#endif
#ifdef LIBXML_SCHEMAS_ENABLED
#include <libxml/relaxng.h>
#include <libxml/xmlschemas.h>
#endif
#ifdef LIBXML_PATTERN_ENABLED
#include <libxml/pattern.h>
#endif
#ifdef LIBXML_C14N_ENABLED
#include <libxml/c14n.h>
#endif
#ifdef LIBXML_OUTPUT_ENABLED
#include <libxml/xmlsave.h>
#endif

#ifndef XML_XML_DEFAULT_CATALOG
#define XML_XML_DEFAULT_CATALOG "file:///etc/xml/catalog"
#endif

typedef enum {
    XMLLINT_RETURN_OK = 0,	/* No error */
    XMLLINT_ERR_UNCLASS = 1,	/* Unclassified */
    XMLLINT_ERR_DTD = 2,	/* Error in DTD */
    XMLLINT_ERR_VALID = 3,	/* Validation error */
    XMLLINT_ERR_RDFILE = 4,	/* CtxtReadFile error */
    XMLLINT_ERR_SCHEMACOMP = 5,	/* Schema compilation */
    XMLLINT_ERR_OUT = 6,	/* Error writing output */
    XMLLINT_ERR_SCHEMAPAT = 7,	/* Error in schema pattern */
    XMLLINT_ERR_RDREGIS = 8,	/* Error in Reader registration */
    XMLLINT_ERR_MEM = 9,	/* Out of memory error */
    XMLLINT_ERR_XPATH = 10	/* XPath evaluation error */
} xmllintReturnCode;
#ifdef LIBXML_DEBUG_ENABLED
static int shell = 0;
static int debugent = 0;
#endif
static int debug = 0;
static int maxmem = 0;
#ifdef LIBXML_TREE_ENABLED
static int copy = 0;
#endif /* LIBXML_TREE_ENABLED */
static int recovery = 0;
static int noent = 0;
static int noenc = 0;
static int noblanks = 0;
static int noout = 0;
static int nowrap = 0;
#ifdef LIBXML_OUTPUT_ENABLED
static int format = 0;
static const char *output = NULL;
static int compress = 0;
static int oldout = 0;
#endif /* LIBXML_OUTPUT_ENABLED */
#ifdef LIBXML_VALID_ENABLED
static int valid = 0;
static int postvalid = 0;
static char * dtdvalid = NULL;
static char * dtdvalidfpi = NULL;
#endif
#ifdef LIBXML_SCHEMAS_ENABLED
static char * relaxng = NULL;
static xmlRelaxNGPtr relaxngschemas = NULL;
static char * schema = NULL;
static xmlSchemaPtr wxschemas = NULL;
#endif
#ifdef LIBXML_SCHEMATRON_ENABLED
static char * schematron = NULL;
static xmlSchematronPtr wxschematron = NULL;
#endif
static int repeat = 0;
static int insert = 0;
#if defined(LIBXML_HTML_ENABLED) || defined(LIBXML_VALID_ENABLED)
static int html = 0;
static int xmlout = 0;
#endif
static int htmlout = 0;
#if defined(LIBXML_HTML_ENABLED)
static int nodefdtd = 0;
#endif
#ifdef LIBXML_PUSH_ENABLED
static int push = 0;
static int pushsize = 4096;
#endif /* LIBXML_PUSH_ENABLED */
#ifdef HAVE_MMAP
static int memory = 0;
#endif
static int testIO = 0;
static char *encoding = NULL;
#ifdef LIBXML_XINCLUDE_ENABLED
static int xinclude = 0;
#endif
static int dtdattrs = 0;
static int loaddtd = 0;
static xmllintReturnCode progresult = XMLLINT_RETURN_OK;
static int timing = 0;
static int generate = 0;
static int dropdtd = 0;
#ifdef LIBXML_CATALOG_ENABLED
static int catalogs = 0;
static int nocatalogs = 0;
#endif
#ifdef LIBXML_C14N_ENABLED
static int canonical = 0;
static int canonical_11 = 0;
static int exc_canonical = 0;
#endif
#ifdef LIBXML_READER_ENABLED
static int stream = 0;
static int walker = 0;
#endif /* LIBXML_READER_ENABLED */
static int chkregister = 0;
static int nbregister = 0;
#ifdef LIBXML_SAX1_ENABLED
static int sax1 = 0;
#endif /* LIBXML_SAX1_ENABLED */
#ifdef LIBXML_PATTERN_ENABLED
static const char *pattern = NULL;
static xmlPatternPtr patternc = NULL;
static xmlStreamCtxtPtr patstream = NULL;
#endif
#ifdef LIBXML_XPATH_ENABLED
static const char *xpathquery = NULL;
#endif
static int options = XML_PARSE_COMPACT | XML_PARSE_BIG_LINES;
static int sax = 0;
static int oldxml10 = 0;

/************************************************************************
 *									*
 *		 Entity loading control and customization.		*
 *									*
 ************************************************************************/
#define MAX_PATHS 64
#ifdef _WIN32
# define PATH_SEPARATOR ';'
#else
# define PATH_SEPARATOR ':'
#endif
static xmlChar *paths[MAX_PATHS + 1];
static int nbpaths = 0;
static int load_trace = 0;

static
void parsePath(const xmlChar *path) {
    const xmlChar *cur;

    if (path == NULL)
	return;
    while (*path != 0) {
	if (nbpaths >= MAX_PATHS) {
	    fprintf(stderr, "MAX_PATHS reached: too many paths\n");
	    return;
	}
	cur = path;
	while ((*cur == ' ') || (*cur == PATH_SEPARATOR))
	    cur++;
	path = cur;
	while ((*cur != 0) && (*cur != ' ') && (*cur != PATH_SEPARATOR))
	    cur++;
	if (cur != path) {
	    paths[nbpaths] = xmlStrndup(path, cur - path);
	    if (paths[nbpaths] != NULL)
		nbpaths++;
	    path = cur;
	}
    }
}

static xmlExternalEntityLoader defaultEntityLoader = NULL;

static xmlParserInputPtr
xmllintExternalEntityLoader(const char *URL, const char *ID,
			     xmlParserCtxtPtr ctxt) {
    xmlParserInputPtr ret;
    warningSAXFunc warning = NULL;
    errorSAXFunc err = NULL;

    int i;
    const char *lastsegment = URL;
    const char *iter = URL;

    if ((nbpaths > 0) && (iter != NULL)) {
	while (*iter != 0) {
	    if (*iter == '/')
		lastsegment = iter + 1;
	    iter++;
	}
    }

    if ((ctxt != NULL) && (ctxt->sax != NULL)) {
	warning = ctxt->sax->warning;
	err = ctxt->sax->error;
	ctxt->sax->warning = NULL;
	ctxt->sax->error = NULL;
    }

    if (defaultEntityLoader != NULL) {
	ret = defaultEntityLoader(URL, ID, ctxt);
	if (ret != NULL) {
	    if (warning != NULL)
		ctxt->sax->warning = warning;
	    if (err != NULL)
		ctxt->sax->error = err;
	    if (load_trace) {
		fprintf \
			(stderr,
			 "Loaded URL=\"%s\" ID=\"%s\"\n",
			 URL ? URL : "(null)",
			 ID ? ID : "(null)");
	    }
	    return(ret);
	}
    }
    for (i = 0;i < nbpaths;i++) {
	xmlChar *newURL;

	newURL = xmlStrdup((const xmlChar *) paths[i]);
	newURL = xmlStrcat(newURL, (const xmlChar *) "/");
	newURL = xmlStrcat(newURL, (const xmlChar *) lastsegment);
	if (newURL != NULL) {
	    ret = defaultEntityLoader((const char *)newURL, ID, ctxt);
	    if (ret != NULL) {
		if (warning != NULL)
		    ctxt->sax->warning = warning;
		if (err != NULL)
		    ctxt->sax->error = err;
		if (load_trace) {
		    fprintf \
			(stderr,
			 "Loaded URL=\"%s\" ID=\"%s\"\n",
			 newURL,
			 ID ? ID : "(null)");
		}
		xmlFree(newURL);
		return(ret);
	    }
	    xmlFree(newURL);
	}
    }
    if (err != NULL)
        ctxt->sax->error = err;
    if (warning != NULL) {
	ctxt->sax->warning = warning;
	if (URL != NULL)
	    warning(ctxt, "failed to load external entity \"%s\"\n", URL);
	else if (ID != NULL)
	    warning(ctxt, "failed to load external entity \"%s\"\n", ID);
    }
    return(NULL);
}
/************************************************************************
 *									*
 * Memory allocation consumption debugging				*
 *									*
 ************************************************************************/

static void
OOM(void)
{
    fprintf(stderr, "Ran out of memory needs > %d bytes\n", maxmem);
    progresult = XMLLINT_ERR_MEM;
}

static void
myFreeFunc(void *mem)
{
    xmlMemFree(mem);
}
static void *
myMallocFunc(size_t size)
{
    void *ret;

    ret = xmlMemMalloc(size);
    if (ret != NULL) {
        if (xmlMemUsed() > maxmem) {
            OOM();
            xmlMemFree(ret);
            return (NULL);
        }
    }
    return (ret);
}
static void *
myReallocFunc(void *mem, size_t size)
{
    void *ret;

    ret = xmlMemRealloc(mem, size);
    if (ret != NULL) {
        if (xmlMemUsed() > maxmem) {
            OOM();
            xmlMemFree(ret);
            return (NULL);
        }
    }
    return (ret);
}
static char *
myStrdupFunc(const char *str)
{
    char *ret;

    ret = xmlMemoryStrdup(str);
    if (ret != NULL) {
        if (xmlMemUsed() > maxmem) {
            OOM();
            xmlFree(ret);
            return (NULL);
        }
    }
    return (ret);
}
/************************************************************************
 *									*
 * Internal timing routines to remove the necessity to have		*
 * unix-specific function calls.					*
 *									*
 ************************************************************************/

#ifndef HAVE_GETTIMEOFDAY
#ifdef HAVE_SYS_TIMEB_H
#ifdef HAVE_SYS_TIME_H
#ifdef HAVE_FTIME

static int
my_gettimeofday(struct timeval *tvp, void *tzp)
{
	struct timeb timebuffer;

	ftime(&timebuffer);
	if (tvp) {
		tvp->tv_sec = timebuffer.time;
		tvp->tv_usec = timebuffer.millitm * 1000L;
	}
	return (0);
}
#define HAVE_GETTIMEOFDAY 1
#define gettimeofday my_gettimeofday

#endif /* HAVE_FTIME */
#endif /* HAVE_SYS_TIME_H */
#endif /* HAVE_SYS_TIMEB_H */
#endif /* !HAVE_GETTIMEOFDAY */

#if defined(HAVE_GETTIMEOFDAY)
static struct timeval begin, end;

/*
 * startTimer: call where you want to start timing
 */
static void
startTimer(void)
{
    gettimeofday(&begin, NULL);
}

/*
 * endTimer: call where you want to stop timing and to print out a
 *           message about the timing performed; format is a printf
 *           type argument
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(1,2)
endTimer(const char *fmt, ...)
{
    long msec;
    va_list ap;

    gettimeofday(&end, NULL);
    msec = end.tv_sec - begin.tv_sec;
    msec *= 1000;
    msec += (end.tv_usec - begin.tv_usec) / 1000;

#ifndef HAVE_STDARG_H
#error "endTimer required stdarg functions"
#endif
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    fprintf(stderr, " took %ld ms\n", msec);
}
#elif defined(HAVE_TIME_H)
/*
 * No gettimeofday function, so we have to make do with calling clock.
 * This is obviously less accurate, but there's little we can do about
 * that.
 */
#ifndef CLOCKS_PER_SEC
#define CLOCKS_PER_SEC 100
#endif

static clock_t begin, end;
static void
startTimer(void)
{
    begin = clock();
}
static void XMLCDECL LIBXML_ATTR_FORMAT(1,2)
endTimer(const char *fmt, ...)
{
    long msec;
    va_list ap;

    end = clock();
    msec = ((end - begin) * 1000) / CLOCKS_PER_SEC;

#ifndef HAVE_STDARG_H
#error "endTimer required stdarg functions"
#endif
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, " took %ld ms\n", msec);
}
#else

/*
 * We don't have a gettimeofday or time.h, so we just don't do timing
 */
static void
startTimer(void)
{
    /*
     * Do nothing
     */
}
static void XMLCDECL LIBXML_ATTR_FORMAT(1,2)
endTimer(char *format, ...)
{
    /*
     * We cannot do anything because we don't have a timing function
     */
#ifdef HAVE_STDARG_H
    va_list ap;
    va_start(ap, format);
    vfprintf(stderr, format, ap);
    va_end(ap);
    fprintf(stderr, " was not timed\n");
#else
    /* We don't have gettimeofday, time or stdarg.h, what crazy world is
     * this ?!
     */
#endif
}
#endif
/************************************************************************
 *									*
 *			HTML ouput					*
 *									*
 ************************************************************************/
static char buffer[50000];

static void
xmlHTMLEncodeSend(void) {
    char *result;

    result = (char *) xmlEncodeEntitiesReentrant(NULL, BAD_CAST buffer);
    if (result) {
	xmlGenericError(xmlGenericErrorContext, "%s", result);
	xmlFree(result);
    }
    buffer[0] = 0;
}

/**
 * xmlHTMLPrintFileInfo:
 * @input:  an xmlParserInputPtr input
 *
 * Displays the associated file and line informations for the current input
 */

static void
xmlHTMLPrintFileInfo(xmlParserInputPtr input) {
    int len;
    xmlGenericError(xmlGenericErrorContext, "<p>");

    len = strlen(buffer);
    if (input != NULL) {
	if (input->filename) {
	    snprintf(&buffer[len], sizeof(buffer) - len, "%s:%d: ", input->filename,
		    input->line);
	} else {
	    snprintf(&buffer[len], sizeof(buffer) - len, "Entity: line %d: ", input->line);
	}
    }
    xmlHTMLEncodeSend();
}

/**
 * xmlHTMLPrintFileContext:
 * @input:  an xmlParserInputPtr input
 *
 * Displays current context within the input content for error tracking
 */

static void
xmlHTMLPrintFileContext(xmlParserInputPtr input) {
    const xmlChar *cur, *base;
    int len;
    int n;

    if (input == NULL) return;
    xmlGenericError(xmlGenericErrorContext, "<pre>\n");
    cur = input->cur;
    base = input->base;
    while ((cur > base) && ((*cur == '\n') || (*cur == '\r'))) {
	cur--;
    }
    n = 0;
    while ((n++ < 80) && (cur > base) && (*cur != '\n') && (*cur != '\r'))
        cur--;
    if ((*cur == '\n') || (*cur == '\r')) cur++;
    base = cur;
    n = 0;
    while ((*cur != 0) && (*cur != '\n') && (*cur != '\r') && (n < 79)) {
	len = strlen(buffer);
        snprintf(&buffer[len], sizeof(buffer) - len, "%c",
		    (unsigned char) *cur++);
	n++;
    }
    len = strlen(buffer);
    snprintf(&buffer[len], sizeof(buffer) - len, "\n");
    cur = input->cur;
    while ((*cur == '\n') || (*cur == '\r'))
	cur--;
    n = 0;
    while ((cur != base) && (n++ < 80)) {
	len = strlen(buffer);
        snprintf(&buffer[len], sizeof(buffer) - len, " ");
        base++;
    }
    len = strlen(buffer);
    snprintf(&buffer[len], sizeof(buffer) - len, "^\n");
    xmlHTMLEncodeSend();
    xmlGenericError(xmlGenericErrorContext, "</pre>");
}

/**
 * xmlHTMLError:
 * @ctx:  an XML parser context
 * @msg:  the message to display/transmit
 * @...:  extra parameters for the message display
 *
 * Display and format an error messages, gives file, line, position and
 * extra parameters.
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(2,3)
xmlHTMLError(void *ctx, const char *msg, ...)
{
    xmlParserCtxtPtr ctxt = (xmlParserCtxtPtr) ctx;
    xmlParserInputPtr input;
    va_list args;
    int len;

    buffer[0] = 0;
    input = ctxt->input;
    if ((input != NULL) && (input->filename == NULL) && (ctxt->inputNr > 1)) {
        input = ctxt->inputTab[ctxt->inputNr - 2];
    }

    xmlHTMLPrintFileInfo(input);

    xmlGenericError(xmlGenericErrorContext, "<b>error</b>: ");
    va_start(args, msg);
    len = strlen(buffer);
    vsnprintf(&buffer[len],  sizeof(buffer) - len, msg, args);
    va_end(args);
    xmlHTMLEncodeSend();
    xmlGenericError(xmlGenericErrorContext, "</p>\n");

    xmlHTMLPrintFileContext(input);
    xmlHTMLEncodeSend();
}

/**
 * xmlHTMLWarning:
 * @ctx:  an XML parser context
 * @msg:  the message to display/transmit
 * @...:  extra parameters for the message display
 *
 * Display and format a warning messages, gives file, line, position and
 * extra parameters.
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(2,3)
xmlHTMLWarning(void *ctx, const char *msg, ...)
{
    xmlParserCtxtPtr ctxt = (xmlParserCtxtPtr) ctx;
    xmlParserInputPtr input;
    va_list args;
    int len;

    buffer[0] = 0;
    input = ctxt->input;
    if ((input != NULL) && (input->filename == NULL) && (ctxt->inputNr > 1)) {
        input = ctxt->inputTab[ctxt->inputNr - 2];
    }


    xmlHTMLPrintFileInfo(input);

    xmlGenericError(xmlGenericErrorContext, "<b>warning</b>: ");
    va_start(args, msg);
    len = strlen(buffer);
    vsnprintf(&buffer[len],  sizeof(buffer) - len, msg, args);
    va_end(args);
    xmlHTMLEncodeSend();
    xmlGenericError(xmlGenericErrorContext, "</p>\n");

    xmlHTMLPrintFileContext(input);
    xmlHTMLEncodeSend();
}

/**
 * xmlHTMLValidityError:
 * @ctx:  an XML parser context
 * @msg:  the message to display/transmit
 * @...:  extra parameters for the message display
 *
 * Display and format an validity error messages, gives file,
 * line, position and extra parameters.
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(2,3)
xmlHTMLValidityError(void *ctx, const char *msg, ...)
{
    xmlParserCtxtPtr ctxt = (xmlParserCtxtPtr) ctx;
    xmlParserInputPtr input;
    va_list args;
    int len;

    buffer[0] = 0;
    input = ctxt->input;
    if ((input->filename == NULL) && (ctxt->inputNr > 1))
        input = ctxt->inputTab[ctxt->inputNr - 2];

    xmlHTMLPrintFileInfo(input);

    xmlGenericError(xmlGenericErrorContext, "<b>validity error</b>: ");
    len = strlen(buffer);
    va_start(args, msg);
    vsnprintf(&buffer[len],  sizeof(buffer) - len, msg, args);
    va_end(args);
    xmlHTMLEncodeSend();
    xmlGenericError(xmlGenericErrorContext, "</p>\n");

    xmlHTMLPrintFileContext(input);
    xmlHTMLEncodeSend();
    progresult = XMLLINT_ERR_VALID;
}

/**
 * xmlHTMLValidityWarning:
 * @ctx:  an XML parser context
 * @msg:  the message to display/transmit
 * @...:  extra parameters for the message display
 *
 * Display and format a validity warning messages, gives file, line,
 * position and extra parameters.
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(2,3)
xmlHTMLValidityWarning(void *ctx, const char *msg, ...)
{
    xmlParserCtxtPtr ctxt = (xmlParserCtxtPtr) ctx;
    xmlParserInputPtr input;
    va_list args;
    int len;

    buffer[0] = 0;
    input = ctxt->input;
    if ((input->filename == NULL) && (ctxt->inputNr > 1))
        input = ctxt->inputTab[ctxt->inputNr - 2];

    xmlHTMLPrintFileInfo(input);

    xmlGenericError(xmlGenericErrorContext, "<b>validity warning</b>: ");
    va_start(args, msg);
    len = strlen(buffer);
    vsnprintf(&buffer[len],  sizeof(buffer) - len, msg, args);
    va_end(args);
    xmlHTMLEncodeSend();
    xmlGenericError(xmlGenericErrorContext, "</p>\n");

    xmlHTMLPrintFileContext(input);
    xmlHTMLEncodeSend();
}

/************************************************************************
 *									*
 *			Shell Interface					*
 *									*
 ************************************************************************/
#ifdef LIBXML_DEBUG_ENABLED
#ifdef LIBXML_XPATH_ENABLED
/**
 * xmlShellReadline:
 * @prompt:  the prompt value
 *
 * Read a string
 *
 * Returns a pointer to it or NULL on EOF the caller is expected to
 *     free the returned string.
 */
static char *
xmlShellReadline(char *prompt) {
#ifdef HAVE_LIBREADLINE
    char *line_read;

    /* Get a line from the user. */
    line_read = readline (prompt);

    /* If the line has any text in it, save it on the history. */
    if (line_read && *line_read)
	add_history (line_read);

    return (line_read);
#else
    char line_read[501];
    char *ret;
    int len;

    if (prompt != NULL)
	fprintf(stdout, "%s", prompt);
    fflush(stdout);
    if (!fgets(line_read, 500, stdin))
        return(NULL);
    line_read[500] = 0;
    len = strlen(line_read);
    ret = (char *) malloc(len + 1);
    if (ret != NULL) {
	memcpy (ret, line_read, len + 1);
    }
    return(ret);
#endif
}
#endif /* LIBXML_XPATH_ENABLED */
#endif /* LIBXML_DEBUG_ENABLED */

/************************************************************************
 *									*
 *			I/O Interfaces					*
 *									*
 ************************************************************************/

static int myRead(FILE *f, char * buf, int len) {
    return(fread(buf, 1, len, f));
}
static void myClose(FILE *f) {
  if (f != stdin) {
    fclose(f);
  }
}

/************************************************************************
 *									*
 *			SAX based tests					*
 *									*
 ************************************************************************/

/*
 * empty SAX block
 */
static xmlSAXHandler emptySAXHandlerStruct = {
    NULL, /* internalSubset */
    NULL, /* isStandalone */
    NULL, /* hasInternalSubset */
    NULL, /* hasExternalSubset */
    NULL, /* resolveEntity */
    NULL, /* getEntity */
    NULL, /* entityDecl */
    NULL, /* notationDecl */
    NULL, /* attributeDecl */
    NULL, /* elementDecl */
    NULL, /* unparsedEntityDecl */
    NULL, /* setDocumentLocator */
    NULL, /* startDocument */
    NULL, /* endDocument */
    NULL, /* startElement */
    NULL, /* endElement */
    NULL, /* reference */
    NULL, /* characters */
    NULL, /* ignorableWhitespace */
    NULL, /* processingInstruction */
    NULL, /* comment */
    NULL, /* xmlParserWarning */
    NULL, /* xmlParserError */
    NULL, /* xmlParserError */
    NULL, /* getParameterEntity */
    NULL, /* cdataBlock; */
    NULL, /* externalSubset; */
    XML_SAX2_MAGIC,
    NULL,
    NULL, /* startElementNs */
    NULL, /* endElementNs */
    NULL  /* xmlStructuredErrorFunc */
};

static xmlSAXHandlerPtr emptySAXHandler = &emptySAXHandlerStruct;
extern xmlSAXHandlerPtr debugSAXHandler;
static int callbacks;

/**
 * isStandaloneDebug:
 * @ctxt:  An XML parser context
 *
 * Is this document tagged standalone ?
 *
 * Returns 1 if true
 */
static int
isStandaloneDebug(void *ctx ATTRIBUTE_UNUSED)
{
    callbacks++;
    if (noout)
	return(0);
    fprintf(stdout, "SAX.isStandalone()\n");
    return(0);
}

/**
 * hasInternalSubsetDebug:
 * @ctxt:  An XML parser context
 *
 * Does this document has an internal subset
 *
 * Returns 1 if true
 */
static int
hasInternalSubsetDebug(void *ctx ATTRIBUTE_UNUSED)
{
    callbacks++;
    if (noout)
	return(0);
    fprintf(stdout, "SAX.hasInternalSubset()\n");
    return(0);
}

/**
 * hasExternalSubsetDebug:
 * @ctxt:  An XML parser context
 *
 * Does this document has an external subset
 *
 * Returns 1 if true
 */
static int
hasExternalSubsetDebug(void *ctx ATTRIBUTE_UNUSED)
{
    callbacks++;
    if (noout)
	return(0);
    fprintf(stdout, "SAX.hasExternalSubset()\n");
    return(0);
}

/**
 * internalSubsetDebug:
 * @ctxt:  An XML parser context
 *
 * Does this document has an internal subset
 */
static void
internalSubsetDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name,
	       const xmlChar *ExternalID, const xmlChar *SystemID)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.internalSubset(%s,", name);
    if (ExternalID == NULL)
	fprintf(stdout, " ,");
    else
	fprintf(stdout, " %s,", ExternalID);
    if (SystemID == NULL)
	fprintf(stdout, " )\n");
    else
	fprintf(stdout, " %s)\n", SystemID);
}

/**
 * externalSubsetDebug:
 * @ctxt:  An XML parser context
 *
 * Does this document has an external subset
 */
static void
externalSubsetDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name,
	       const xmlChar *ExternalID, const xmlChar *SystemID)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.externalSubset(%s,", name);
    if (ExternalID == NULL)
	fprintf(stdout, " ,");
    else
	fprintf(stdout, " %s,", ExternalID);
    if (SystemID == NULL)
	fprintf(stdout, " )\n");
    else
	fprintf(stdout, " %s)\n", SystemID);
}

/**
 * resolveEntityDebug:
 * @ctxt:  An XML parser context
 * @publicId: The public ID of the entity
 * @systemId: The system ID of the entity
 *
 * Special entity resolver, better left to the parser, it has
 * more context than the application layer.
 * The default behaviour is to NOT resolve the entities, in that case
 * the ENTITY_REF nodes are built in the structure (and the parameter
 * values).
 *
 * Returns the xmlParserInputPtr if inlined or NULL for DOM behaviour.
 */
static xmlParserInputPtr
resolveEntityDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *publicId, const xmlChar *systemId)
{
    callbacks++;
    if (noout)
	return(NULL);
    /* xmlParserCtxtPtr ctxt = (xmlParserCtxtPtr) ctx; */


    fprintf(stdout, "SAX.resolveEntity(");
    if (publicId != NULL)
	fprintf(stdout, "%s", (char *)publicId);
    else
	fprintf(stdout, " ");
    if (systemId != NULL)
	fprintf(stdout, ", %s)\n", (char *)systemId);
    else
	fprintf(stdout, ", )\n");
    return(NULL);
}

/**
 * getEntityDebug:
 * @ctxt:  An XML parser context
 * @name: The entity name
 *
 * Get an entity by name
 *
 * Returns the xmlParserInputPtr if inlined or NULL for DOM behaviour.
 */
static xmlEntityPtr
getEntityDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name)
{
    callbacks++;
    if (noout)
	return(NULL);
    fprintf(stdout, "SAX.getEntity(%s)\n", name);
    return(NULL);
}

/**
 * getParameterEntityDebug:
 * @ctxt:  An XML parser context
 * @name: The entity name
 *
 * Get a parameter entity by name
 *
 * Returns the xmlParserInputPtr
 */
static xmlEntityPtr
getParameterEntityDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name)
{
    callbacks++;
    if (noout)
	return(NULL);
    fprintf(stdout, "SAX.getParameterEntity(%s)\n", name);
    return(NULL);
}


/**
 * entityDeclDebug:
 * @ctxt:  An XML parser context
 * @name:  the entity name
 * @type:  the entity type
 * @publicId: The public ID of the entity
 * @systemId: The system ID of the entity
 * @content: the entity value (without processing).
 *
 * An entity definition has been parsed
 */
static void
entityDeclDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name, int type,
          const xmlChar *publicId, const xmlChar *systemId, xmlChar *content)
{
const xmlChar *nullstr = BAD_CAST "(null)";
    /* not all libraries handle printing null pointers nicely */
    if (publicId == NULL)
        publicId = nullstr;
    if (systemId == NULL)
        systemId = nullstr;
    if (content == NULL)
        content = (xmlChar *)nullstr;
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.entityDecl(%s, %d, %s, %s, %s)\n",
            name, type, publicId, systemId, content);
}

/**
 * attributeDeclDebug:
 * @ctxt:  An XML parser context
 * @name:  the attribute name
 * @type:  the attribute type
 *
 * An attribute definition has been parsed
 */
static void
attributeDeclDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar * elem,
                   const xmlChar * name, int type, int def,
                   const xmlChar * defaultValue, xmlEnumerationPtr tree)
{
    callbacks++;
    if (noout)
        return;
    if (defaultValue == NULL)
        fprintf(stdout, "SAX.attributeDecl(%s, %s, %d, %d, NULL, ...)\n",
                elem, name, type, def);
    else
        fprintf(stdout, "SAX.attributeDecl(%s, %s, %d, %d, %s, ...)\n",
                elem, name, type, def, defaultValue);
    xmlFreeEnumeration(tree);
}

/**
 * elementDeclDebug:
 * @ctxt:  An XML parser context
 * @name:  the element name
 * @type:  the element type
 * @content: the element value (without processing).
 *
 * An element definition has been parsed
 */
static void
elementDeclDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name, int type,
	    xmlElementContentPtr content ATTRIBUTE_UNUSED)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.elementDecl(%s, %d, ...)\n",
            name, type);
}

/**
 * notationDeclDebug:
 * @ctxt:  An XML parser context
 * @name: The name of the notation
 * @publicId: The public ID of the entity
 * @systemId: The system ID of the entity
 *
 * What to do when a notation declaration has been parsed.
 */
static void
notationDeclDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name,
	     const xmlChar *publicId, const xmlChar *systemId)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.notationDecl(%s, %s, %s)\n",
            (char *) name, (char *) publicId, (char *) systemId);
}

/**
 * unparsedEntityDeclDebug:
 * @ctxt:  An XML parser context
 * @name: The name of the entity
 * @publicId: The public ID of the entity
 * @systemId: The system ID of the entity
 * @notationName: the name of the notation
 *
 * What to do when an unparsed entity declaration is parsed
 */
static void
unparsedEntityDeclDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name,
		   const xmlChar *publicId, const xmlChar *systemId,
		   const xmlChar *notationName)
{
const xmlChar *nullstr = BAD_CAST "(null)";

    if (publicId == NULL)
        publicId = nullstr;
    if (systemId == NULL)
        systemId = nullstr;
    if (notationName == NULL)
        notationName = nullstr;
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.unparsedEntityDecl(%s, %s, %s, %s)\n",
            (char *) name, (char *) publicId, (char *) systemId,
	    (char *) notationName);
}

/**
 * setDocumentLocatorDebug:
 * @ctxt:  An XML parser context
 * @loc: A SAX Locator
 *
 * Receive the document locator at startup, actually xmlDefaultSAXLocator
 * Everything is available on the context, so this is useless in our case.
 */
static void
setDocumentLocatorDebug(void *ctx ATTRIBUTE_UNUSED, xmlSAXLocatorPtr loc ATTRIBUTE_UNUSED)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.setDocumentLocator()\n");
}

/**
 * startDocumentDebug:
 * @ctxt:  An XML parser context
 *
 * called when the document start being processed.
 */
static void
startDocumentDebug(void *ctx ATTRIBUTE_UNUSED)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.startDocument()\n");
}

/**
 * endDocumentDebug:
 * @ctxt:  An XML parser context
 *
 * called when the document end has been detected.
 */
static void
endDocumentDebug(void *ctx ATTRIBUTE_UNUSED)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.endDocument()\n");
}

/**
 * startElementDebug:
 * @ctxt:  An XML parser context
 * @name:  The element name
 *
 * called when an opening tag has been processed.
 */
static void
startElementDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name, const xmlChar **atts)
{
    int i;

    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.startElement(%s", (char *) name);
    if (atts != NULL) {
        for (i = 0;(atts[i] != NULL);i++) {
	    fprintf(stdout, ", %s='", atts[i++]);
	    if (atts[i] != NULL)
	        fprintf(stdout, "%s'", atts[i]);
	}
    }
    fprintf(stdout, ")\n");
}

/**
 * endElementDebug:
 * @ctxt:  An XML parser context
 * @name:  The element name
 *
 * called when the end of an element has been detected.
 */
static void
endElementDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.endElement(%s)\n", (char *) name);
}

/**
 * charactersDebug:
 * @ctxt:  An XML parser context
 * @ch:  a xmlChar string
 * @len: the number of xmlChar
 *
 * receiving some chars from the parser.
 * Question: how much at a time ???
 */
static void
charactersDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *ch, int len)
{
    char out[40];
    int i;

    callbacks++;
    if (noout)
	return;
    for (i = 0;(i<len) && (i < 30);i++)
	out[i] = ch[i];
    out[i] = 0;

    fprintf(stdout, "SAX.characters(%s, %d)\n", out, len);
}

/**
 * referenceDebug:
 * @ctxt:  An XML parser context
 * @name:  The entity name
 *
 * called when an entity reference is detected.
 */
static void
referenceDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *name)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.reference(%s)\n", name);
}

/**
 * ignorableWhitespaceDebug:
 * @ctxt:  An XML parser context
 * @ch:  a xmlChar string
 * @start: the first char in the string
 * @len: the number of xmlChar
 *
 * receiving some ignorable whitespaces from the parser.
 * Question: how much at a time ???
 */
static void
ignorableWhitespaceDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *ch, int len)
{
    char out[40];
    int i;

    callbacks++;
    if (noout)
	return;
    for (i = 0;(i<len) && (i < 30);i++)
	out[i] = ch[i];
    out[i] = 0;
    fprintf(stdout, "SAX.ignorableWhitespace(%s, %d)\n", out, len);
}

/**
 * processingInstructionDebug:
 * @ctxt:  An XML parser context
 * @target:  the target name
 * @data: the PI data's
 * @len: the number of xmlChar
 *
 * A processing instruction has been parsed.
 */
static void
processingInstructionDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *target,
                      const xmlChar *data)
{
    callbacks++;
    if (noout)
	return;
    if (data != NULL)
	fprintf(stdout, "SAX.processingInstruction(%s, %s)\n",
		(char *) target, (char *) data);
    else
	fprintf(stdout, "SAX.processingInstruction(%s, NULL)\n",
		(char *) target);
}

/**
 * cdataBlockDebug:
 * @ctx: the user data (XML parser context)
 * @value:  The pcdata content
 * @len:  the block length
 *
 * called when a pcdata block has been parsed
 */
static void
cdataBlockDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *value, int len)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.pcdata(%.20s, %d)\n",
	    (char *) value, len);
}

/**
 * commentDebug:
 * @ctxt:  An XML parser context
 * @value:  the comment content
 *
 * A comment has been parsed.
 */
static void
commentDebug(void *ctx ATTRIBUTE_UNUSED, const xmlChar *value)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.comment(%s)\n", value);
}

/**
 * warningDebug:
 * @ctxt:  An XML parser context
 * @msg:  the message to display/transmit
 * @...:  extra parameters for the message display
 *
 * Display and format a warning messages, gives file, line, position and
 * extra parameters.
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(2,3)
warningDebug(void *ctx ATTRIBUTE_UNUSED, const char *msg, ...)
{
    va_list args;

    callbacks++;
    if (noout)
	return;
    va_start(args, msg);
    fprintf(stdout, "SAX.warning: ");
    vfprintf(stdout, msg, args);
    va_end(args);
}

/**
 * errorDebug:
 * @ctxt:  An XML parser context
 * @msg:  the message to display/transmit
 * @...:  extra parameters for the message display
 *
 * Display and format a error messages, gives file, line, position and
 * extra parameters.
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(2,3)
errorDebug(void *ctx ATTRIBUTE_UNUSED, const char *msg, ...)
{
    va_list args;

    callbacks++;
    if (noout)
	return;
    va_start(args, msg);
    fprintf(stdout, "SAX.error: ");
    vfprintf(stdout, msg, args);
    va_end(args);
}

/**
 * fatalErrorDebug:
 * @ctxt:  An XML parser context
 * @msg:  the message to display/transmit
 * @...:  extra parameters for the message display
 *
 * Display and format a fatalError messages, gives file, line, position and
 * extra parameters.
 */
static void XMLCDECL LIBXML_ATTR_FORMAT(2,3)
fatalErrorDebug(void *ctx ATTRIBUTE_UNUSED, const char *msg, ...)
{
    va_list args;

    callbacks++;
    if (noout)
	return;
    va_start(args, msg);
    fprintf(stdout, "SAX.fatalError: ");
    vfprintf(stdout, msg, args);
    va_end(args);
}

static xmlSAXHandler debugSAXHandlerStruct = {
    internalSubsetDebug,
    isStandaloneDebug,
    hasInternalSubsetDebug,
    hasExternalSubsetDebug,
    resolveEntityDebug,
    getEntityDebug,
    entityDeclDebug,
    notationDeclDebug,
    attributeDeclDebug,
    elementDeclDebug,
    unparsedEntityDeclDebug,
    setDocumentLocatorDebug,
    startDocumentDebug,
    endDocumentDebug,
    startElementDebug,
    endElementDebug,
    referenceDebug,
    charactersDebug,
    ignorableWhitespaceDebug,
    processingInstructionDebug,
    commentDebug,
    warningDebug,
    errorDebug,
    fatalErrorDebug,
    getParameterEntityDebug,
    cdataBlockDebug,
    externalSubsetDebug,
    1,
    NULL,
    NULL,
    NULL,
    NULL
};

xmlSAXHandlerPtr debugSAXHandler = &debugSAXHandlerStruct;

/*
 * SAX2 specific callbacks
 */
/**
 * startElementNsDebug:
 * @ctxt:  An XML parser context
 * @name:  The element name
 *
 * called when an opening tag has been processed.
 */
static void
startElementNsDebug(void *ctx ATTRIBUTE_UNUSED,
                    const xmlChar *localname,
                    const xmlChar *prefix,
                    const xmlChar *URI,
		    int nb_namespaces,
		    const xmlChar **namespaces,
		    int nb_attributes,
		    int nb_defaulted,
		    const xmlChar **attributes)
{
    int i;

    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.startElementNs(%s", (char *) localname);
    if (prefix == NULL)
	fprintf(stdout, ", NULL");
    else
	fprintf(stdout, ", %s", (char *) prefix);
    if (URI == NULL)
	fprintf(stdout, ", NULL");
    else
	fprintf(stdout, ", '%s'", (char *) URI);
    fprintf(stdout, ", %d", nb_namespaces);

    if (namespaces != NULL) {
        for (i = 0;i < nb_namespaces * 2;i++) {
	    fprintf(stdout, ", xmlns");
	    if (namespaces[i] != NULL)
	        fprintf(stdout, ":%s", namespaces[i]);
	    i++;
	    fprintf(stdout, "='%s'", namespaces[i]);
	}
    }
    fprintf(stdout, ", %d, %d", nb_attributes, nb_defaulted);
    if (attributes != NULL) {
        for (i = 0;i < nb_attributes * 5;i += 5) {
	    if (attributes[i + 1] != NULL)
		fprintf(stdout, ", %s:%s='", attributes[i + 1], attributes[i]);
	    else
		fprintf(stdout, ", %s='", attributes[i]);
	    fprintf(stdout, "%.4s...', %d", attributes[i + 3],
		    (int)(attributes[i + 4] - attributes[i + 3]));
	}
    }
    fprintf(stdout, ")\n");
}

/**
 * endElementDebug:
 * @ctxt:  An XML parser context
 * @name:  The element name
 *
 * called when the end of an element has been detected.
 */
static void
endElementNsDebug(void *ctx ATTRIBUTE_UNUSED,
                  const xmlChar *localname,
                  const xmlChar *prefix,
                  const xmlChar *URI)
{
    callbacks++;
    if (noout)
	return;
    fprintf(stdout, "SAX.endElementNs(%s", (char *) localname);
    if (prefix == NULL)
	fprintf(stdout, ", NULL");
    else
	fprintf(stdout, ", %s", (char *) prefix);
    if (URI == NULL)
	fprintf(stdout, ", NULL)\n");
    else
	fprintf(stdout, ", '%s')\n", (char *) URI);
}

static xmlSAXHandler debugSAX2HandlerStruct = {
    internalSubsetDebug,
    isStandaloneDebug,
    hasInternalSubsetDebug,
    hasExternalSubsetDebug,
    resolveEntityDebug,
    getEntityDebug,
    entityDeclDebug,
    notationDeclDebug,
    attributeDeclDebug,
    elementDeclDebug,
    unparsedEntityDeclDebug,
    setDocumentLocatorDebug,
    startDocumentDebug,
    endDocumentDebug,
    NULL,
    NULL,
    referenceDebug,
    charactersDebug,
    ignorableWhitespaceDebug,
    processingInstructionDebug,
    commentDebug,
    warningDebug,
    errorDebug,
    fatalErrorDebug,
    getParameterEntityDebug,
    cdataBlockDebug,
    externalSubsetDebug,
    XML_SAX2_MAGIC,
    NULL,
    startElementNsDebug,
    endElementNsDebug,
    NULL
};

static xmlSAXHandlerPtr debugSAX2Handler = &debugSAX2HandlerStruct;

static void
testSAX(const char *filename) {
    xmlSAXHandlerPtr handler;
    const char *user_data = "user_data"; /* mostly for debugging */
    xmlParserInputBufferPtr buf = NULL;
    xmlParserInputPtr inputStream;
    xmlParserCtxtPtr ctxt = NULL;
    xmlSAXHandlerPtr old_sax = NULL;

    callbacks = 0;

    if (noout) {
        handler = emptySAXHandler;
#ifdef LIBXML_SAX1_ENABLED
    } else if (sax1) {
        handler = debugSAXHandler;
#endif
    } else {
        handler = debugSAX2Handler;
    }

    /*
     * it's not the simplest code but the most generic in term of I/O
     */
    buf = xmlParserInputBufferCreateFilename(filename, XML_CHAR_ENCODING_NONE);
    if (buf == NULL) {
        goto error;
    }

#ifdef LIBXML_SCHEMAS_ENABLED
    if (wxschemas != NULL) {
        int ret;
	xmlSchemaValidCtxtPtr vctxt;

	vctxt = xmlSchemaNewValidCtxt(wxschemas);
	xmlSchemaSetValidErrors(vctxt,
		(xmlSchemaValidityErrorFunc) fprintf,
		(xmlSchemaValidityWarningFunc) fprintf,
		stderr);
	xmlSchemaValidateSetFilename(vctxt, filename);

	ret = xmlSchemaValidateStream(vctxt, buf, 0, handler,
	                              (void *)user_data);
	if (repeat == 0) {
	    if (ret == 0) {
		fprintf(stderr, "%s validates\n", filename);
	    } else if (ret > 0) {
		fprintf(stderr, "%s fails to validate\n", filename);
		progresult = XMLLINT_ERR_VALID;
	    } else {
		fprintf(stderr, "%s validation generated an internal error\n",
		       filename);
		progresult = XMLLINT_ERR_VALID;
	    }
	}
	xmlSchemaFreeValidCtxt(vctxt);
    } else
#endif
    {
	/*
	 * Create the parser context amd hook the input
	 */
	ctxt = xmlNewParserCtxt();
	if (ctxt == NULL) {
	    xmlFreeParserInputBuffer(buf);
	    goto error;
	}
	old_sax = ctxt->sax;
	ctxt->sax = handler;
	ctxt->userData = (void *) user_data;
	inputStream = xmlNewIOInputStream(ctxt, buf, XML_CHAR_ENCODING_NONE);
	if (inputStream == NULL) {
	    xmlFreeParserInputBuffer(buf);
	    goto error;
	}
	inputPush(ctxt, inputStream);

	/* do the parsing */
	xmlParseDocument(ctxt);

	if (ctxt->myDoc != NULL) {
	    fprintf(stderr, "SAX generated a doc !\n");
	    xmlFreeDoc(ctxt->myDoc);
	    ctxt->myDoc = NULL;
	}
    }

error:
    if (ctxt != NULL) {
        ctxt->sax = old_sax;
        xmlFreeParserCtxt(ctxt);
    }
}

/************************************************************************
 *									*
 *			Stream Test processing				*
 *									*
 ************************************************************************/
#ifdef LIBXML_READER_ENABLED
static void processNode(xmlTextReaderPtr reader) {
    const xmlChar *name, *value;
    int type, empty;

    type = xmlTextReaderNodeType(reader);
    empty = xmlTextReaderIsEmptyElement(reader);

    if (debug) {
	name = xmlTextReaderConstName(reader);
	if (name == NULL)
	    name = BAD_CAST "--";

	value = xmlTextReaderConstValue(reader);


	printf("%d %d %s %d %d",
		xmlTextReaderDepth(reader),
		type,
		name,
		empty,
		xmlTextReaderHasValue(reader));
	if (value == NULL)
	    printf("\n");
	else {
	    printf(" %s\n", value);
	}
    }
#ifdef LIBXML_PATTERN_ENABLED
    if (patternc) {
        xmlChar *path = NULL;
        int match = -1;

	if (type == XML_READER_TYPE_ELEMENT) {
	    /* do the check only on element start */
	    match = xmlPatternMatch(patternc, xmlTextReaderCurrentNode(reader));

	    if (match) {
#if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_DEBUG_ENABLED)
		path = xmlGetNodePath(xmlTextReaderCurrentNode(reader));
		printf("Node %s matches pattern %s\n", path, pattern);
#else
                printf("Node %s matches pattern %s\n",
                       xmlTextReaderConstName(reader), pattern);
#endif
	    }
	}
	if (patstream != NULL) {
	    int ret;

	    if (type == XML_READER_TYPE_ELEMENT) {
		ret = xmlStreamPush(patstream,
		                    xmlTextReaderConstLocalName(reader),
				    xmlTextReaderConstNamespaceUri(reader));
		if (ret < 0) {
		    fprintf(stderr, "xmlStreamPush() failure\n");
                    xmlFreeStreamCtxt(patstream);
		    patstream = NULL;
		} else if (ret != match) {
#if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_DEBUG_ENABLED)
		    if (path == NULL) {
		        path = xmlGetNodePath(
		                       xmlTextReaderCurrentNode(reader));
		    }
#endif
		    fprintf(stderr,
		            "xmlPatternMatch and xmlStreamPush disagree\n");
                    if (path != NULL)
                        fprintf(stderr, "  pattern %s node %s\n",
                                pattern, path);
                    else
		        fprintf(stderr, "  pattern %s node %s\n",
			    pattern, xmlTextReaderConstName(reader));
		}

	    }
	    if ((type == XML_READER_TYPE_END_ELEMENT) ||
	        ((type == XML_READER_TYPE_ELEMENT) && (empty))) {
	        ret = xmlStreamPop(patstream);
		if (ret < 0) {
		    fprintf(stderr, "xmlStreamPop() failure\n");
                    xmlFreeStreamCtxt(patstream);
		    patstream = NULL;
		}
	    }
	}
	if (path != NULL)
	    xmlFree(path);
    }
#endif
}

static void streamFile(char *filename) {
    xmlTextReaderPtr reader;
    int ret;
#ifdef HAVE_MMAP
    int fd = -1;
    struct stat info;
    const char *base = NULL;
    xmlParserInputBufferPtr input = NULL;

    if (memory) {
	if (stat(filename, &info) < 0)
	    return;
	if ((fd = open(filename, O_RDONLY)) < 0)
	    return;
	base = mmap(NULL, info.st_size, PROT_READ, MAP_SHARED, fd, 0) ;
	if (base == (void *) MAP_FAILED) {
	    close(fd);
	    fprintf(stderr, "mmap failure for file %s\n", filename);
	    progresult = XMLLINT_ERR_RDFILE;
	    return;
	}

	reader = xmlReaderForMemory(base, info.st_size, filename,
	                            NULL, options);
    } else
#endif
	reader = xmlReaderForFile(filename, NULL, options);
#ifdef LIBXML_PATTERN_ENABLED
    if (pattern != NULL) {
        patternc = xmlPatterncompile((const xmlChar *) pattern, NULL, 0, NULL);
	if (patternc == NULL) {
	    xmlGenericError(xmlGenericErrorContext,
		    "Pattern %s failed to compile\n", pattern);
            progresult = XMLLINT_ERR_SCHEMAPAT;
	    pattern = NULL;
	}
    }
    if (patternc != NULL) {
        patstream = xmlPatternGetStreamCtxt(patternc);
	if (patstream != NULL) {
	    ret = xmlStreamPush(patstream, NULL, NULL);
	    if (ret < 0) {
		fprintf(stderr, "xmlStreamPush() failure\n");
		xmlFreeStreamCtxt(patstream);
		patstream = NULL;
            }
	}
    }
#endif


    if (reader != NULL) {
#ifdef LIBXML_VALID_ENABLED
	if (valid)
	    xmlTextReaderSetParserProp(reader, XML_PARSER_VALIDATE, 1);
	else
#endif /* LIBXML_VALID_ENABLED */
	    if (loaddtd)
		xmlTextReaderSetParserProp(reader, XML_PARSER_LOADDTD, 1);
#ifdef LIBXML_SCHEMAS_ENABLED
	if (relaxng != NULL) {
	    if ((timing) && (!repeat)) {
		startTimer();
	    }
	    ret = xmlTextReaderRelaxNGValidate(reader, relaxng);
	    if (ret < 0) {
		xmlGenericError(xmlGenericErrorContext,
			"Relax-NG schema %s failed to compile\n", relaxng);
		progresult = XMLLINT_ERR_SCHEMACOMP;
		relaxng = NULL;
	    }
	    if ((timing) && (!repeat)) {
		endTimer("Compiling the schemas");
	    }
	}
	if (schema != NULL) {
	    if ((timing) && (!repeat)) {
		startTimer();
	    }
	    ret = xmlTextReaderSchemaValidate(reader, schema);
	    if (ret < 0) {
		xmlGenericError(xmlGenericErrorContext,
			"XSD schema %s failed to compile\n", schema);
		progresult = XMLLINT_ERR_SCHEMACOMP;
		schema = NULL;
	    }
	    if ((timing) && (!repeat)) {
		endTimer("Compiling the schemas");
	    }
	}
#endif

	/*
	 * Process all nodes in sequence
	 */
	if ((timing) && (!repeat)) {
	    startTimer();
	}
	ret = xmlTextReaderRead(reader);
	while (ret == 1) {
	    if ((debug)
#ifdef LIBXML_PATTERN_ENABLED
	        || (patternc)
#endif
	       )
		processNode(reader);
	    ret = xmlTextReaderRead(reader);
	}
	if ((timing) && (!repeat)) {
#ifdef LIBXML_SCHEMAS_ENABLED
	    if (relaxng != NULL)
		endTimer("Parsing and validating");
	    else
#endif
#ifdef LIBXML_VALID_ENABLED
	    if (valid)
		endTimer("Parsing and validating");
	    else
#endif
	    endTimer("Parsing");
	}

#ifdef LIBXML_VALID_ENABLED
	if (valid) {
	    if (xmlTextReaderIsValid(reader) != 1) {
		xmlGenericError(xmlGenericErrorContext,
			"Document %s does not validate\n", filename);
		progresult = XMLLINT_ERR_VALID;
	    }
	}
#endif /* LIBXML_VALID_ENABLED */
#ifdef LIBXML_SCHEMAS_ENABLED
	if ((relaxng != NULL) || (schema != NULL)) {
	    if (xmlTextReaderIsValid(reader) != 1) {
		fprintf(stderr, "%s fails to validate\n", filename);
		progresult = XMLLINT_ERR_VALID;
	    } else {
		fprintf(stderr, "%s validates\n", filename);
	    }
	}
#endif
	/*
	 * Done, cleanup and status
	 */
	xmlFreeTextReader(reader);
	if (ret != 0) {
	    fprintf(stderr, "%s : failed to parse\n", filename);
	    progresult = XMLLINT_ERR_UNCLASS;
	}
    } else {
	fprintf(stderr, "Unable to open %s\n", filename);
	progresult = XMLLINT_ERR_UNCLASS;
    }
#ifdef LIBXML_PATTERN_ENABLED
    if (patstream != NULL) {
	xmlFreeStreamCtxt(patstream);
	patstream = NULL;
    }
#endif
#ifdef HAVE_MMAP
    if (memory) {
        xmlFreeParserInputBuffer(input);
	munmap((char *) base, info.st_size);
	close(fd);
    }
#endif
}

static void walkDoc(xmlDocPtr doc) {
    xmlTextReaderPtr reader;
    int ret;

#ifdef LIBXML_PATTERN_ENABLED
    xmlNodePtr root;
    const xmlChar *namespaces[22];
    int i;
    xmlNsPtr ns;

    root = xmlDocGetRootElement(doc);
    if (root == NULL ) {
        xmlGenericError(xmlGenericErrorContext,
                "Document does not have a root element");
        progresult = XMLLINT_ERR_UNCLASS;
        return;
    }
    for (ns = root->nsDef, i = 0;ns != NULL && i < 20;ns=ns->next) {
        namespaces[i++] = ns->href;
        namespaces[i++] = ns->prefix;
    }
    namespaces[i++] = NULL;
    namespaces[i] = NULL;

    if (pattern != NULL) {
        patternc = xmlPatterncompile((const xmlChar *) pattern, doc->dict,
	                             0, &namespaces[0]);
	if (patternc == NULL) {
	    xmlGenericError(xmlGenericErrorContext,
		    "Pattern %s failed to compile\n", pattern);
            progresult = XMLLINT_ERR_SCHEMAPAT;
	    pattern = NULL;
	}
    }
    if (patternc != NULL) {
        patstream = xmlPatternGetStreamCtxt(patternc);
	if (patstream != NULL) {
	    ret = xmlStreamPush(patstream, NULL, NULL);
	    if (ret < 0) {
		fprintf(stderr, "xmlStreamPush() failure\n");
		xmlFreeStreamCtxt(patstream);
		patstream = NULL;
            }
	}
    }
#endif /* LIBXML_PATTERN_ENABLED */
    reader = xmlReaderWalker(doc);
    if (reader != NULL) {
	if ((timing) && (!repeat)) {
	    startTimer();
	}
	ret = xmlTextReaderRead(reader);
	while (ret == 1) {
	    if ((debug)
#ifdef LIBXML_PATTERN_ENABLED
	        || (patternc)
#endif
	       )
		processNode(reader);
	    ret = xmlTextReaderRead(reader);
	}
	if ((timing) && (!repeat)) {
	    endTimer("walking through the doc");
	}
	xmlFreeTextReader(reader);
	if (ret != 0) {
	    fprintf(stderr, "failed to walk through the doc\n");
	    progresult = XMLLINT_ERR_UNCLASS;
	}
    } else {
	fprintf(stderr, "Failed to crate a reader from the document\n");
	progresult = XMLLINT_ERR_UNCLASS;
    }
#ifdef LIBXML_PATTERN_ENABLED
    if (patstream != NULL) {
	xmlFreeStreamCtxt(patstream);
	patstream = NULL;
    }
#endif
}
#endif /* LIBXML_READER_ENABLED */

#ifdef LIBXML_XPATH_ENABLED
/************************************************************************
 *									*
 *			XPath Query                                     *
 *									*
 ************************************************************************/

static void doXPathDump(xmlXPathObjectPtr cur) {
    switch(cur->type) {
        case XPATH_NODESET: {
            int i;
            xmlNodePtr node;
#ifdef LIBXML_OUTPUT_ENABLED
            xmlSaveCtxtPtr ctxt;

            if ((cur->nodesetval == NULL) || (cur->nodesetval->nodeNr <= 0)) {
                fprintf(stderr, "XPath set is empty\n");
                progresult = XMLLINT_ERR_XPATH;
                break;
            }
            ctxt = xmlSaveToFd(1, NULL, 0);
            if (ctxt == NULL) {
                fprintf(stderr, "Out of memory for XPath\n");
                progresult = XMLLINT_ERR_MEM;
                return;
            }
            for (i = 0;i < cur->nodesetval->nodeNr;i++) {
                node = cur->nodesetval->nodeTab[i];
                xmlSaveTree(ctxt, node);
            }
            xmlSaveClose(ctxt);
#else
            printf("xpath returned %d nodes\n", cur->nodesetval->nodeNr);
#endif
	    break;
        }
        case XPATH_BOOLEAN:
	    if (cur->boolval) printf("true");
	    else printf("false");
	    break;
        case XPATH_NUMBER:
	    switch (xmlXPathIsInf(cur->floatval)) {
	    case 1:
		printf("Infinity");
		break;
	    case -1:
		printf("-Infinity");
		break;
	    default:
		if (xmlXPathIsNaN(cur->floatval)) {
		    printf("NaN");
		} else {
		    printf("%0g", cur->floatval);
		}
	    }
	    break;
        case XPATH_STRING:
	    printf("%s", (const char *) cur->stringval);
	    break;
        case XPATH_UNDEFINED:
	    fprintf(stderr, "XPath Object is uninitialized\n");
            progresult = XMLLINT_ERR_XPATH;
	    break;
	default:
	    fprintf(stderr, "XPath object of unexpected type\n");
            progresult = XMLLINT_ERR_XPATH;
	    break;
    }
}

static void doXPathQuery(xmlDocPtr doc, const char *query) {
    xmlXPathContextPtr ctxt;
    xmlXPathObjectPtr res;

    ctxt = xmlXPathNewContext(doc);
    if (ctxt == NULL) {
        fprintf(stderr, "Out of memory for XPath\n");
        progresult = XMLLINT_ERR_MEM;
        return;
    }
    ctxt->node = (xmlNodePtr) doc;
    res = xmlXPathEval(BAD_CAST query, ctxt);
    xmlXPathFreeContext(ctxt);

    if (res == NULL) {
        fprintf(stderr, "XPath evaluation failure\n");
        progresult = XMLLINT_ERR_XPATH;
        return;
    }
    doXPathDump(res);
    xmlXPathFreeObject(res);
}
#endif /* LIBXML_XPATH_ENABLED */

/************************************************************************
 *									*
 *			Tree Test processing				*
 *									*
 ************************************************************************/
static void parseAndPrintFile(char *filename, xmlParserCtxtPtr rectxt) {
    xmlDocPtr doc = NULL;
#ifdef LIBXML_TREE_ENABLED
    xmlDocPtr tmp;
#endif /* LIBXML_TREE_ENABLED */

    if ((timing) && (!repeat))
	startTimer();


#ifdef LIBXML_TREE_ENABLED
    if (filename == NULL) {
	if (generate) {
	    xmlNodePtr n;

	    doc = xmlNewDoc(BAD_CAST "1.0");
	    n = xmlNewDocNode(doc, NULL, BAD_CAST "info", NULL);
	    xmlNodeSetContent(n, BAD_CAST "abc");
	    xmlDocSetRootElement(doc, n);
	}
    }
#endif /* LIBXML_TREE_ENABLED */
#ifdef LIBXML_HTML_ENABLED
#ifdef LIBXML_PUSH_ENABLED
    else if ((html) && (push)) {
        FILE *f;

#if defined(_WIN32) || defined (__DJGPP__) && !defined (__CYGWIN__)
	f = fopen(filename, "rb");
#elif defined(__OS400__)
	f = fopen(filename, "rb");
#else
	f = fopen(filename, "r");
#endif
        if (f != NULL) {
            int res;
            char chars[4096];
            htmlParserCtxtPtr ctxt;

            res = fread(chars, 1, 4, f);
            if (res > 0) {
                ctxt = htmlCreatePushParserCtxt(NULL, NULL,
                            chars, res, filename, XML_CHAR_ENCODING_NONE);
                xmlCtxtUseOptions(ctxt, options);
                while ((res = fread(chars, 1, pushsize, f)) > 0) {
                    htmlParseChunk(ctxt, chars, res, 0);
                }
                htmlParseChunk(ctxt, chars, 0, 1);
                doc = ctxt->myDoc;
                htmlFreeParserCtxt(ctxt);
            }
            fclose(f);
        }
    }
#endif /* LIBXML_PUSH_ENABLED */
#ifdef HAVE_MMAP
    else if ((html) && (memory)) {
	int fd;
	struct stat info;
	const char *base;
	if (stat(filename, &info) < 0)
	    return;
	if ((fd = open(filename, O_RDONLY)) < 0)
	    return;
	base = mmap(NULL, info.st_size, PROT_READ, MAP_SHARED, fd, 0) ;
	if (base == (void *) MAP_FAILED) {
	    close(fd);
	    fprintf(stderr, "mmap failure for file %s\n", filename);
	    progresult = XMLLINT_ERR_RDFILE;
	    return;
	}

	doc = htmlReadMemory((char *) base, info.st_size, filename,
	                     NULL, options);

	munmap((char *) base, info.st_size);
	close(fd);
    }
#endif
    else if (html) {
	doc = htmlReadFile(filename, NULL, options);
    }
#endif /* LIBXML_HTML_ENABLED */
    else {
#ifdef LIBXML_PUSH_ENABLED
	/*
	 * build an XML tree from a string;
	 */
	if (push) {
	    FILE *f;

	    /* '-' Usually means stdin -<sven@zen.org> */
	    if ((filename[0] == '-') && (filename[1] == 0)) {
	      f = stdin;
	    } else {
#if defined(_WIN32) || defined (__DJGPP__) && !defined (__CYGWIN__)
		f = fopen(filename, "rb");
#elif defined(__OS400__)
		f = fopen(filename, "rb");
#else
		f = fopen(filename, "r");
#endif
	    }
	    if (f != NULL) {
		int ret;
	        int res, size = 1024;
	        char chars[1024];
                xmlParserCtxtPtr ctxt;

		/* if (repeat) size = 1024; */
		res = fread(chars, 1, 4, f);
		if (res > 0) {
		    ctxt = xmlCreatePushParserCtxt(NULL, NULL,
		                chars, res, filename);
		    xmlCtxtUseOptions(ctxt, options);
		    while ((res = fread(chars, 1, size, f)) > 0) {
			xmlParseChunk(ctxt, chars, res, 0);
		    }
		    xmlParseChunk(ctxt, chars, 0, 1);
		    doc = ctxt->myDoc;
		    ret = ctxt->wellFormed;
		    xmlFreeParserCtxt(ctxt);
		    if (!ret) {
			xmlFreeDoc(doc);
			doc = NULL;
		    }
	        }
                if (f != stdin)
                    fclose(f);
	    }
	} else
#endif /* LIBXML_PUSH_ENABLED */
        if (testIO) {
	    if ((filename[0] == '-') && (filename[1] == 0)) {
	        doc = xmlReadFd(0, NULL, NULL, options);
	    } else {
	        FILE *f;

#if defined(_WIN32) || defined (__DJGPP__) && !defined (__CYGWIN__)
		f = fopen(filename, "rb");
#elif defined(__OS400__)
		f = fopen(filename, "rb");
#else
		f = fopen(filename, "r");
#endif
		if (f != NULL) {
		    if (rectxt == NULL)
			doc = xmlReadIO((xmlInputReadCallback) myRead,
					(xmlInputCloseCallback) myClose, f,
					filename, NULL, options);
		    else
			doc = xmlCtxtReadIO(rectxt,
			                (xmlInputReadCallback) myRead,
					(xmlInputCloseCallback) myClose, f,
					filename, NULL, options);
		} else
		    doc = NULL;
	    }
	} else if (htmlout) {
	    xmlParserCtxtPtr ctxt;

	    if (rectxt == NULL)
		ctxt = xmlNewParserCtxt();
	    else
	        ctxt = rectxt;
	    if (ctxt == NULL) {
	        doc = NULL;
	    } else {
	        ctxt->sax->error = xmlHTMLError;
	        ctxt->sax->warning = xmlHTMLWarning;
	        ctxt->vctxt.error = xmlHTMLValidityError;
	        ctxt->vctxt.warning = xmlHTMLValidityWarning;

		doc = xmlCtxtReadFile(ctxt, filename, NULL, options);

		if (rectxt == NULL)
		    xmlFreeParserCtxt(ctxt);
	    }
#ifdef HAVE_MMAP
	} else if (memory) {
	    int fd;
	    struct stat info;
	    const char *base;
	    if (stat(filename, &info) < 0)
		return;
	    if ((fd = open(filename, O_RDONLY)) < 0)
		return;
	    base = mmap(NULL, info.st_size, PROT_READ, MAP_SHARED, fd, 0) ;
	    if (base == (void *) MAP_FAILED) {
	        close(fd);
	        fprintf(stderr, "mmap failure for file %s\n", filename);
		progresult = XMLLINT_ERR_RDFILE;
	        return;
	    }

	    if (rectxt == NULL)
		doc = xmlReadMemory((char *) base, info.st_size,
		                    filename, NULL, options);
	    else
		doc = xmlCtxtReadMemory(rectxt, (char *) base, info.st_size,
			                filename, NULL, options);

	    munmap((char *) base, info.st_size);
	    close(fd);
#endif
#ifdef LIBXML_VALID_ENABLED
	} else if (valid) {
	    xmlParserCtxtPtr ctxt = NULL;

	    if (rectxt == NULL)
		ctxt = xmlNewParserCtxt();
	    else
	        ctxt = rectxt;
	    if (ctxt == NULL) {
	        doc = NULL;
	    } else {
		doc = xmlCtxtReadFile(ctxt, filename, NULL, options);

		if (ctxt->valid == 0)
		    progresult = XMLLINT_ERR_RDFILE;
		if (rectxt == NULL)
		    xmlFreeParserCtxt(ctxt);
	    }
#endif /* LIBXML_VALID_ENABLED */
	} else {
	    if (rectxt != NULL)
	        doc = xmlCtxtReadFile(rectxt, filename, NULL, options);
	    else {
#ifdef LIBXML_SAX1_ENABLED
                if (sax1)
		    doc = xmlParseFile(filename);
		else
#endif /* LIBXML_SAX1_ENABLED */
		doc = xmlReadFile(filename, NULL, options);
	    }
	}
    }

    /*
     * If we don't have a document we might as well give up.  Do we
     * want an error message here?  <sven@zen.org> */
    if (doc == NULL) {
	progresult = XMLLINT_ERR_UNCLASS;
	return;
    }

    if ((timing) && (!repeat)) {
	endTimer("Parsing");
    }

    /*
     * Remove DOCTYPE nodes
     */
    if (dropdtd) {
	xmlDtdPtr dtd;

	dtd = xmlGetIntSubset(doc);
	if (dtd != NULL) {
	    xmlUnlinkNode((xmlNodePtr)dtd);
	    xmlFreeDtd(dtd);
	}
    }

#ifdef LIBXML_XINCLUDE_ENABLED
    if (xinclude) {
	if ((timing) && (!repeat)) {
	    startTimer();
	}
	if (xmlXIncludeProcessFlags(doc, options) < 0)
	    progresult = XMLLINT_ERR_UNCLASS;
	if ((timing) && (!repeat)) {
	    endTimer("Xinclude processing");
	}
    }
#endif

#ifdef LIBXML_XPATH_ENABLED
    if (xpathquery != NULL) {
        doXPathQuery(doc, xpathquery);
    }
#endif

#ifdef LIBXML_DEBUG_ENABLED
#ifdef LIBXML_XPATH_ENABLED
    /*
     * shell interaction
     */
    if (shell) {
        xmlXPathOrderDocElems(doc);
        xmlShell(doc, filename, xmlShellReadline, stdout);
    }
#endif
#endif

#ifdef LIBXML_TREE_ENABLED
    /*
     * test intermediate copy if needed.
     */
    if (copy) {
        tmp = doc;
	if (timing) {
	    startTimer();
	}
	doc = xmlCopyDoc(doc, 1);
	if (timing) {
	    endTimer("Copying");
	}
	if (timing) {
	    startTimer();
	}
	xmlFreeDoc(tmp);
	if (timing) {
	    endTimer("Freeing original");
	}
    }
#endif /* LIBXML_TREE_ENABLED */

#ifdef LIBXML_VALID_ENABLED
    if ((insert) && (!html)) {
        const xmlChar* list[256];
	int nb, i;
	xmlNodePtr node;

	if (doc->children != NULL) {
	    node = doc->children;
	    while ((node != NULL) && (node->last == NULL)) node = node->next;
	    if (node != NULL) {
		nb = xmlValidGetValidElements(node->last, NULL, list, 256);
		if (nb < 0) {
		    fprintf(stderr, "could not get valid list of elements\n");
		} else if (nb == 0) {
		    fprintf(stderr, "No element can be inserted under root\n");
		} else {
		    fprintf(stderr, "%d element types can be inserted under root:\n",
		           nb);
		    for (i = 0;i < nb;i++) {
			 fprintf(stderr, "%s\n", (char *) list[i]);
		    }
		}
	    }
	}
    }else
#endif /* LIBXML_VALID_ENABLED */
#ifdef LIBXML_READER_ENABLED
    if (walker) {
        walkDoc(doc);
    }
#endif /* LIBXML_READER_ENABLED */
#ifdef LIBXML_OUTPUT_ENABLED
    if (noout == 0) {
        int ret;

	/*
	 * print it.
	 */
#ifdef LIBXML_DEBUG_ENABLED
	if (!debug) {
#endif
	    if ((timing) && (!repeat)) {
		startTimer();
	    }
#ifdef LIBXML_HTML_ENABLED
            if ((html) && (!xmlout)) {
		if (compress) {
		    htmlSaveFile(output ? output : "-", doc);
		}
		else if (encoding != NULL) {
		    if (format == 1) {
			htmlSaveFileFormat(output ? output : "-", doc, encoding, 1);
		    }
		    else {
			htmlSaveFileFormat(output ? output : "-", doc, encoding, 0);
		    }
		}
		else if (format == 1) {
		    htmlSaveFileFormat(output ? output : "-", doc, NULL, 1);
		}
		else {
		    FILE *out;
		    if (output == NULL)
			out = stdout;
		    else {
			out = fopen(output,"wb");
		    }
		    if (out != NULL) {
			if (htmlDocDump(out, doc) < 0)
			    progresult = XMLLINT_ERR_OUT;

			if (output != NULL)
			    fclose(out);
		    } else {
			fprintf(stderr, "failed to open %s\n", output);
			progresult = XMLLINT_ERR_OUT;
		    }
		}
		if ((timing) && (!repeat)) {
		    endTimer("Saving");
		}
	    } else
#endif
#ifdef LIBXML_C14N_ENABLED
            if (canonical) {
	        xmlChar *result = NULL;
		int size;

		size = xmlC14NDocDumpMemory(doc, NULL, XML_C14N_1_0, NULL, 1, &result);
		if (size >= 0) {
		    if (write(1, result, size) == -1) {
		        fprintf(stderr, "Can't write data\n");
		    }
		    xmlFree(result);
		} else {
		    fprintf(stderr, "Failed to canonicalize\n");
		    progresult = XMLLINT_ERR_OUT;
		}
	    } else if (canonical_11) {
	        xmlChar *result = NULL;
		int size;

		size = xmlC14NDocDumpMemory(doc, NULL, XML_C14N_1_1, NULL, 1, &result);
		if (size >= 0) {
		    if (write(1, result, size) == -1) {
		        fprintf(stderr, "Can't write data\n");
		    }
		    xmlFree(result);
		} else {
		    fprintf(stderr, "Failed to canonicalize\n");
		    progresult = XMLLINT_ERR_OUT;
		}
	    } else
            if (exc_canonical) {
	        xmlChar *result = NULL;
		int size;

		size = xmlC14NDocDumpMemory(doc, NULL, XML_C14N_EXCLUSIVE_1_0, NULL, 1, &result);
		if (size >= 0) {
		    if (write(1, result, size) == -1) {
		        fprintf(stderr, "Can't write data\n");
		    }
		    xmlFree(result);
		} else {
		    fprintf(stderr, "Failed to canonicalize\n");
		    progresult = XMLLINT_ERR_OUT;
		}
	    } else
#endif
#ifdef HAVE_MMAP
	    if (memory) {
		xmlChar *result;
		int len;

		if (encoding != NULL) {
		    if (format == 1) {
		        xmlDocDumpFormatMemoryEnc(doc, &result, &len, encoding, 1);
		    } else {
			xmlDocDumpMemoryEnc(doc, &result, &len, encoding);
		    }
		} else {
		    if (format == 1)
			xmlDocDumpFormatMemory(doc, &result, &len, 1);
		    else
			xmlDocDumpMemory(doc, &result, &len);
		}
		if (result == NULL) {
		    fprintf(stderr, "Failed to save\n");
		    progresult = XMLLINT_ERR_OUT;
		} else {
		    if (write(1, result, len) == -1) {
		        fprintf(stderr, "Can't write data\n");
		    }
		    xmlFree(result);
		}

	    } else
#endif /* HAVE_MMAP */
	    if (compress) {
		xmlSaveFile(output ? output : "-", doc);
	    } else if (oldout) {
	        if (encoding != NULL) {
		    if (format == 1) {
			ret = xmlSaveFormatFileEnc(output ? output : "-", doc,
						   encoding, 1);
		    }
		    else {
			ret = xmlSaveFileEnc(output ? output : "-", doc,
			                     encoding);
		    }
		    if (ret < 0) {
			fprintf(stderr, "failed save to %s\n",
				output ? output : "-");
			progresult = XMLLINT_ERR_OUT;
		    }
		} else if (format == 1) {
		    ret = xmlSaveFormatFile(output ? output : "-", doc, 1);
		    if (ret < 0) {
			fprintf(stderr, "failed save to %s\n",
				output ? output : "-");
			progresult = XMLLINT_ERR_OUT;
		    }
		} else {
		    FILE *out;
		    if (output == NULL)
			out = stdout;
		    else {
			out = fopen(output,"wb");
		    }
		    if (out != NULL) {
			if (xmlDocDump(out, doc) < 0)
			    progresult = XMLLINT_ERR_OUT;

			if (output != NULL)
			    fclose(out);
		    } else {
			fprintf(stderr, "failed to open %s\n", output);
			progresult = XMLLINT_ERR_OUT;
		    }
		}
	    } else {
	        xmlSaveCtxtPtr ctxt;
		int saveOpts = 0;

                if (format == 1)
		    saveOpts |= XML_SAVE_FORMAT;
                else if (format == 2)
                    saveOpts |= XML_SAVE_WSNONSIG;

#if defined(LIBXML_HTML_ENABLED) || defined(LIBXML_VALID_ENABLED)
                if (xmlout)
                    saveOpts |= XML_SAVE_AS_XML;
#endif

		if (output == NULL)
		    ctxt = xmlSaveToFd(1, encoding, saveOpts);
		else
		    ctxt = xmlSaveToFilename(output, encoding, saveOpts);

		if (ctxt != NULL) {
		    if (xmlSaveDoc(ctxt, doc) < 0) {
			fprintf(stderr, "failed save to %s\n",
				output ? output : "-");
			progresult = XMLLINT_ERR_OUT;
		    }
		    xmlSaveClose(ctxt);
		} else {
		    progresult = XMLLINT_ERR_OUT;
		}
	    }
	    if ((timing) && (!repeat)) {
		endTimer("Saving");
	    }
#ifdef LIBXML_DEBUG_ENABLED
	} else {
	    FILE *out;
	    if (output == NULL)
	        out = stdout;
	    else {
		out = fopen(output,"wb");
	    }
	    if (out != NULL) {
		xmlDebugDumpDocument(out, doc);

		if (output != NULL)
		    fclose(out);
	    } else {
		fprintf(stderr, "failed to open %s\n", output);
		progresult = XMLLINT_ERR_OUT;
	    }
	}
#endif
    }
#endif /* LIBXML_OUTPUT_ENABLED */

#ifdef LIBXML_VALID_ENABLED
    /*
     * A posteriori validation test
     */
    if ((dtdvalid != NULL) || (dtdvalidfpi != NULL)) {
	xmlDtdPtr dtd;

	if ((timing) && (!repeat)) {
	    startTimer();
	}
	if (dtdvalid != NULL)
	    dtd = xmlParseDTD(NULL, (const xmlChar *)dtdvalid);
	else
	    dtd = xmlParseDTD((const xmlChar *)dtdvalidfpi, NULL);
	if ((timing) && (!repeat)) {
	    endTimer("Parsing DTD");
	}
	if (dtd == NULL) {
	    if (dtdvalid != NULL)
		xmlGenericError(xmlGenericErrorContext,
			"Could not parse DTD %s\n", dtdvalid);
	    else
		xmlGenericError(xmlGenericErrorContext,
			"Could not parse DTD %s\n", dtdvalidfpi);
	    progresult = XMLLINT_ERR_DTD;
	} else {
	    xmlValidCtxtPtr cvp;

	    if ((cvp = xmlNewValidCtxt()) == NULL) {
		xmlGenericError(xmlGenericErrorContext,
			"Couldn't allocate validation context\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(-1);}
	    }
	    cvp->userData = (void *) stderr;
	    cvp->error    = (xmlValidityErrorFunc) fprintf;
	    cvp->warning  = (xmlValidityWarningFunc) fprintf;

	    if ((timing) && (!repeat)) {
		startTimer();
	    }
	    if (!xmlValidateDtd(cvp, doc, dtd)) {
		if (dtdvalid != NULL)
		    xmlGenericError(xmlGenericErrorContext,
			    "Document %s does not validate against %s\n",
			    filename, dtdvalid);
		else
		    xmlGenericError(xmlGenericErrorContext,
			    "Document %s does not validate against %s\n",
			    filename, dtdvalidfpi);
		progresult = XMLLINT_ERR_VALID;
	    }
	    if ((timing) && (!repeat)) {
		endTimer("Validating against DTD");
	    }
	    xmlFreeValidCtxt(cvp);
	    xmlFreeDtd(dtd);
	}
    } else if (postvalid) {
	xmlValidCtxtPtr cvp;

	if ((cvp = xmlNewValidCtxt()) == NULL) {
	    xmlGenericError(xmlGenericErrorContext,
		    "Couldn't allocate validation context\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(-1);}
	}

	if ((timing) && (!repeat)) {
	    startTimer();
	}
	cvp->userData = (void *) stderr;
	cvp->error    = (xmlValidityErrorFunc) fprintf;
	cvp->warning  = (xmlValidityWarningFunc) fprintf;
	if (!xmlValidateDocument(cvp, doc)) {
	    xmlGenericError(xmlGenericErrorContext,
		    "Document %s does not validate\n", filename);
	    progresult = XMLLINT_ERR_VALID;
	}
	if ((timing) && (!repeat)) {
	    endTimer("Validating");
	}
	xmlFreeValidCtxt(cvp);
    }
#endif /* LIBXML_VALID_ENABLED */
#ifdef LIBXML_SCHEMATRON_ENABLED
    if (wxschematron != NULL) {
	xmlSchematronValidCtxtPtr ctxt;
	int ret;
	int flag;

	if ((timing) && (!repeat)) {
	    startTimer();
	}

	if (debug)
	    flag = XML_SCHEMATRON_OUT_XML;
	else
	    flag = XML_SCHEMATRON_OUT_TEXT;
	if (noout)
	    flag |= XML_SCHEMATRON_OUT_QUIET;
	ctxt = xmlSchematronNewValidCtxt(wxschematron, flag);
#if 0
	xmlSchematronSetValidErrors(ctxt,
		(xmlSchematronValidityErrorFunc) fprintf,
		(xmlSchematronValidityWarningFunc) fprintf,
		stderr);
#endif
	ret = xmlSchematronValidateDoc(ctxt, doc);
	if (ret == 0) {
	    fprintf(stderr, "%s validates\n", filename);
	} else if (ret > 0) {
	    fprintf(stderr, "%s fails to validate\n", filename);
	    progresult = XMLLINT_ERR_VALID;
	} else {
	    fprintf(stderr, "%s validation generated an internal error\n",
		   filename);
	    progresult = XMLLINT_ERR_VALID;
	}
	xmlSchematronFreeValidCtxt(ctxt);
	if ((timing) && (!repeat)) {
	    endTimer("Validating");
	}
    }
#endif
#ifdef LIBXML_SCHEMAS_ENABLED
    if (relaxngschemas != NULL) {
	xmlRelaxNGValidCtxtPtr ctxt;
	int ret;

	if ((timing) && (!repeat)) {
	    startTimer();
	}

	ctxt = xmlRelaxNGNewValidCtxt(relaxngschemas);
	xmlRelaxNGSetValidErrors(ctxt,
		(xmlRelaxNGValidityErrorFunc) fprintf,
		(xmlRelaxNGValidityWarningFunc) fprintf,
		stderr);
	ret = xmlRelaxNGValidateDoc(ctxt, doc);
	if (ret == 0) {
	    fprintf(stderr, "%s validates\n", filename);
	} else if (ret > 0) {
	    fprintf(stderr, "%s fails to validate\n", filename);
	    progresult = XMLLINT_ERR_VALID;
	} else {
	    fprintf(stderr, "%s validation generated an internal error\n",
		   filename);
	    progresult = XMLLINT_ERR_VALID;
	}
	xmlRelaxNGFreeValidCtxt(ctxt);
	if ((timing) && (!repeat)) {
	    endTimer("Validating");
	}
    } else if (wxschemas != NULL) {
	xmlSchemaValidCtxtPtr ctxt;
	int ret;

	if ((timing) && (!repeat)) {
	    startTimer();
	}

	ctxt = xmlSchemaNewValidCtxt(wxschemas);
	xmlSchemaSetValidErrors(ctxt,
		(xmlSchemaValidityErrorFunc) fprintf,
		(xmlSchemaValidityWarningFunc) fprintf,
		stderr);
	ret = xmlSchemaValidateDoc(ctxt, doc);
	if (ret == 0) {
	    fprintf(stderr, "%s validates\n", filename);
	} else if (ret > 0) {
	    fprintf(stderr, "%s fails to validate\n", filename);
	    progresult = XMLLINT_ERR_VALID;
	} else {
	    fprintf(stderr, "%s validation generated an internal error\n",
		   filename);
	    progresult = XMLLINT_ERR_VALID;
	}
	xmlSchemaFreeValidCtxt(ctxt);
	if ((timing) && (!repeat)) {
	    endTimer("Validating");
	}
    }
#endif

#ifdef LIBXML_DEBUG_ENABLED
#if defined(LIBXML_HTML_ENABLED) || defined(LIBXML_VALID_ENABLED)
    if ((debugent) && (!html))
	xmlDebugDumpEntities(stderr, doc);
#endif
#endif

    /*
     * free it.
     */
    if ((timing) && (!repeat)) {
	startTimer();
    }
    xmlFreeDoc(doc);
    if ((timing) && (!repeat)) {
	endTimer("Freeing");
    }
}

/************************************************************************
 *									*
 *			Usage and Main					*
 *									*
 ************************************************************************/

static void showVersion(const char *name) {
    fprintf(stderr, "%s: using libxml version %s\n", name, xmlParserVersion);
    fprintf(stderr, "   compiled with: ");
    if (xmlHasFeature(XML_WITH_THREAD)) fprintf(stderr, "Threads ");
    if (xmlHasFeature(XML_WITH_TREE)) fprintf(stderr, "Tree ");
    if (xmlHasFeature(XML_WITH_OUTPUT)) fprintf(stderr, "Output ");
    if (xmlHasFeature(XML_WITH_PUSH)) fprintf(stderr, "Push ");
    if (xmlHasFeature(XML_WITH_READER)) fprintf(stderr, "Reader ");
    if (xmlHasFeature(XML_WITH_PATTERN)) fprintf(stderr, "Patterns ");
    if (xmlHasFeature(XML_WITH_WRITER)) fprintf(stderr, "Writer ");
    if (xmlHasFeature(XML_WITH_SAX1)) fprintf(stderr, "SAXv1 ");
    if (xmlHasFeature(XML_WITH_FTP)) fprintf(stderr, "FTP ");
    if (xmlHasFeature(XML_WITH_HTTP)) fprintf(stderr, "HTTP ");
    if (xmlHasFeature(XML_WITH_VALID)) fprintf(stderr, "DTDValid ");
    if (xmlHasFeature(XML_WITH_HTML)) fprintf(stderr, "HTML ");
    if (xmlHasFeature(XML_WITH_LEGACY)) fprintf(stderr, "Legacy ");
    if (xmlHasFeature(XML_WITH_C14N)) fprintf(stderr, "C14N ");
    if (xmlHasFeature(XML_WITH_CATALOG)) fprintf(stderr, "Catalog ");
    if (xmlHasFeature(XML_WITH_XPATH)) fprintf(stderr, "XPath ");
    if (xmlHasFeature(XML_WITH_XPTR)) fprintf(stderr, "XPointer ");
    if (xmlHasFeature(XML_WITH_XINCLUDE)) fprintf(stderr, "XInclude ");
    if (xmlHasFeature(XML_WITH_ICONV)) fprintf(stderr, "Iconv ");
    if (xmlHasFeature(XML_WITH_ICU)) fprintf(stderr, "ICU ");
    if (xmlHasFeature(XML_WITH_ISO8859X)) fprintf(stderr, "ISO8859X ");
    if (xmlHasFeature(XML_WITH_UNICODE)) fprintf(stderr, "Unicode ");
    if (xmlHasFeature(XML_WITH_REGEXP)) fprintf(stderr, "Regexps ");
    if (xmlHasFeature(XML_WITH_AUTOMATA)) fprintf(stderr, "Automata ");
    if (xmlHasFeature(XML_WITH_EXPR)) fprintf(stderr, "Expr ");
    if (xmlHasFeature(XML_WITH_SCHEMAS)) fprintf(stderr, "Schemas ");
    if (xmlHasFeature(XML_WITH_SCHEMATRON)) fprintf(stderr, "Schematron ");
    if (xmlHasFeature(XML_WITH_MODULES)) fprintf(stderr, "Modules ");
    if (xmlHasFeature(XML_WITH_DEBUG)) fprintf(stderr, "Debug ");
    if (xmlHasFeature(XML_WITH_DEBUG_MEM)) fprintf(stderr, "MemDebug ");
    if (xmlHasFeature(XML_WITH_DEBUG_RUN)) fprintf(stderr, "RunDebug ");
    if (xmlHasFeature(XML_WITH_ZLIB)) fprintf(stderr, "Zlib ");
    if (xmlHasFeature(XML_WITH_LZMA)) fprintf(stderr, "Lzma ");
    fprintf(stderr, "\n");
}

static void usage(const char *name) {
    printf("Usage : %s [options] XMLfiles ...\n", name);
#ifdef LIBXML_OUTPUT_ENABLED
    printf("\tParse the XML files and output the result of the parsing\n");
#else
    printf("\tParse the XML files\n");
#endif /* LIBXML_OUTPUT_ENABLED */
    printf("\t--version : display the version of the XML library used\n");
#ifdef LIBXML_DEBUG_ENABLED
    printf("\t--debug : dump a debug tree of the in-memory document\n");
    printf("\t--shell : run a navigating shell\n");
    printf("\t--debugent : debug the entities defined in the document\n");
#else
#ifdef LIBXML_READER_ENABLED
    printf("\t--debug : dump the nodes content when using --stream\n");
#endif /* LIBXML_READER_ENABLED */
#endif
#ifdef LIBXML_TREE_ENABLED
    printf("\t--copy : used to test the internal copy implementation\n");
#endif /* LIBXML_TREE_ENABLED */
    printf("\t--recover : output what was parsable on broken XML documents\n");
    printf("\t--huge : remove any internal arbitrary parser limits\n");
    printf("\t--noent : substitute entity references by their value\n");
    printf("\t--noenc : ignore any encoding specified inside the document\n");
    printf("\t--noout : don't output the result tree\n");
    printf("\t--path 'paths': provide a set of paths for resources\n");
    printf("\t--load-trace : print trace of all external entities loaded\n");
    printf("\t--nonet : refuse to fetch DTDs or entities over network\n");
    printf("\t--nocompact : do not generate compact text nodes\n");
    printf("\t--htmlout : output results as HTML\n");
    printf("\t--nowrap : do not put HTML doc wrapper\n");
#ifdef LIBXML_VALID_ENABLED
    printf("\t--valid : validate the document in addition to std well-formed check\n");
    printf("\t--postvalid : do a posteriori validation, i.e after parsing\n");
    printf("\t--dtdvalid URL : do a posteriori validation against a given DTD\n");
    printf("\t--dtdvalidfpi FPI : same but name the DTD with a Public Identifier\n");
#endif /* LIBXML_VALID_ENABLED */
    printf("\t--timing : print some timings\n");
    printf("\t--output file or -o file: save to a given file\n");
    printf("\t--repeat : repeat 100 times, for timing or profiling\n");
    printf("\t--insert : ad-hoc test for valid insertions\n");
#ifdef LIBXML_OUTPUT_ENABLED
#ifdef HAVE_ZLIB_H
    printf("\t--compress : turn on gzip compression of output\n");
#endif
#endif /* LIBXML_OUTPUT_ENABLED */
#ifdef LIBXML_HTML_ENABLED
    printf("\t--html : use the HTML parser\n");
    printf("\t--xmlout : force to use the XML serializer when using --html\n");
    printf("\t--nodefdtd : do not default HTML doctype\n");
#endif
#ifdef LIBXML_PUSH_ENABLED
    printf("\t--push : use the push mode of the parser\n");
    printf("\t--pushsmall : use the push mode of the parser using tiny increments\n");
#endif /* LIBXML_PUSH_ENABLED */
#ifdef HAVE_MMAP
    printf("\t--memory : parse from memory\n");
#endif
    printf("\t--maxmem nbbytes : limits memory allocation to nbbytes bytes\n");
    printf("\t--nowarning : do not emit warnings from parser/validator\n");
    printf("\t--noblanks : drop (ignorable?) blanks spaces\n");
    printf("\t--nocdata : replace cdata section with text nodes\n");
#ifdef LIBXML_OUTPUT_ENABLED
    printf("\t--format : reformat/reindent the output\n");
    printf("\t--encode encoding : output in the given encoding\n");
    printf("\t--dropdtd : remove the DOCTYPE of the input docs\n");
    printf("\t--pretty STYLE : pretty-print in a particular style\n");
    printf("\t                 0 Do not pretty print\n");
    printf("\t                 1 Format the XML content, as --format\n");
    printf("\t                 2 Add whitespace inside tags, preserving content\n");
#endif /* LIBXML_OUTPUT_ENABLED */
    printf("\t--c14n : save in W3C canonical format v1.0 (with comments)\n");
    printf("\t--c14n11 : save in W3C canonical format v1.1 (with comments)\n");
    printf("\t--exc-c14n : save in W3C exclusive canonical format (with comments)\n");
#ifdef LIBXML_C14N_ENABLED
#endif /* LIBXML_C14N_ENABLED */
    printf("\t--nsclean : remove redundant namespace declarations\n");
    printf("\t--testIO : test user I/O support\n");
#ifdef LIBXML_CATALOG_ENABLED
    printf("\t--catalogs : use SGML catalogs from $SGML_CATALOG_FILES\n");
    printf("\t             otherwise XML Catalogs starting from \n");
    printf("\t         %s are activated by default\n", XML_XML_DEFAULT_CATALOG);
    printf("\t--nocatalogs: deactivate all catalogs\n");
#endif
    printf("\t--auto : generate a small doc on the fly\n");
#ifdef LIBXML_XINCLUDE_ENABLED
    printf("\t--xinclude : do XInclude processing\n");
    printf("\t--noxincludenode : same but do not generate XInclude nodes\n");
    printf("\t--nofixup-base-uris : do not fixup xml:base uris\n");
#endif
    printf("\t--loaddtd : fetch external DTD\n");
    printf("\t--dtdattr : loaddtd + populate the tree with inherited attributes \n");
#ifdef LIBXML_READER_ENABLED
    printf("\t--stream : use the streaming interface to process very large files\n");
    printf("\t--walker : create a reader and walk though the resulting doc\n");
#endif /* LIBXML_READER_ENABLED */
#ifdef LIBXML_PATTERN_ENABLED
    printf("\t--pattern pattern_value : test the pattern support\n");
#endif
    printf("\t--chkregister : verify the node registration code\n");
#ifdef LIBXML_SCHEMAS_ENABLED
    printf("\t--relaxng schema : do RelaxNG validation against the schema\n");
    printf("\t--schema schema : do validation against the WXS schema\n");
#endif
#ifdef LIBXML_SCHEMATRON_ENABLED
    printf("\t--schematron schema : do validation against a schematron\n");
#endif
#ifdef LIBXML_SAX1_ENABLED
    printf("\t--sax1: use the old SAX1 interfaces for processing\n");
#endif
    printf("\t--sax: do not build a tree but work just at the SAX level\n");
    printf("\t--oldxml10: use XML-1.0 parsing rules before the 5th edition\n");
#ifdef LIBXML_XPATH_ENABLED
    printf("\t--xpath expr: evaluate the XPath expression, imply --noout\n");
#endif

    printf("\nLibxml project home page: http://xmlsoft.org/\n");
    printf("To report bugs or get some help check: http://xmlsoft.org/bugs.html\n");
}

static void registerNode(xmlNodePtr node)
{
    node->_private = malloc(sizeof(long));
    if (node->_private == NULL) {
        fprintf(stderr, "Out of memory in xmllint:registerNode()\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(XMLLINT_ERR_MEM);}
    }
    *(long*)node->_private = (long) 0x81726354;
    nbregister++;
}

static void deregisterNode(xmlNodePtr node)
{
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;assert(node->_private != NULL);}
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;assert(*(long*)node->_private == (long) 0x81726354);}
    free(node->_private);
    nbregister--;
}

int w0(unsigned char* buf, unsigned int len){
	printf("w0 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 0 % len;
    int index2 = 1 % len;
    int index3 = 2 % len;
    int index4 = 3 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3902515186) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1507252891) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 603338905) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1696904468) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1065828383) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3048701182) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1392146492) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3902515186) {
        ret = 12;
      }
    }
    if(buf[index1] > 56) {
      if(buf[index2] < 118) {
        if(buf[index3] - buf[index4] < 56) {
          if(buf[index3] + buf[index4] > 118) {
            if(((buf[index1] + buf[index2]) % 9) == 0) {
              if(((buf[index1] * buf[index2]) % 5) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 56) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }

  return ret;
}

int w1(unsigned char* buf, unsigned int len){
  printf("w1 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 1 % len;
    int index2 = 2 % len;
    int index3 = 3 % len;
    int index4 = 4 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3649224605) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2350045607) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3014389030) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2122141901) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1860222409) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2127411081) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3096905285) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3649224605) {
        ret = 12;
      }
    }
    if(buf[index1] > 209) {
      if(buf[index2] < 215) {
        if(buf[index3] - buf[index4] < 209) {
          if(buf[index3] + buf[index4] > 215) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 209) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w2(unsigned char* buf, unsigned int len){
	printf("w2 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 2 % len;
    int index2 = 3 % len;
    int index3 = 4 % len;
    int index4 = 5 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3060206730) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1823329745) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1798358539) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1130645893) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 334868504) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1095408358) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2855184194) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3060206730) {
        ret = 12;
      }
    }
    if(buf[index1] > 236) {
      if(buf[index2] < 125) {
        if(buf[index3] - buf[index4] < 236) {
          if(buf[index3] + buf[index4] > 125) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  64) {
                  if((buf[index1] ^ buf[index2]) == 236) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w3(unsigned char* buf, unsigned int len){
	  printf("w3 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 3 % len;
    int index2 = 4 % len;
    int index3 = 5 % len;
    int index4 = 6 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1306562184) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2309250971) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 702642080) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3193801888) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1618453275) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3908944833) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1426801106) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1306562184) {
        ret = 12;
      }
    }
    if(buf[index1] > 33) {
      if(buf[index2] < 13) {
        if(buf[index3] - buf[index4] < 33) {
          if(buf[index3] + buf[index4] > 13) {
            if(((buf[index1] + buf[index2]) % 6) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 33) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w4(unsigned char* buf, unsigned int len){
	  printf("w4 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 4 % len;
    int index2 = 5 % len;
    int index3 = 6 % len;
    int index4 = 7 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1317201472) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 4182791665) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1712271078) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3769200889) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3974150540) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1676806712) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 503006531) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1317201472) {
        ret = 12;
      }
    }
    if(buf[index1] > 225) {
      if(buf[index2] < 134) {
        if(buf[index3] - buf[index4] < 225) {
          if(buf[index3] + buf[index4] > 134) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 225) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w5(unsigned char* buf, unsigned int len){
	  printf("w5 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 5 % len;
    int index2 = 6 % len;
    int index3 = 7 % len;
    int index4 = 8 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 2680581394) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798508734) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1827993300) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 713768984) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4009010704) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1285272007) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4015010868) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 2680581394) {
        ret = 12;
      }
    }
    if(buf[index1] > 221) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 221) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 221) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w6(unsigned char* buf, unsigned int len){
	  printf("w6 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 6 % len;
    int index2 = 7 % len;
    int index3 = 8 % len;
    int index4 = 9 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1617115864) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1631690012) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3460463880) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3382779234) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1649578633) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 728322968) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2998716203) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1617115864) {
        ret = 12;
      }
    }
    if(buf[index1] > 132) {
      if(buf[index2] < 208) {
        if(buf[index3] - buf[index4] < 132) {
          if(buf[index3] + buf[index4] > 208) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  32) {
                  if((buf[index1] ^ buf[index2]) == 132) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w7(unsigned char* buf, unsigned int len){
	  printf("w7 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 7 % len;
    int index2 = 8 % len;
    int index3 = 9 % len;
    int index4 = 10 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1208351771) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2227323053) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1104939852) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4258576467) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 780873126) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2155013799) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1158125450) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1208351771) {
        ret = 12;
      }
    }
    if(buf[index1] > 18) {
      if(buf[index2] < 181) {
        if(buf[index3] - buf[index4] < 18) {
          if(buf[index3] + buf[index4] > 181) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  1) {
                  if((buf[index1] ^ buf[index2]) == 18) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w8(unsigned char* buf, unsigned int len){
	  printf("w8 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 8 % len;
    int index2 = 9 % len;
    int index3 = 10 % len;
    int index4 = 11 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 323927678) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798644434) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 2384399227) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 361926757) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 897612148) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 990255627) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3311835330) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 323927678) {
        ret = 12;
      }
    }
    if(buf[index1] > 100) {
      if(buf[index2] < 196) {
        if(buf[index3] - buf[index4] < 100) {
          if(buf[index3] + buf[index4] > 196) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 100) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w9(unsigned char* buf, unsigned int len){
	  printf("w9 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 9 % len;
    int index2 = 10 % len;
    int index3 = 11 % len;
    int index4 = 12 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 720174660) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2124116700) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3937385190) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1443256077) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1256522327) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1727523764) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2994088857) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 720174660) {
        ret = 12;
      }
    }
    if(buf[index1] > 249) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 249) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 8) == 0) {
                if((buf[index1] ^ buf[index2]) &  8) {
                  if((buf[index1] ^ buf[index2]) == 249) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}



typedef int (*obfuscation_call_heavyweight)(unsigned char* buf, unsigned int len);
obfuscation_call_heavyweight functions_array[10] = {
  
  w0,
  
  w1,
  
  w2,
  
  w3,
  
  w4,
  
  w5,
  
  w6,
  
  w7,
  
  w8,
  
  w9
};

/*https://github.com/francis-yli/kmeans_1d/blob/master/kmeans1D.c*/

float getDist(float p1, float p2) {
	float dist;
	dist = fabsf(p1 - p2); 
	return dist;
}

int getNearestDist(float array[], int p, int k) {
	float nearestDist = 300; 
	float dist; 
	int i;
	int label=-1;

	for (i = 0; i < k; i++) {
		dist = getDist(array[i], p);
		if (dist < nearestDist) {
			nearestDist = dist;
			label = i;
		}
	}
	return label;
}

float getMean(unsigned int vArray[], int lArray[], int label, int size) {
	float sum = 0.0;
	int counter = 0;
	float mean;
	int i = 0;

	for (; i < size; i++) {
		if (lArray[i] == label) {
			sum += vArray[i];
			counter++;
		}
	}

	if (counter != 0) {
		mean = sum / (float)counter;
	}
	else mean = -1;

	return mean;
}


void antifuzz(void){

	#define MAX_FILE_CONTENT_SIZE 512
	unsigned char* fileContentMult = (unsigned char*)"abcdefghijklmnopqrstuvwxyz";
shell=((((((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[277])-fake_arr[161])-fake_arr[51])*fake_arr[755])*fake_arr[115])+fake_arr[465])*fake_arr[335])/fake_arr[803])/fake_arr[669])+fake_arr[327])+fake_arr[805])/fake_arr[125])+fake_arr[575])+fake_arr[807])*fake_arr[809])/fake_arr[195])*fake_arr[811])/fake_arr[19])*fake_arr[627])-fake_arr[813])+fake_arr[815])-fake_arr[703])+fake_arr[405])/fake_arr[817])/fake_arr[819])-fake_arr[821])/fake_arr[279])+fake_arr[823])*fake_arr[825])+fake_arr[827])*fake_arr[791])-fake_arr[829])-fake_arr[831])*fake_arr[833])+fake_arr[835])*fake_arr[837])*fake_arr[839])-fake_arr[841])/fake_arr[843])/fake_arr[845])+fake_arr[847])-fake_arr[849])/fake_arr[851])-fake_arr[853])/fake_arr[855])/fake_arr[857])/fake_arr[859])/fake_arr[669]);
debugent=((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[671])-fake_arr[673])-fake_arr[675])/fake_arr[677])/fake_arr[679])+fake_arr[283])/fake_arr[681])/fake_arr[219])-fake_arr[683])*fake_arr[685])*fake_arr[687])-fake_arr[689])*fake_arr[691])-fake_arr[693])/fake_arr[695])/fake_arr[139])*fake_arr[531])/fake_arr[697])/fake_arr[627])*fake_arr[699])*fake_arr[607])*fake_arr[95])/fake_arr[701])-fake_arr[615])/fake_arr[703])+fake_arr[319])*fake_arr[705])/fake_arr[707])-fake_arr[709])/fake_arr[711])/fake_arr[405])-fake_arr[713])*fake_arr[715])/fake_arr[53])+fake_arr[717])*fake_arr[719])/fake_arr[721])*fake_arr[599])/fake_arr[723])-fake_arr[657])/fake_arr[725])/fake_arr[727])/fake_arr[303])/fake_arr[729]);
debug=((((((((((((((((((((((((((((((((((((((((fake_arr[0]+fake_arr[927])/fake_arr[929])/fake_arr[931])-fake_arr[933])-fake_arr[935])/fake_arr[647])-fake_arr[743])-fake_arr[937])*fake_arr[263])-fake_arr[939])/fake_arr[479])/fake_arr[941])+fake_arr[257])/fake_arr[353])+fake_arr[439])/fake_arr[943])+fake_arr[945])-fake_arr[947])*fake_arr[311])/fake_arr[949])*fake_arr[221])*fake_arr[951])+fake_arr[953])+fake_arr[955])-fake_arr[957])*fake_arr[959])/fake_arr[429])*fake_arr[961])/fake_arr[71])-fake_arr[963])*fake_arr[965])-fake_arr[709])/fake_arr[417])/fake_arr[331])-fake_arr[967])/fake_arr[969])/fake_arr[971])-fake_arr[973])/fake_arr[389])/fake_arr[975]);
maxmem=(((((((((((((((((((((((fake_arr[0]+fake_arr[349])-fake_arr[1311])/fake_arr[539])-fake_arr[1313])/fake_arr[1315])/fake_arr[1137])+fake_arr[1317])/fake_arr[727])+fake_arr[613])/fake_arr[929])+fake_arr[649])-fake_arr[1087])+fake_arr[1193])+fake_arr[625])-fake_arr[645])+fake_arr[1319])/fake_arr[1043])-fake_arr[1271])/fake_arr[75])/fake_arr[1321])*fake_arr[159])*fake_arr[743])/fake_arr[867]);
copy=((((((((((((((((((((((((((((fake_arr[0]*fake_arr[349])+fake_arr[691])-fake_arr[1209])-fake_arr[169])/fake_arr[1211])/fake_arr[423])-fake_arr[1213])/fake_arr[1215])*fake_arr[719])*fake_arr[1217])-fake_arr[167])/fake_arr[907])-fake_arr[557])/fake_arr[703])/fake_arr[1219])/fake_arr[567])-fake_arr[529])*fake_arr[11])/fake_arr[1221])+fake_arr[785])-fake_arr[939])/fake_arr[1223])*fake_arr[1225])*fake_arr[1227])-fake_arr[739])+fake_arr[1229])/fake_arr[1231])*fake_arr[1159]);
recovery=(((((((((((((((((((((((((((((((((((((((((((fake_arr[0]+fake_arr[1303])*fake_arr[1133])/fake_arr[1361])*fake_arr[1363])/fake_arr[167])-fake_arr[413])/fake_arr[1365])/fake_arr[1367])-fake_arr[1369])+fake_arr[535])/fake_arr[1371])*fake_arr[173])+fake_arr[1095])-fake_arr[1149])-fake_arr[1373])-fake_arr[173])+fake_arr[1159])+fake_arr[1375])/fake_arr[1377])*fake_arr[155])+fake_arr[851])/fake_arr[1095])-fake_arr[457])/fake_arr[1379])-fake_arr[819])/fake_arr[1021])*fake_arr[1381])+fake_arr[547])*fake_arr[29])+fake_arr[1383])+fake_arr[1385])+fake_arr[703])/fake_arr[395])-fake_arr[1387])+fake_arr[1325])+fake_arr[467])-fake_arr[1389])-fake_arr[1391])+fake_arr[1393])/fake_arr[699])+fake_arr[1395])/fake_arr[95])/fake_arr[1173]);
noent=(((((((((((((((((((fake_arr[0]-fake_arr[1323])/fake_arr[17])*fake_arr[479])/fake_arr[1325])/fake_arr[1099])*fake_arr[33])*fake_arr[967])+fake_arr[609])*fake_arr[1327])-fake_arr[503])*fake_arr[1085])*fake_arr[111])*fake_arr[371])/fake_arr[37])/fake_arr[1001])/fake_arr[1329])-fake_arr[1331])/fake_arr[79])/fake_arr[1333]);
noenc=(((((((((((((((((((((((((((((fake_arr[0]+fake_arr[1095])-fake_arr[1463])+fake_arr[1007])-fake_arr[295])/fake_arr[1093])-fake_arr[539])-fake_arr[791])+fake_arr[1415])/fake_arr[1475])+fake_arr[163])+fake_arr[1477])*fake_arr[373])*fake_arr[1405])+fake_arr[1479])+fake_arr[1481])/fake_arr[155])-fake_arr[343])/fake_arr[1483])/fake_arr[1485])*fake_arr[1487])*fake_arr[1063])*fake_arr[837])/fake_arr[239])/fake_arr[1061])+fake_arr[653])/fake_arr[801])/fake_arr[591])*fake_arr[307])/fake_arr[413]);
noblanks=(((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]*fake_arr[187])-fake_arr[189])+fake_arr[191])-fake_arr[193])+fake_arr[195])+fake_arr[197])*fake_arr[199])/fake_arr[201])+fake_arr[203])-fake_arr[205])/fake_arr[207])+fake_arr[203])*fake_arr[209])+fake_arr[211])*fake_arr[213])/fake_arr[215])/fake_arr[217])-fake_arr[219])*fake_arr[221])-fake_arr[223])+fake_arr[225])/fake_arr[227])+fake_arr[229])*fake_arr[231])+fake_arr[233])*fake_arr[235])+fake_arr[237])/fake_arr[129])*fake_arr[239])*fake_arr[241])/fake_arr[243])-fake_arr[245])*fake_arr[247])/fake_arr[163])-fake_arr[249])/fake_arr[251])/fake_arr[253])-fake_arr[27])/fake_arr[255])-fake_arr[257])-fake_arr[259])/fake_arr[219])-fake_arr[261])+fake_arr[263])/fake_arr[265]);
noout=((((((((((((((((((((((fake_arr[0]*fake_arr[541])/fake_arr[543])/fake_arr[545])*fake_arr[547])/fake_arr[549])/fake_arr[551])-fake_arr[17])+fake_arr[553])/fake_arr[555])/fake_arr[557])+fake_arr[97])-fake_arr[559])-fake_arr[561])+fake_arr[563])*fake_arr[29])-fake_arr[565])*fake_arr[567])/fake_arr[569])/fake_arr[571])/fake_arr[573])+fake_arr[389])/fake_arr[575]);
nowrap=((((((((((((((((((((((((((((((((fake_arr[0]*fake_arr[267])/fake_arr[135])-fake_arr[269])*fake_arr[271])/fake_arr[273])-fake_arr[275])*fake_arr[277])*fake_arr[279])-fake_arr[281])+fake_arr[283])+fake_arr[61])-fake_arr[285])-fake_arr[287])/fake_arr[289])+fake_arr[291])-fake_arr[293])*fake_arr[295])-fake_arr[99])/fake_arr[297])-fake_arr[299])*fake_arr[301])-fake_arr[303])/fake_arr[255])+fake_arr[305])-fake_arr[307])/fake_arr[309])-fake_arr[311])-fake_arr[201])+fake_arr[313])+fake_arr[81])-fake_arr[315])/fake_arr[317]);
format=(((((((((((((((((((fake_arr[0]-fake_arr[899])+fake_arr[901])+fake_arr[247])/fake_arr[903])/fake_arr[905])-fake_arr[907])+fake_arr[909])*fake_arr[911])/fake_arr[913])+fake_arr[635])*fake_arr[915])/fake_arr[917])+fake_arr[919])/fake_arr[165])*fake_arr[921])/fake_arr[923])-fake_arr[925])/fake_arr[121])*fake_arr[461]);
compress=(((((((((((((((((((((((fake_arr[0]+fake_arr[977])-fake_arr[71])-fake_arr[979])-fake_arr[849])/fake_arr[869])/fake_arr[133])*fake_arr[467])*fake_arr[981])*fake_arr[473])+fake_arr[983])-fake_arr[985])/fake_arr[75])/fake_arr[837])/fake_arr[483])/fake_arr[385])+fake_arr[987])*fake_arr[181])/fake_arr[989])+fake_arr[991])*fake_arr[993])/fake_arr[995])-fake_arr[27])/fake_arr[669]);
oldout=((((((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]*fake_arr[221])-fake_arr[369])-fake_arr[371])/fake_arr[373])-fake_arr[375])-fake_arr[377])*fake_arr[379])+fake_arr[381])/fake_arr[383])/fake_arr[231])-fake_arr[385])/fake_arr[387])-fake_arr[389])/fake_arr[391])+fake_arr[91])-fake_arr[393])-fake_arr[395])+fake_arr[397])/fake_arr[399])*fake_arr[401])-fake_arr[403])*fake_arr[405])/fake_arr[219])*fake_arr[407])/fake_arr[409])+fake_arr[411])*fake_arr[413])+fake_arr[189])/fake_arr[415])-fake_arr[417])-fake_arr[419])-fake_arr[421])-fake_arr[423])*fake_arr[425])-fake_arr[427])+fake_arr[429])/fake_arr[431])/fake_arr[433])-fake_arr[435])*fake_arr[437])+fake_arr[439])/fake_arr[167])+fake_arr[441])*fake_arr[443])+fake_arr[445])-fake_arr[447])/fake_arr[449])/fake_arr[451]);
valid=(((((((((((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[281])/fake_arr[1321])*fake_arr[639])+fake_arr[1523])+fake_arr[1411])*fake_arr[177])+fake_arr[645])+fake_arr[1525])-fake_arr[139])-fake_arr[1527])/fake_arr[733])*fake_arr[707])+fake_arr[51])-fake_arr[1209])-fake_arr[1195])-fake_arr[1529])+fake_arr[127])/fake_arr[635])/fake_arr[921])*fake_arr[1531])-fake_arr[347])-fake_arr[1533])+fake_arr[1535])/fake_arr[1097])+fake_arr[339])/fake_arr[1527])*fake_arr[1537])-fake_arr[1539])*fake_arr[169])+fake_arr[1541])-fake_arr[245])-fake_arr[1543])/fake_arr[1221])-fake_arr[1067])+fake_arr[909])-fake_arr[631])+fake_arr[1209])-fake_arr[813])/fake_arr[927]);
postvalid=(((((((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[997])*fake_arr[999])*fake_arr[1001])-fake_arr[1003])-fake_arr[893])*fake_arr[907])+fake_arr[27])*fake_arr[121])/fake_arr[1005])+fake_arr[1007])-fake_arr[1009])*fake_arr[497])/fake_arr[1001])/fake_arr[713])-fake_arr[863])*fake_arr[967])-fake_arr[469])-fake_arr[1011])*fake_arr[801])*fake_arr[1013])/fake_arr[303])-fake_arr[1015])*fake_arr[1017])*fake_arr[1019])*fake_arr[509])+fake_arr[383])*fake_arr[461])+fake_arr[1021])-fake_arr[1023])/fake_arr[1025])-fake_arr[1027])-fake_arr[1013])/fake_arr[1029])/fake_arr[1031])/fake_arr[1033])/fake_arr[737])/fake_arr[177])/fake_arr[1035])*fake_arr[1037])+fake_arr[1039])/fake_arr[1041])-fake_arr[1043])+fake_arr[1045])-fake_arr[597])-fake_arr[1047])+fake_arr[693])/fake_arr[1049])/fake_arr[341])*fake_arr[1051]);
repeat=((((((((((((((((((((((((fake_arr[0]*fake_arr[335])/fake_arr[157])+fake_arr[869])/fake_arr[509])-fake_arr[1293])-fake_arr[1295])+fake_arr[223])-fake_arr[809])-fake_arr[565])-fake_arr[1297])/fake_arr[1251])+fake_arr[1255])/fake_arr[289])-fake_arr[1299])+fake_arr[1301])/fake_arr[539])-fake_arr[665])+fake_arr[1303])+fake_arr[1305])/fake_arr[1013])-fake_arr[379])+fake_arr[1307])/fake_arr[1049])/fake_arr[1309]);
insert=(((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[379])/fake_arr[1145])/fake_arr[549])*fake_arr[157])-fake_arr[1147])/fake_arr[1149])+fake_arr[1151])+fake_arr[161])/fake_arr[147])+fake_arr[1153])*fake_arr[37])+fake_arr[1093])-fake_arr[565])*fake_arr[1155])-fake_arr[909])-fake_arr[1157])/fake_arr[801])-fake_arr[1159])-fake_arr[1161])+fake_arr[1163])+fake_arr[1165])/fake_arr[1167])/fake_arr[391])/fake_arr[1013])+fake_arr[1169])*fake_arr[721])/fake_arr[1171])/fake_arr[865])*fake_arr[27])*fake_arr[1173])*fake_arr[103])*fake_arr[281])-fake_arr[1175])+fake_arr[1177])*fake_arr[1179])/fake_arr[1181])/fake_arr[279]);
html=(((((((((((((((((((((((((((((((((((((((((fake_arr[0]*fake_arr[445])*fake_arr[499])*fake_arr[621])+fake_arr[623])*fake_arr[537])-fake_arr[625])+fake_arr[627])+fake_arr[479])-fake_arr[629])+fake_arr[505])/fake_arr[631])-fake_arr[367])/fake_arr[633])*fake_arr[631])/fake_arr[635])+fake_arr[637])-fake_arr[639])+fake_arr[629])-fake_arr[127])+fake_arr[641])/fake_arr[643])-fake_arr[645])*fake_arr[647])-fake_arr[43])-fake_arr[453])/fake_arr[37])*fake_arr[649])-fake_arr[651])-fake_arr[5])-fake_arr[653])+fake_arr[655])-fake_arr[491])-fake_arr[657])/fake_arr[659])+fake_arr[75])/fake_arr[661])/fake_arr[663])+fake_arr[555])-fake_arr[665])/fake_arr[667])/fake_arr[669]);
xmlout=((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[1409])*fake_arr[189])*fake_arr[417])*fake_arr[753])/fake_arr[363])*fake_arr[1411])+fake_arr[1355])-fake_arr[1413])+fake_arr[1415])-fake_arr[181])-fake_arr[1417])/fake_arr[565])*fake_arr[1419])+fake_arr[233])/fake_arr[137])*fake_arr[913])-fake_arr[439])-fake_arr[437])/fake_arr[15])+fake_arr[641])-fake_arr[457])/fake_arr[1421])*fake_arr[1423])/fake_arr[837])*fake_arr[1425])+fake_arr[579])/fake_arr[1427])*fake_arr[549])-fake_arr[1429])/fake_arr[1431]);
htmlout=(((((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[401])+fake_arr[375])/fake_arr[307])*fake_arr[1433])/fake_arr[1435])*fake_arr[1099])-fake_arr[543])+fake_arr[1437])+fake_arr[1083])/fake_arr[753])*fake_arr[551])/fake_arr[1047])/fake_arr[1439])/fake_arr[1039])+fake_arr[955])*fake_arr[1375])+fake_arr[963])+fake_arr[593])+fake_arr[1197])+fake_arr[727])-fake_arr[103])/fake_arr[1387])*fake_arr[639])-fake_arr[807])/fake_arr[947])-fake_arr[225])+fake_arr[281])/fake_arr[363])*fake_arr[523])*fake_arr[1441])*fake_arr[521])/fake_arr[565])*fake_arr[673]);
nodefdtd=(((((((((((((((fake_arr[0]-fake_arr[275])*fake_arr[787])+fake_arr[789])+fake_arr[785])/fake_arr[701])+fake_arr[597])-fake_arr[791])+fake_arr[461])/fake_arr[787])*fake_arr[793])+fake_arr[795])*fake_arr[797])/fake_arr[799])/fake_arr[367])*fake_arr[801]);
push=((((((((((((fake_arr[0]*fake_arr[713])/fake_arr[291])+fake_arr[467])+fake_arr[27])/fake_arr[1489])*fake_arr[327])-fake_arr[205])+fake_arr[1491])-fake_arr[315])/fake_arr[1493])*fake_arr[827])/fake_arr[97]);
pushsize=(((((((((((((((((((((((((((((((((((((((((fake_arr[0]+fake_arr[453])*fake_arr[55])*fake_arr[455])/fake_arr[161])-fake_arr[363])-fake_arr[457])-fake_arr[459])-fake_arr[461])-fake_arr[463])+fake_arr[71])*fake_arr[103])-fake_arr[465])*fake_arr[303])+fake_arr[467])/fake_arr[469])-fake_arr[471])/fake_arr[151])/fake_arr[473])+fake_arr[475])+fake_arr[477])-fake_arr[479])/fake_arr[481])-fake_arr[483])+fake_arr[485])-fake_arr[487])*fake_arr[489])/fake_arr[491])/fake_arr[493])+fake_arr[495])*fake_arr[465])-fake_arr[209])/fake_arr[443])*fake_arr[497])/fake_arr[117])*fake_arr[499])*fake_arr[501])*fake_arr[503])/fake_arr[505])/fake_arr[507])+fake_arr[341])+fake_arr[509]);
memory=((((((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[731])+fake_arr[733])*fake_arr[735])-fake_arr[267])/fake_arr[737])/fake_arr[229])+fake_arr[695])-fake_arr[739])*fake_arr[741])/fake_arr[743])-fake_arr[733])-fake_arr[745])-fake_arr[747])+fake_arr[749])-fake_arr[143])-fake_arr[751])*fake_arr[201])*fake_arr[753])*fake_arr[755])-fake_arr[757])*fake_arr[759])/fake_arr[761])-fake_arr[621])-fake_arr[667])-fake_arr[763])*fake_arr[109])/fake_arr[147])/fake_arr[113])+fake_arr[765])/fake_arr[715])/fake_arr[241])+fake_arr[767])*fake_arr[769])/fake_arr[633])*fake_arr[9])-fake_arr[387])+fake_arr[771])+fake_arr[773])+fake_arr[775])*fake_arr[543])-fake_arr[97])/fake_arr[777])+fake_arr[779])/fake_arr[173])*fake_arr[781])/fake_arr[769])/fake_arr[783])/fake_arr[785]);
testIO=(((((((((((((fake_arr[0]*fake_arr[689])*fake_arr[383])/fake_arr[867])*fake_arr[1545])/fake_arr[1357])/fake_arr[1397])*fake_arr[299])+fake_arr[763])/fake_arr[1407])+fake_arr[1547])+fake_arr[1549])/fake_arr[1523])/fake_arr[561]);
xinclude=((((((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[567])-fake_arr[703])/fake_arr[1233])+fake_arr[1235])-fake_arr[1037])*fake_arr[389])-fake_arr[1237])-fake_arr[657])*fake_arr[1097])/fake_arr[199])+fake_arr[1239])*fake_arr[425])-fake_arr[227])/fake_arr[911])/fake_arr[137])*fake_arr[411])/fake_arr[931])+fake_arr[245])*fake_arr[815])*fake_arr[307])*fake_arr[1241])*fake_arr[1109])-fake_arr[955])+fake_arr[823])-fake_arr[1243])+fake_arr[55])/fake_arr[1245])+fake_arr[863])/fake_arr[1247])/fake_arr[1249])+fake_arr[821])+fake_arr[397])-fake_arr[321])-fake_arr[387])*fake_arr[751])*fake_arr[381])+fake_arr[1251])-fake_arr[1253])/fake_arr[729])-fake_arr[1255])+fake_arr[1257])*fake_arr[917])+fake_arr[509])/fake_arr[227])/fake_arr[729])/fake_arr[895])*fake_arr[1259])/fake_arr[647]);
dtdattrs=(((((((((((((((((((((fake_arr[0]-fake_arr[511])/fake_arr[513])*fake_arr[515])-fake_arr[69])/fake_arr[517])*fake_arr[519])*fake_arr[521])*fake_arr[523])-fake_arr[525])*fake_arr[527])*fake_arr[237])*fake_arr[529])-fake_arr[531])/fake_arr[335])+fake_arr[287])/fake_arr[429])/fake_arr[533])-fake_arr[535])+fake_arr[425])-fake_arr[537])/fake_arr[539]);
loaddtd=(((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[1507])-fake_arr[1509])+fake_arr[1511])*fake_arr[649])+fake_arr[15])+fake_arr[1513])-fake_arr[1065])/fake_arr[915])-fake_arr[1243])/fake_arr[1265])+fake_arr[1515])-fake_arr[1421])+fake_arr[979])/fake_arr[347])/fake_arr[727])-fake_arr[103])+fake_arr[231])+fake_arr[457])-fake_arr[853])+fake_arr[1003])/fake_arr[1351])+fake_arr[1415])-fake_arr[77])/fake_arr[1085])+fake_arr[1389])*fake_arr[1471])*fake_arr[1517])/fake_arr[1519])-fake_arr[485])+fake_arr[415])*fake_arr[383])-fake_arr[935])-fake_arr[1041])-fake_arr[709])/fake_arr[1245])+fake_arr[31])-fake_arr[1097])/fake_arr[993])*fake_arr[1235])+fake_arr[1521])-fake_arr[1153])+fake_arr[1219])/fake_arr[1159])/fake_arr[115])*fake_arr[1031]);
timing=(((((((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[1183])/fake_arr[881])-fake_arr[259])+fake_arr[363])*fake_arr[1185])-fake_arr[1187])/fake_arr[435])-fake_arr[877])*fake_arr[1189])-fake_arr[1031])-fake_arr[1123])+fake_arr[753])-fake_arr[1191])*fake_arr[523])-fake_arr[1101])+fake_arr[321])+fake_arr[519])-fake_arr[1193])*fake_arr[1029])/fake_arr[925])*fake_arr[1195])/fake_arr[1197])+fake_arr[1091])-fake_arr[1029])/fake_arr[993])-fake_arr[1141])+fake_arr[451])-fake_arr[1199])*fake_arr[429])-fake_arr[215])*fake_arr[1201])/fake_arr[1203])+fake_arr[977])-fake_arr[607])-fake_arr[275])-fake_arr[901])+fake_arr[351])-fake_arr[1205])/fake_arr[1007])/fake_arr[1207])/fake_arr[473]);
generate=(((((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[115])+fake_arr[1205])+fake_arr[221])*fake_arr[431])-fake_arr[1261])+fake_arr[537])/fake_arr[395])/fake_arr[1263])/fake_arr[931])*fake_arr[1265])-fake_arr[139])/fake_arr[1065])-fake_arr[541])/fake_arr[423])+fake_arr[1217])-fake_arr[615])-fake_arr[691])-fake_arr[575])+fake_arr[953])*fake_arr[1267])-fake_arr[1269])+fake_arr[1237])-fake_arr[381])+fake_arr[1271])-fake_arr[1273])+fake_arr[1275])-fake_arr[171])+fake_arr[971])+fake_arr[841])/fake_arr[1277])-fake_arr[1251])*fake_arr[363])/fake_arr[297])*fake_arr[141])*fake_arr[1279])-fake_arr[547])/fake_arr[1281])-fake_arr[1283])+fake_arr[1285])/fake_arr[393])/fake_arr[947])/fake_arr[121])/fake_arr[1287])+fake_arr[1289])/fake_arr[1191])-fake_arr[949])/fake_arr[1291]);
dropdtd=(((((((((((((((((((((((((((((fake_arr[0]/fake_arr[59])*fake_arr[137])-fake_arr[139])*fake_arr[141])/fake_arr[143])-fake_arr[145])+fake_arr[147])/fake_arr[149])-fake_arr[151])-fake_arr[153])-fake_arr[91])*fake_arr[155])/fake_arr[157])*fake_arr[159])*fake_arr[161])/fake_arr[163])-fake_arr[165])+fake_arr[119])*fake_arr[167])/fake_arr[169])/fake_arr[171])+fake_arr[173])-fake_arr[175])+fake_arr[177])-fake_arr[179])+fake_arr[181])/fake_arr[183])*fake_arr[185])/fake_arr[133]);
catalogs=((((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[351])*fake_arr[1335])*fake_arr[703])/fake_arr[487])*fake_arr[311])/fake_arr[905])*fake_arr[1179])-fake_arr[169])+fake_arr[1337])+fake_arr[1071])+fake_arr[875])+fake_arr[1339])/fake_arr[489])-fake_arr[1341])/fake_arr[949])*fake_arr[1343])-fake_arr[1345])*fake_arr[1173])*fake_arr[1043])*fake_arr[839])/fake_arr[893])/fake_arr[353])+fake_arr[1347])/fake_arr[115])-fake_arr[27])*fake_arr[961])-fake_arr[903])-fake_arr[1177])/fake_arr[1349])-fake_arr[1157])/fake_arr[1125])*fake_arr[1351])*fake_arr[1353])*fake_arr[5])-fake_arr[1355])+fake_arr[3])-fake_arr[1023])/fake_arr[43])+fake_arr[49])*fake_arr[1167])-fake_arr[1357])-fake_arr[857])/fake_arr[539])-fake_arr[1221])/fake_arr[1359])/fake_arr[841]);
nocatalogs=(((((((((((((((((((((((fake_arr[0]/fake_arr[261])/fake_arr[669])+fake_arr[1495])+fake_arr[687])*fake_arr[353])+fake_arr[843])+fake_arr[1497])-fake_arr[495])-fake_arr[1471])-fake_arr[1445])-fake_arr[1495])+fake_arr[1499])*fake_arr[497])/fake_arr[315])+fake_arr[1067])-fake_arr[983])+fake_arr[1501])*fake_arr[1503])-fake_arr[911])/fake_arr[51])+fake_arr[1505])/fake_arr[919])/fake_arr[263]);
canonical=(((((((((((((((((((((((((((((fake_arr[0]/fake_arr[29])/fake_arr[319])+fake_arr[321])+fake_arr[323])/fake_arr[325])/fake_arr[327])/fake_arr[329])*fake_arr[51])+fake_arr[331])-fake_arr[333])+fake_arr[335])*fake_arr[337])*fake_arr[339])/fake_arr[341])*fake_arr[343])+fake_arr[227])-fake_arr[345])/fake_arr[265])+fake_arr[347])/fake_arr[349])*fake_arr[351])-fake_arr[353])+fake_arr[355])/fake_arr[357])/fake_arr[359])*fake_arr[361])+fake_arr[363])/fake_arr[365])*fake_arr[367]);
canonical_11=((((((((((((((((((((((((((((((fake_arr[0]*fake_arr[1013])-fake_arr[1053])*fake_arr[1055])/fake_arr[479])/fake_arr[1057])*fake_arr[475])*fake_arr[849])*fake_arr[107])*fake_arr[1059])+fake_arr[1061])-fake_arr[329])-fake_arr[459])-fake_arr[1063])*fake_arr[1023])/fake_arr[923])+fake_arr[1065])*fake_arr[1067])/fake_arr[1069])-fake_arr[983])+fake_arr[1071])-fake_arr[71])+fake_arr[777])+fake_arr[1073])/fake_arr[1075])*fake_arr[765])+fake_arr[827])-fake_arr[1077])/fake_arr[119])/fake_arr[1079])*fake_arr[1069]);
exc_canonical=((((((((((((((((((((((((((((((((((((((((((fake_arr[0]+fake_arr[529])*fake_arr[1081])-fake_arr[1083])/fake_arr[629])+fake_arr[795])+fake_arr[951])/fake_arr[1085])-fake_arr[205])+fake_arr[1087])/fake_arr[1089])+fake_arr[1091])/fake_arr[1093])-fake_arr[1023])+fake_arr[1095])+fake_arr[1097])*fake_arr[663])+fake_arr[983])+fake_arr[863])+fake_arr[1099])*fake_arr[581])*fake_arr[523])+fake_arr[809])/fake_arr[89])-fake_arr[581])+fake_arr[585])+fake_arr[961])+fake_arr[1101])-fake_arr[239])/fake_arr[1103])+fake_arr[401])*fake_arr[361])*fake_arr[1105])+fake_arr[1107])/fake_arr[765])-fake_arr[37])-fake_arr[767])+fake_arr[441])+fake_arr[505])/fake_arr[1109])-fake_arr[395])/fake_arr[529])/fake_arr[535]);
stream=((((((((((((((((fake_arr[0]+fake_arr[107])/fake_arr[109])-fake_arr[111])/fake_arr[113])/fake_arr[115])+fake_arr[117])-fake_arr[119])+fake_arr[115])/fake_arr[121])*fake_arr[123])*fake_arr[125])/fake_arr[127])/fake_arr[129])+fake_arr[131])-fake_arr[133])/fake_arr[135]);
walker=((((((((((((((((((((((((((((((((((((((fake_arr[0]-fake_arr[511])*fake_arr[1111])+fake_arr[1113])-fake_arr[1115])+fake_arr[423])-fake_arr[1117])+fake_arr[1119])*fake_arr[545])*fake_arr[1121])+fake_arr[1123])/fake_arr[1125])+fake_arr[295])/fake_arr[297])-fake_arr[1127])-fake_arr[1129])/fake_arr[1131])*fake_arr[943])+fake_arr[1133])*fake_arr[233])-fake_arr[1135])*fake_arr[1137])*fake_arr[1139])+fake_arr[859])/fake_arr[17])+fake_arr[1141])*fake_arr[275])+fake_arr[1069])+fake_arr[1143])-fake_arr[833])-fake_arr[567])-fake_arr[105])+fake_arr[485])/fake_arr[363])/fake_arr[95])-fake_arr[717])/fake_arr[569])+fake_arr[851])/fake_arr[43]);
chkregister=((((((((((((((fake_arr[0]-fake_arr[577])/fake_arr[579])+fake_arr[527])+fake_arr[297])/fake_arr[581])*fake_arr[51])/fake_arr[1])/fake_arr[583])+fake_arr[399])/fake_arr[347])*fake_arr[129])*fake_arr[111])*fake_arr[585])/fake_arr[203]);
nbregister=(((((((((((((((((((fake_arr[0]*fake_arr[587])*fake_arr[589])-fake_arr[591])-fake_arr[593])-fake_arr[595])-fake_arr[597])*fake_arr[599])-fake_arr[551])/fake_arr[601])-fake_arr[603])-fake_arr[605])+fake_arr[607])/fake_arr[609])-fake_arr[503])/fake_arr[611])+fake_arr[613])/fake_arr[615])/fake_arr[617])/fake_arr[619]);
sax1=(((((((((((((((((((((((((((((((((((((((((((fake_arr[0]/fake_arr[21])*fake_arr[23])/fake_arr[25])-fake_arr[27])+fake_arr[29])/fake_arr[31])/fake_arr[33])/fake_arr[35])/fake_arr[37])+fake_arr[39])*fake_arr[41])/fake_arr[43])/fake_arr[45])*fake_arr[47])*fake_arr[49])/fake_arr[51])/fake_arr[53])*fake_arr[55])-fake_arr[57])-fake_arr[59])/fake_arr[61])/fake_arr[63])+fake_arr[65])*fake_arr[67])/fake_arr[69])-fake_arr[71])+fake_arr[73])+fake_arr[75])+fake_arr[77])+fake_arr[79])-fake_arr[81])+fake_arr[83])/fake_arr[85])*fake_arr[87])/fake_arr[89])/fake_arr[91])+fake_arr[93])*fake_arr[95])+fake_arr[97])-fake_arr[99])/fake_arr[101])-fake_arr[103])/fake_arr[105]);
sax=((((((((((((((((((((((((((((((((((((((((((((fake_arr[0]*fake_arr[319])*fake_arr[1443])*fake_arr[1419])+fake_arr[1445])/fake_arr[1447])+fake_arr[1359])*fake_arr[391])/fake_arr[1449])*fake_arr[103])*fake_arr[1451])+fake_arr[1453])/fake_arr[1361])-fake_arr[643])-fake_arr[621])+fake_arr[1445])+fake_arr[269])*fake_arr[245])*fake_arr[1455])*fake_arr[1457])+fake_arr[1227])-fake_arr[1019])-fake_arr[1333])/fake_arr[83])*fake_arr[307])/fake_arr[1459])*fake_arr[811])-fake_arr[823])-fake_arr[401])+fake_arr[1461])+fake_arr[1339])*fake_arr[1463])/fake_arr[1187])+fake_arr[149])/fake_arr[301])+fake_arr[1465])/fake_arr[1467])+fake_arr[171])/fake_arr[1469])/fake_arr[1471])/fake_arr[165])+fake_arr[1473])/fake_arr[339])*fake_arr[575])*fake_arr[987]);
oldxml10=((((((((((((((((((((((fake_arr[0]+fake_arr[1169])+fake_arr[515])+fake_arr[1397])+fake_arr[73])/fake_arr[1295])-fake_arr[281])-fake_arr[1399])+fake_arr[951])+fake_arr[1401])*fake_arr[1403])-fake_arr[909])+fake_arr[301])-fake_arr[351])-fake_arr[245])/fake_arr[1405])-fake_arr[365])-fake_arr[1111])+fake_arr[383])/fake_arr[215])/fake_arr[1295])*fake_arr[1407])/fake_arr[1207]);
nbpaths=((((((((((fake_arr[0]/fake_arr[1])/fake_arr[3])/fake_arr[5])/fake_arr[7])-fake_arr[9])-fake_arr[11])+fake_arr[13])/fake_arr[15])/fake_arr[17])*fake_arr[19]);
load_trace=(((((((((((((((((((((((((((((((((fake_arr[0]*fake_arr[337])-fake_arr[529])*fake_arr[861])+fake_arr[863])/fake_arr[865])-fake_arr[71])-fake_arr[285])+fake_arr[867])+fake_arr[869])-fake_arr[369])/fake_arr[871])+fake_arr[873])*fake_arr[875])-fake_arr[519])/fake_arr[877])/fake_arr[879])-fake_arr[881])+fake_arr[797])/fake_arr[883])/fake_arr[527])/fake_arr[885])+fake_arr[887])/fake_arr[467])-fake_arr[889])+fake_arr[363])*fake_arr[203])-fake_arr[891])+fake_arr[893])+fake_arr[895])+fake_arr[765])/fake_arr[809])+fake_arr[233])/fake_arr[897]);
	unsigned int x[0xFFFFF]={0x00,};
	unsigned int y[0xFFFFF]={0x00,};

	char pid_mem_file[50]={0x00,};
	char pid_mem_file_name[50]={0x00,};
	
  	pid_t pid=getpid();
  	sprintf(pid_mem_file, "/proc/%d/maps", pid);
	sprintf(pid_mem_file_name, "./maps_%d", pid);
  	int canary_index, canary_val;
  	while(1){
  		int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
  		if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
        	else{
            		fake_arr[0]=1;
           	 	break;
        	}
        	canary_val=rand()%1000;
		if(fake_arr[canary_index]!=canary_val){
			fake_arr[canary_index]=canary_val;
			break;
		}
  	}
  	FILE *fp1  = fopen(pid_mem_file, "r");
	FILE *fp2  = fopen(pid_mem_file_name, "w");
  
	char buffer[128];
	regex_t state;
  	regmatch_t pmatch[3];

	char addr1[18];
  	char addr2[18];
  	const char *pattern = "[rwx-]{3}+[s]";
  	const char *addr = "([0-9a-fA-F]{12,16})-([0-9a-fA-F]{12,16})";
  	strncpy(addr1,"0x",2);
  	strncpy(addr2,"0x",2);

  	memset(addr1+2, 0x00, sizeof(addr1)-2);
 	memset(addr2+2, 0x00, sizeof(addr2)-2);

	regcomp(&state, pattern, REG_EXTENDED);
  	int index;
  	while (fgets(buffer, 128, fp1) != NULL)
  	{	
      		fprintf(fp2, "%s", buffer);
		int status = regexec(&state, buffer, 1, pmatch, 0);
		if(status==0){
      			regfree(&state);
      			regcomp(&state, addr, REG_EXTENDED);
      			status = regexec(&state, buffer, 3, pmatch, 0);
      			if(status==0){		
        			strncpy(addr1+2,buffer+pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
        			strncpy(addr2+2,buffer+pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
		
        			start_addr=(unsigned char*)strtoll(addr1,NULL,16);
        			end_addr=(unsigned char*)strtoll(addr2,NULL,16);
			
				for (int i = 0; i < end_addr-start_addr; i++) {
					x[i]=i;
					y[i] = start_addr[i];
				}
    
				for(int i = 0; i <130; i++) {
		        		functions_array[0](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i <110; i++) {
		        		functions_array[1](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 90; i++) {
		        		functions_array[2](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 70; i++) {
		        		functions_array[3](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 50; i++) {
		        		functions_array[4](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}

		      		int k=0;
		      		for (int i = 0; i < end_addr-start_addr; i++){
					x[i]=i;
					int tmp_value=start_addr[i]-y[i];
					if(tmp_value>30 && tmp_value<150)y[k++]=tmp_value;
				}

		      		int labelArray[k];
		      		int orig_centerArray[5] = { 50,70,90,110,130 };
		      		float centerArray[5] = { 50,70,90,110,130 };
		      		float tmp_centerArray[5] = { 0, };
      
		     		int loop = 0;
				while(1) {
		        		loop++;
		        		for (int i = 0; i < k; i++) {
		          			int label = getNearestDist(centerArray, y[i], 5);
		          			labelArray[i] = label;
		       			}

		       			int finish = 0;

		       			for (int i = 0; i < 5; i++) {
		          			tmp_centerArray[i] = centerArray[i];
		          			centerArray[i] = getMean(y, labelArray, i, k);
		          			if (tmp_centerArray[i] == centerArray[i]) {
		            				finish += 1;
		          			}
		          
        				}
		       	 		if (finish == 5) {
		          			break;
		        		}
      				}

		      		float distance = 0;
		     	 	int tmp_index=5;
		      		for (int i = 0; i < tmp_index; i++) {
		        		if (centerArray[i] == -1)tmp_index -= 1;
		        		else distance+=(centerArray[i] - orig_centerArray[i])*(centerArray[i]-orig_centerArray[i]);
		      		}
		      		distance /=tmp_index;

				if(distance<10){
					antifuzz_check=1;
					for (int i = 0; i < end_addr-start_addr; i++) {
						start_addr[i]=0x80;
					}
				}
				break;
     			}
   		}
 	}
	regfree(&state);
	fclose(fp1);
	fclose(fp2);

       	unlink(pid_mem_file_name);

}

int
main(int argc, char **argv) {
	antifuzz();
    int i, acount;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
    int files = 0;
    int version = 0;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
    const char* indent;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

    if (argc <= 1) {
	usage(argv[0]);
	{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return(1);}
    }
    LIBXML_TEST_VERSION
    for (i = 1; i < argc ; i++) {
	if (!strcmp(argv[i], "-"))
	    break;

	if (argv[i][0] != '-')
	    continue;
	if ((!strcmp(argv[i], "-debug")) || (!strcmp(argv[i], "--debug")))
	    debug++;
	else
#ifdef LIBXML_DEBUG_ENABLED
	if ((!strcmp(argv[i], "-shell")) ||
	         (!strcmp(argv[i], "--shell"))) {
	    shell++;
            noout = 1;
        } else
#endif
#ifdef LIBXML_TREE_ENABLED
	if ((!strcmp(argv[i], "-copy")) || (!strcmp(argv[i], "--copy")))
	    copy++;
	else
#endif /* LIBXML_TREE_ENABLED */
	if ((!strcmp(argv[i], "-recover")) ||
	         (!strcmp(argv[i], "--recover"))) {
	    recovery++;
	    options |= XML_PARSE_RECOVER;
	} else if ((!strcmp(argv[i], "-huge")) ||
	         (!strcmp(argv[i], "--huge"))) {
	    options |= XML_PARSE_HUGE;
	} else if ((!strcmp(argv[i], "-noent")) ||
	         (!strcmp(argv[i], "--noent"))) {
	    noent++;
	    options |= XML_PARSE_NOENT;
	} else if ((!strcmp(argv[i], "-noenc")) ||
	         (!strcmp(argv[i], "--noenc"))) {
	    noenc++;
	    options |= XML_PARSE_IGNORE_ENC;
	} else if ((!strcmp(argv[i], "-nsclean")) ||
	         (!strcmp(argv[i], "--nsclean"))) {
	    options |= XML_PARSE_NSCLEAN;
	} else if ((!strcmp(argv[i], "-nocdata")) ||
	         (!strcmp(argv[i], "--nocdata"))) {
	    options |= XML_PARSE_NOCDATA;
	} else if ((!strcmp(argv[i], "-nodict")) ||
	         (!strcmp(argv[i], "--nodict"))) {
	    options |= XML_PARSE_NODICT;
	} else if ((!strcmp(argv[i], "-version")) ||
	         (!strcmp(argv[i], "--version"))) {
	    showVersion(argv[0]);
	    version = 1;
	} else if ((!strcmp(argv[i], "-noout")) ||
	         (!strcmp(argv[i], "--noout")))
	    noout++;
#ifdef LIBXML_OUTPUT_ENABLED
	else if ((!strcmp(argv[i], "-o")) ||
	         (!strcmp(argv[i], "-output")) ||
	         (!strcmp(argv[i], "--output"))) {
	    i++;
	    output = argv[i];
	}
#endif /* LIBXML_OUTPUT_ENABLED */
	else if ((!strcmp(argv[i], "-htmlout")) ||
	         (!strcmp(argv[i], "--htmlout")))
	    htmlout++;
	else if ((!strcmp(argv[i], "-nowrap")) ||
	         (!strcmp(argv[i], "--nowrap")))
	    nowrap++;
#ifdef LIBXML_HTML_ENABLED
	else if ((!strcmp(argv[i], "-html")) ||
	         (!strcmp(argv[i], "--html"))) {
	    html++;
        }
	else if ((!strcmp(argv[i], "-xmlout")) ||
	         (!strcmp(argv[i], "--xmlout"))) {
	    xmlout++;
	} else if ((!strcmp(argv[i], "-nodefdtd")) ||
	         (!strcmp(argv[i], "--nodefdtd"))) {
            nodefdtd++;
	    options |= HTML_PARSE_NODEFDTD;
        }
#endif /* LIBXML_HTML_ENABLED */
	else if ((!strcmp(argv[i], "-loaddtd")) ||
	         (!strcmp(argv[i], "--loaddtd"))) {
	    loaddtd++;
	    options |= XML_PARSE_DTDLOAD;
	} else if ((!strcmp(argv[i], "-dtdattr")) ||
	         (!strcmp(argv[i], "--dtdattr"))) {
	    loaddtd++;
	    dtdattrs++;
	    options |= XML_PARSE_DTDATTR;
	}
#ifdef LIBXML_VALID_ENABLED
	else if ((!strcmp(argv[i], "-valid")) ||
	         (!strcmp(argv[i], "--valid"))) {
	    valid++;
	    options |= XML_PARSE_DTDVALID;
	} else if ((!strcmp(argv[i], "-postvalid")) ||
	         (!strcmp(argv[i], "--postvalid"))) {
	    postvalid++;
	    loaddtd++;
	    options |= XML_PARSE_DTDLOAD;
	} else if ((!strcmp(argv[i], "-dtdvalid")) ||
	         (!strcmp(argv[i], "--dtdvalid"))) {
	    i++;
	    dtdvalid = argv[i];
	    loaddtd++;
	    options |= XML_PARSE_DTDLOAD;
	} else if ((!strcmp(argv[i], "-dtdvalidfpi")) ||
	         (!strcmp(argv[i], "--dtdvalidfpi"))) {
	    i++;
	    dtdvalidfpi = argv[i];
	    loaddtd++;
	    options |= XML_PARSE_DTDLOAD;
        }
#endif /* LIBXML_VALID_ENABLED */
	else if ((!strcmp(argv[i], "-dropdtd")) ||
	         (!strcmp(argv[i], "--dropdtd")))
	    dropdtd++;
	else if ((!strcmp(argv[i], "-insert")) ||
	         (!strcmp(argv[i], "--insert")))
	    insert++;
	else if ((!strcmp(argv[i], "-timing")) ||
	         (!strcmp(argv[i], "--timing")))
	    timing++;
	else if ((!strcmp(argv[i], "-auto")) ||
	         (!strcmp(argv[i], "--auto")))
	    generate++;
	else if ((!strcmp(argv[i], "-repeat")) ||
	         (!strcmp(argv[i], "--repeat"))) {
	    if (repeat)
	        repeat *= 10;
	    else
	        repeat = 100;
	}
#ifdef LIBXML_PUSH_ENABLED
	else if ((!strcmp(argv[i], "-push")) ||
	         (!strcmp(argv[i], "--push")))
	    push++;
	else if ((!strcmp(argv[i], "-pushsmall")) ||
	         (!strcmp(argv[i], "--pushsmall"))) {
	    push++;
            pushsize = 10;
        }
#endif /* LIBXML_PUSH_ENABLED */
#ifdef HAVE_MMAP
	else if ((!strcmp(argv[i], "-memory")) ||
	         (!strcmp(argv[i], "--memory")))
	    memory++;
#endif
	else if ((!strcmp(argv[i], "-testIO")) ||
	         (!strcmp(argv[i], "--testIO")))
	    testIO++;
#ifdef LIBXML_XINCLUDE_ENABLED
	else if ((!strcmp(argv[i], "-xinclude")) ||
	         (!strcmp(argv[i], "--xinclude"))) {
	    xinclude++;
	    options |= XML_PARSE_XINCLUDE;
	}
	else if ((!strcmp(argv[i], "-noxincludenode")) ||
	         (!strcmp(argv[i], "--noxincludenode"))) {
	    xinclude++;
	    options |= XML_PARSE_XINCLUDE;
	    options |= XML_PARSE_NOXINCNODE;
	}
	else if ((!strcmp(argv[i], "-nofixup-base-uris")) ||
	         (!strcmp(argv[i], "--nofixup-base-uris"))) {
	    xinclude++;
	    options |= XML_PARSE_XINCLUDE;
	    options |= XML_PARSE_NOBASEFIX;
	}
#endif
#ifdef LIBXML_OUTPUT_ENABLED
#ifdef HAVE_ZLIB_H
	else if ((!strcmp(argv[i], "-compress")) ||
	         (!strcmp(argv[i], "--compress"))) {
	    compress++;
	    xmlSetCompressMode(9);
        }
#endif
#endif /* LIBXML_OUTPUT_ENABLED */
	else if ((!strcmp(argv[i], "-nowarning")) ||
	         (!strcmp(argv[i], "--nowarning"))) {
	    xmlGetWarningsDefaultValue = 0;
	    xmlPedanticParserDefault(0);
	    options |= XML_PARSE_NOWARNING;
        }
	else if ((!strcmp(argv[i], "-pedantic")) ||
	         (!strcmp(argv[i], "--pedantic"))) {
	    xmlGetWarningsDefaultValue = 1;
	    xmlPedanticParserDefault(1);
	    options |= XML_PARSE_PEDANTIC;
        }
#ifdef LIBXML_DEBUG_ENABLED
	else if ((!strcmp(argv[i], "-debugent")) ||
		 (!strcmp(argv[i], "--debugent"))) {
	    debugent++;
	    xmlParserDebugEntities = 1;
	}
#endif
#ifdef LIBXML_C14N_ENABLED
	else if ((!strcmp(argv[i], "-c14n")) ||
		 (!strcmp(argv[i], "--c14n"))) {
	    canonical++;
	    options |= XML_PARSE_NOENT | XML_PARSE_DTDATTR | XML_PARSE_DTDLOAD;
	}
	else if ((!strcmp(argv[i], "-c14n11")) ||
		 (!strcmp(argv[i], "--c14n11"))) {
	    canonical_11++;
	    options |= XML_PARSE_NOENT | XML_PARSE_DTDATTR | XML_PARSE_DTDLOAD;
	}
	else if ((!strcmp(argv[i], "-exc-c14n")) ||
		 (!strcmp(argv[i], "--exc-c14n"))) {
	    exc_canonical++;
	    options |= XML_PARSE_NOENT | XML_PARSE_DTDATTR | XML_PARSE_DTDLOAD;
	}
#endif
#ifdef LIBXML_CATALOG_ENABLED
	else if ((!strcmp(argv[i], "-catalogs")) ||
		 (!strcmp(argv[i], "--catalogs"))) {
	    catalogs++;
	} else if ((!strcmp(argv[i], "-nocatalogs")) ||
		 (!strcmp(argv[i], "--nocatalogs"))) {
	    nocatalogs++;
	}
#endif
	else if ((!strcmp(argv[i], "-encode")) ||
	         (!strcmp(argv[i], "--encode"))) {
	    i++;
	    encoding = argv[i];
	    /*
	     * OK it's for testing purposes
	     */
	    xmlAddEncodingAlias("UTF-8", "DVEnc");
        }
	else if ((!strcmp(argv[i], "-noblanks")) ||
	         (!strcmp(argv[i], "--noblanks"))) {
	    noblanks++;
	    xmlKeepBlanksDefault(0);
	    options |= XML_PARSE_NOBLANKS;
        }
	else if ((!strcmp(argv[i], "-maxmem")) ||
	         (!strcmp(argv[i], "--maxmem"))) {
	     i++;
	     if (sscanf(argv[i], "%d", &maxmem) == 1) {
	         xmlMemSetup(myFreeFunc, myMallocFunc, myReallocFunc,
		             myStrdupFunc);
	     } else {
	         maxmem = 0;
	     }
        }
	else if ((!strcmp(argv[i], "-format")) ||
	         (!strcmp(argv[i], "--format"))) {
	     noblanks++;
#ifdef LIBXML_OUTPUT_ENABLED
	     format = 1;
#endif /* LIBXML_OUTPUT_ENABLED */
	     xmlKeepBlanksDefault(0);
	}
	else if ((!strcmp(argv[i], "-pretty")) ||
	         (!strcmp(argv[i], "--pretty"))) {
	     i++;
#ifdef LIBXML_OUTPUT_ENABLED
       if (argv[i] != NULL) {
	         format = atoi(argv[i]);
	         if (format == 1) {
	             noblanks++;
	             xmlKeepBlanksDefault(0);
	         }
       }
#endif /* LIBXML_OUTPUT_ENABLED */
	}
#ifdef LIBXML_READER_ENABLED
	else if ((!strcmp(argv[i], "-stream")) ||
	         (!strcmp(argv[i], "--stream"))) {
	     stream++;
	}
	else if ((!strcmp(argv[i], "-walker")) ||
	         (!strcmp(argv[i], "--walker"))) {
	     walker++;
             noout++;
	}
#endif /* LIBXML_READER_ENABLED */
#ifdef LIBXML_SAX1_ENABLED
	else if ((!strcmp(argv[i], "-sax1")) ||
	         (!strcmp(argv[i], "--sax1"))) {
	    sax1++;
	    options |= XML_PARSE_SAX1;
	}
#endif /* LIBXML_SAX1_ENABLED */
	else if ((!strcmp(argv[i], "-sax")) ||
	         (!strcmp(argv[i], "--sax"))) {
	    sax++;
	}
	else if ((!strcmp(argv[i], "-chkregister")) ||
	         (!strcmp(argv[i], "--chkregister"))) {
	    chkregister++;
#ifdef LIBXML_SCHEMAS_ENABLED
	} else if ((!strcmp(argv[i], "-relaxng")) ||
	         (!strcmp(argv[i], "--relaxng"))) {
	    i++;
	    relaxng = argv[i];
	    noent++;
	    options |= XML_PARSE_NOENT;
	} else if ((!strcmp(argv[i], "-schema")) ||
	         (!strcmp(argv[i], "--schema"))) {
	    i++;
	    schema = argv[i];
	    noent++;
#endif
#ifdef LIBXML_SCHEMATRON_ENABLED
	} else if ((!strcmp(argv[i], "-schematron")) ||
	         (!strcmp(argv[i], "--schematron"))) {
	    i++;
	    schematron = argv[i];
	    noent++;
#endif
        } else if ((!strcmp(argv[i], "-nonet")) ||
                   (!strcmp(argv[i], "--nonet"))) {
	    options |= XML_PARSE_NONET;
	    xmlSetExternalEntityLoader(xmlNoNetExternalEntityLoader);
        } else if ((!strcmp(argv[i], "-nocompact")) ||
                   (!strcmp(argv[i], "--nocompact"))) {
	    options &= ~XML_PARSE_COMPACT;
	} else if ((!strcmp(argv[i], "-load-trace")) ||
	           (!strcmp(argv[i], "--load-trace"))) {
	    load_trace++;
        } else if ((!strcmp(argv[i], "-path")) ||
                   (!strcmp(argv[i], "--path"))) {
	    i++;
	    parsePath(BAD_CAST argv[i]);
#ifdef LIBXML_PATTERN_ENABLED
        } else if ((!strcmp(argv[i], "-pattern")) ||
                   (!strcmp(argv[i], "--pattern"))) {
	    i++;
	    pattern = argv[i];
#endif
#ifdef LIBXML_XPATH_ENABLED
        } else if ((!strcmp(argv[i], "-xpath")) ||
                   (!strcmp(argv[i], "--xpath"))) {
	    i++;
	    noout++;
	    xpathquery = argv[i];
#endif
	} else if ((!strcmp(argv[i], "-oldxml10")) ||
	           (!strcmp(argv[i], "--oldxml10"))) {
	    oldxml10++;
	    options |= XML_PARSE_OLD10;
	} else {
	    fprintf(stderr, "Unknown option %s\n", argv[i]);
	    usage(argv[0]);
	    {if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return(1);}
	}
    }

#ifdef LIBXML_CATALOG_ENABLED
    if (nocatalogs == 0) {
	if (catalogs) {
	    const char *catal;

	    catal = getenv("SGML_CATALOG_FILES");
	    if (catal != NULL) {
		xmlLoadCatalogs(catal);
	    } else {
		fprintf(stderr, "Variable $SGML_CATALOG_FILES not set\n");
	    }
	}
    }
#endif

#ifdef LIBXML_SAX1_ENABLED
    if (sax1)
        xmlSAXDefaultVersion(1);
    else
        xmlSAXDefaultVersion(2);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
#endif /* LIBXML_SAX1_ENABLED */

    if (chkregister) {
	xmlRegisterNodeDefault(registerNode);
	xmlDeregisterNodeDefault(deregisterNode);
    }

    indent = getenv("XMLLINT_INDENT");
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
    if(indent != NULL) {
	xmlTreeIndentString = indent;
    }


    defaultEntityLoader = xmlGetExternalEntityLoader();
    xmlSetExternalEntityLoader(xmllintExternalEntityLoader);

    xmlLineNumbersDefault(1);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
    if (loaddtd != 0)
	xmlLoadExtDtdDefaultValue |= XML_DETECT_IDS;
    if (dtdattrs)
	xmlLoadExtDtdDefaultValue |= XML_COMPLETE_ATTRS;
    if (noent != 0) xmlSubstituteEntitiesDefault(1);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
#ifdef LIBXML_VALID_ENABLED
    if (valid != 0) xmlDoValidityCheckingDefaultValue = 1;
#endif /* LIBXML_VALID_ENABLED */
    if ((htmlout) && (!nowrap)) {
	xmlGenericError(xmlGenericErrorContext,
         "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"\n");
	xmlGenericError(xmlGenericErrorContext,
		"\t\"http://www.w3.org/TR/REC-html40/loose.dtd\">\n");
	xmlGenericError(xmlGenericErrorContext,
	 "<html><head><title>%s output</title></head>\n",
		argv[0]);
	xmlGenericError(xmlGenericErrorContext,
	 "<body bgcolor=\"#ffffff\"><h1 align=\"center\">%s output</h1>\n",
		argv[0]);
    }

#ifdef LIBXML_SCHEMATRON_ENABLED
    if ((schematron != NULL) && (sax == 0)
#ifdef LIBXML_READER_ENABLED
        && (stream == 0)
#endif /* LIBXML_READER_ENABLED */
	) {
	xmlSchematronParserCtxtPtr ctxt;

        /* forces loading the DTDs */
        xmlLoadExtDtdDefaultValue |= 1;
	options |= XML_PARSE_DTDLOAD;
	if (timing) {
	    startTimer();
	}
	ctxt = xmlSchematronNewParserCtxt(schematron);
#if 0
	xmlSchematronSetParserErrors(ctxt,
		(xmlSchematronValidityErrorFunc) fprintf,
		(xmlSchematronValidityWarningFunc) fprintf,
		stderr);
#endif
	wxschematron = xmlSchematronParse(ctxt);
	if (wxschematron == NULL) {
	    xmlGenericError(xmlGenericErrorContext,
		    "Schematron schema %s failed to compile\n", schematron);
            progresult = XMLLINT_ERR_SCHEMACOMP;
	    schematron = NULL;
	}
	xmlSchematronFreeParserCtxt(ctxt);
	if (timing) {
	    endTimer("Compiling the schemas");
	}
    }
#endif
#ifdef LIBXML_SCHEMAS_ENABLED
    if ((relaxng != NULL) && (sax == 0)
#ifdef LIBXML_READER_ENABLED
        && (stream == 0)
#endif /* LIBXML_READER_ENABLED */
	) {
	xmlRelaxNGParserCtxtPtr ctxt;

        /* forces loading the DTDs */
        xmlLoadExtDtdDefaultValue |= 1;
	options |= XML_PARSE_DTDLOAD;
	if (timing) {
	    startTimer();
	}
	ctxt = xmlRelaxNGNewParserCtxt(relaxng);
	xmlRelaxNGSetParserErrors(ctxt,
		(xmlRelaxNGValidityErrorFunc) fprintf,
		(xmlRelaxNGValidityWarningFunc) fprintf,
		stderr);
	relaxngschemas = xmlRelaxNGParse(ctxt);
	if (relaxngschemas == NULL) {
	    xmlGenericError(xmlGenericErrorContext,
		    "Relax-NG schema %s failed to compile\n", relaxng);
            progresult = XMLLINT_ERR_SCHEMACOMP;
	    relaxng = NULL;
	}
	xmlRelaxNGFreeParserCtxt(ctxt);
	if (timing) {
	    endTimer("Compiling the schemas");
	}
    } else if ((schema != NULL)
#ifdef LIBXML_READER_ENABLED
		&& (stream == 0)
#endif
	) {
	xmlSchemaParserCtxtPtr ctxt;

	if (timing) {
	    startTimer();
	}
	ctxt = xmlSchemaNewParserCtxt(schema);
	xmlSchemaSetParserErrors(ctxt,
		(xmlSchemaValidityErrorFunc) fprintf,
		(xmlSchemaValidityWarningFunc) fprintf,
		stderr);
	wxschemas = xmlSchemaParse(ctxt);
	if (wxschemas == NULL) {
	    xmlGenericError(xmlGenericErrorContext,
		    "WXS schema %s failed to compile\n", schema);
            progresult = XMLLINT_ERR_SCHEMACOMP;
	    schema = NULL;
	}
	xmlSchemaFreeParserCtxt(ctxt);
	if (timing) {
	    endTimer("Compiling the schemas");
	}
    }
#endif /* LIBXML_SCHEMAS_ENABLED */
#ifdef LIBXML_PATTERN_ENABLED
    if ((pattern != NULL)
#ifdef LIBXML_READER_ENABLED
        && (walker == 0)
#endif
	) {
        patternc = xmlPatterncompile((const xmlChar *) pattern, NULL, 0, NULL);
	if (patternc == NULL) {
	    xmlGenericError(xmlGenericErrorContext,
		    "Pattern %s failed to compile\n", pattern);
            progresult = XMLLINT_ERR_SCHEMAPAT;
	    pattern = NULL;
	}
    }
#endif /* LIBXML_PATTERN_ENABLED */
    for (i = 1; i < argc ; i++) {
	if ((!strcmp(argv[i], "-encode")) ||
	         (!strcmp(argv[i], "--encode"))) {
	    i++;
	    continue;
        } else if ((!strcmp(argv[i], "-o")) ||
                   (!strcmp(argv[i], "-output")) ||
                   (!strcmp(argv[i], "--output"))) {
            i++;
	    continue;
        }
#ifdef LIBXML_VALID_ENABLED
	if ((!strcmp(argv[i], "-dtdvalid")) ||
	         (!strcmp(argv[i], "--dtdvalid"))) {
	    i++;
	    continue;
        }
	if ((!strcmp(argv[i], "-path")) ||
                   (!strcmp(argv[i], "--path"))) {
            i++;
	    continue;
        }
	if ((!strcmp(argv[i], "-dtdvalidfpi")) ||
	         (!strcmp(argv[i], "--dtdvalidfpi"))) {
	    i++;
	    continue;
        }
#endif /* LIBXML_VALID_ENABLED */
	if ((!strcmp(argv[i], "-relaxng")) ||
	         (!strcmp(argv[i], "--relaxng"))) {
	    i++;
	    continue;
        }
	if ((!strcmp(argv[i], "-maxmem")) ||
	         (!strcmp(argv[i], "--maxmem"))) {
	    i++;
	    continue;
        }
	if ((!strcmp(argv[i], "-pretty")) ||
	         (!strcmp(argv[i], "--pretty"))) {
	    i++;
	    continue;
        }
	if ((!strcmp(argv[i], "-schema")) ||
	         (!strcmp(argv[i], "--schema"))) {
	    i++;
	    continue;
        }
	if ((!strcmp(argv[i], "-schematron")) ||
	         (!strcmp(argv[i], "--schematron"))) {
	    i++;
	    continue;
        }
#ifdef LIBXML_PATTERN_ENABLED
        if ((!strcmp(argv[i], "-pattern")) ||
	    (!strcmp(argv[i], "--pattern"))) {
	    i++;
	    continue;
	}
#endif
#ifdef LIBXML_XPATH_ENABLED
        if ((!strcmp(argv[i], "-xpath")) ||
	    (!strcmp(argv[i], "--xpath"))) {
	    i++;
	    continue;
	}
#endif
	if ((timing) && (repeat))
	    startTimer();
	/* Remember file names.  "-" means stdin.  <sven@zen.org> */
	if ((argv[i][0] != '-') || (strcmp(argv[i], "-") == 0)) {
	    if (repeat) {
		xmlParserCtxtPtr ctxt = NULL;

		for (acount = 0;acount < repeat;acount++) {
#ifdef LIBXML_READER_ENABLED
		    if (stream != 0) {
			streamFile(argv[i]);
		    } else {
#endif /* LIBXML_READER_ENABLED */
                        if (sax) {
			    testSAX(argv[i]);
			} else {
			    if (ctxt == NULL)
				ctxt = xmlNewParserCtxt();
			    parseAndPrintFile(argv[i], ctxt);
			}
#ifdef LIBXML_READER_ENABLED
		    }
#endif /* LIBXML_READER_ENABLED */
		}
		if (ctxt != NULL)
		    xmlFreeParserCtxt(ctxt);
	    } else {
		nbregister = 0;

#ifdef LIBXML_READER_ENABLED
		if (stream != 0)
		    streamFile(argv[i]);
		else
#endif /* LIBXML_READER_ENABLED */
                if (sax) {
		    testSAX(argv[i]);
		} else {
		    parseAndPrintFile(argv[i], NULL);
		}

                if ((chkregister) && (nbregister != 0)) {
		    fprintf(stderr, "Registration count off: %d\n", nbregister);
		    progresult = XMLLINT_ERR_RDREGIS;
		}
	    }
	    files ++;
	    if ((timing) && (repeat)) {
		endTimer("%d iterations", repeat);
	    }
	}
    }
    if (generate)
	parseAndPrintFile(NULL, NULL);
    if ((htmlout) && (!nowrap)) {
	xmlGenericError(xmlGenericErrorContext, "</body></html>\n");
    }
    if ((files == 0) && (!generate) && (version == 0)) {
	usage(argv[0]);
    }
#ifdef LIBXML_SCHEMATRON_ENABLED
    if (wxschematron != NULL)
	xmlSchematronFree(wxschematron);
#endif
#ifdef LIBXML_SCHEMAS_ENABLED
    if (relaxngschemas != NULL)
	xmlRelaxNGFree(relaxngschemas);
    if (wxschemas != NULL)
	xmlSchemaFree(wxschemas);
    xmlRelaxNGCleanupTypes();
#endif
#ifdef LIBXML_PATTERN_ENABLED
    if (patternc != NULL)
        xmlFreePattern(patternc);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
#endif
    xmlCleanupParser();
    xmlMemoryDump();
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

    {if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return(progresult);}
}

