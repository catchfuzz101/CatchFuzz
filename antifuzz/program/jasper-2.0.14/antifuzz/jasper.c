/*
 * Copyright (c) 1999-2000 Image Power, Inc. and the University of
 *   British Columbia.
 * Copyright (c) 2001-2003 Michael David Adams.
 * All rights reserved.
 */

/* __START_OF_JASPER_LICENSE__
 * 
 * JasPer License Version 2.0
 * 
 * Copyright (c) 2001-2006 Michael David Adams
 * Copyright (c) 1999-2000 Image Power, Inc.
 * Copyright (c) 1999-2000 The University of British Columbia
 * 
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person (the
 * "User") obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 * 
 * 1.  The above copyright notices and this permission notice (which
 * includes the disclaimer below) shall be included in all copies or
 * substantial portions of the Software.
 * 
 * 2.  The name of a copyright holder shall not be used to endorse or
 * promote products derived from the Software without specific prior
 * written permission.
 * 
 * THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
 * LICENSE.  NO USE OF THE SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER
 * THIS DISCLAIMER.  THE SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
 * "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.  IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.  NO ASSURANCES ARE
 * PROVIDED BY THE COPYRIGHT HOLDERS THAT THE SOFTWARE DOES NOT INFRINGE
 * THE PATENT OR OTHER INTELLECTUAL PROPERTY RIGHTS OF ANY OTHER ENTITY.
 * EACH COPYRIGHT HOLDER DISCLAIMS ANY LIABILITY TO THE USER FOR CLAIMS
 * BROUGHT BY ANY OTHER ENTITY BASED ON INFRINGEMENT OF INTELLECTUAL
 * PROPERTY RIGHTS OR OTHERWISE.  AS A CONDITION TO EXERCISING THE RIGHTS
 * GRANTED HEREUNDER, EACH USER HEREBY ASSUMES SOLE RESPONSIBILITY TO SECURE
 * ANY OTHER INTELLECTUAL PROPERTY RIGHTS NEEDED, IF ANY.  THE SOFTWARE
 * IS NOT FAULT-TOLERANT AND IS NOT INTENDED FOR USE IN MISSION-CRITICAL
 * SYSTEMS, SUCH AS THOSE USED IN THE OPERATION OF NUCLEAR FACILITIES,
 * AIRCRAFT NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL
 * SYSTEMS, DIRECT LIFE SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH
 * THE FAILURE OF THE SOFTWARE OR SYSTEM COULD LEAD DIRECTLY TO DEATH,
 * PERSONAL INJURY, OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH
 * RISK ACTIVITIES").  THE COPYRIGHT HOLDERS SPECIFICALLY DISCLAIM ANY
 * EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR HIGH RISK ACTIVITIES.
 * 
 * __END_OF_JASPER_LICENSE__
 */

/*
 * JasPer Transcoder Program
 *
 * $Id$
 */

/******************************************************************************\
* Includes.
\******************************************************************************/

#include <stdio.h>
unsigned char *start_addr;
unsigned char *end_addr;
int antifuzz_check=0;
int canary_xor;
int fake_arr[1]={0};
#include <stdlib.h>
#include <stdint.h>
#include <regex.h>
#include <string.h>


#include <assert.h>
#include <time.h>
#include <stdint.h>

#include <jasper/jasper.h>
#include <jasper/jas_debug.h>

/******************************************************************************\
*
\******************************************************************************/

#define OPTSMAX	4096

/******************************************************************************\
* Types.
\******************************************************************************/

/* Encoder command line options. */

typedef struct {

	char *infile;
	/* The input image file. */

	int infmt;
	/* The input image file format. */

	char *inopts;
	char inoptsbuf[OPTSMAX + 1];

	char *outfile;
	/* The output image file. */

	int outfmt;

	char *outopts;
	char outoptsbuf[OPTSMAX + 1];

	int verbose;
	/* Verbose mode. */

	int debug;

	int version;

	int_fast32_t cmptno;

	int srgb;

	size_t max_mem;

} cmdopts_t;

/******************************************************************************\
* Local prototypes.
\******************************************************************************/

cmdopts_t *cmdopts_parse(int argc, char **argv);
void cmdopts_destroy(cmdopts_t *cmdopts);
void cmdusage(void);
void badusage(void);
void cmdinfo(void);
int addopt(char *optstr, int maxlen, char *s);

/******************************************************************************\
* Global data.
\******************************************************************************/

char *cmdname = "";

/******************************************************************************\
* Code.
\******************************************************************************/

int w0(unsigned char* buf, unsigned int len){
	printf("w0 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 0 % len;
    int index2 = 1 % len;
    int index3 = 2 % len;
    int index4 = 3 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3902515186) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1507252891) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 603338905) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1696904468) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1065828383) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3048701182) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1392146492) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3902515186) {
        ret = 12;
      }
    }
    if(buf[index1] > 56) {
      if(buf[index2] < 118) {
        if(buf[index3] - buf[index4] < 56) {
          if(buf[index3] + buf[index4] > 118) {
            if(((buf[index1] + buf[index2]) % 9) == 0) {
              if(((buf[index1] * buf[index2]) % 5) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 56) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }

  return ret;
}

int w1(unsigned char* buf, unsigned int len){
  printf("w1 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 1 % len;
    int index2 = 2 % len;
    int index3 = 3 % len;
    int index4 = 4 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3649224605) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2350045607) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3014389030) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2122141901) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1860222409) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2127411081) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3096905285) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3649224605) {
        ret = 12;
      }
    }
    if(buf[index1] > 209) {
      if(buf[index2] < 215) {
        if(buf[index3] - buf[index4] < 209) {
          if(buf[index3] + buf[index4] > 215) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 209) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w2(unsigned char* buf, unsigned int len){
	printf("w2 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 2 % len;
    int index2 = 3 % len;
    int index3 = 4 % len;
    int index4 = 5 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3060206730) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1823329745) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1798358539) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1130645893) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 334868504) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1095408358) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2855184194) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3060206730) {
        ret = 12;
      }
    }
    if(buf[index1] > 236) {
      if(buf[index2] < 125) {
        if(buf[index3] - buf[index4] < 236) {
          if(buf[index3] + buf[index4] > 125) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  64) {
                  if((buf[index1] ^ buf[index2]) == 236) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w3(unsigned char* buf, unsigned int len){
	  printf("w3 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 3 % len;
    int index2 = 4 % len;
    int index3 = 5 % len;
    int index4 = 6 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1306562184) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2309250971) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 702642080) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3193801888) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1618453275) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3908944833) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1426801106) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1306562184) {
        ret = 12;
      }
    }
    if(buf[index1] > 33) {
      if(buf[index2] < 13) {
        if(buf[index3] - buf[index4] < 33) {
          if(buf[index3] + buf[index4] > 13) {
            if(((buf[index1] + buf[index2]) % 6) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 33) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w4(unsigned char* buf, unsigned int len){
	  printf("w4 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 4 % len;
    int index2 = 5 % len;
    int index3 = 6 % len;
    int index4 = 7 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1317201472) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 4182791665) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1712271078) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3769200889) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3974150540) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1676806712) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 503006531) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1317201472) {
        ret = 12;
      }
    }
    if(buf[index1] > 225) {
      if(buf[index2] < 134) {
        if(buf[index3] - buf[index4] < 225) {
          if(buf[index3] + buf[index4] > 134) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 225) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w5(unsigned char* buf, unsigned int len){
	  printf("w5 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 5 % len;
    int index2 = 6 % len;
    int index3 = 7 % len;
    int index4 = 8 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 2680581394) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798508734) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1827993300) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 713768984) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4009010704) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1285272007) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4015010868) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 2680581394) {
        ret = 12;
      }
    }
    if(buf[index1] > 221) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 221) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 221) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w6(unsigned char* buf, unsigned int len){
	  printf("w6 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 6 % len;
    int index2 = 7 % len;
    int index3 = 8 % len;
    int index4 = 9 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1617115864) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1631690012) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3460463880) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3382779234) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1649578633) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 728322968) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2998716203) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1617115864) {
        ret = 12;
      }
    }
    if(buf[index1] > 132) {
      if(buf[index2] < 208) {
        if(buf[index3] - buf[index4] < 132) {
          if(buf[index3] + buf[index4] > 208) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  32) {
                  if((buf[index1] ^ buf[index2]) == 132) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w7(unsigned char* buf, unsigned int len){
	  printf("w7 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 7 % len;
    int index2 = 8 % len;
    int index3 = 9 % len;
    int index4 = 10 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1208351771) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2227323053) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1104939852) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4258576467) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 780873126) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2155013799) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1158125450) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1208351771) {
        ret = 12;
      }
    }
    if(buf[index1] > 18) {
      if(buf[index2] < 181) {
        if(buf[index3] - buf[index4] < 18) {
          if(buf[index3] + buf[index4] > 181) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  1) {
                  if((buf[index1] ^ buf[index2]) == 18) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w8(unsigned char* buf, unsigned int len){
	  printf("w8 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 8 % len;
    int index2 = 9 % len;
    int index3 = 10 % len;
    int index4 = 11 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 323927678) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798644434) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 2384399227) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 361926757) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 897612148) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 990255627) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3311835330) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 323927678) {
        ret = 12;
      }
    }
    if(buf[index1] > 100) {
      if(buf[index2] < 196) {
        if(buf[index3] - buf[index4] < 100) {
          if(buf[index3] + buf[index4] > 196) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 100) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w9(unsigned char* buf, unsigned int len){
	  printf("w9 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 9 % len;
    int index2 = 10 % len;
    int index3 = 11 % len;
    int index4 = 12 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 720174660) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2124116700) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3937385190) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1443256077) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1256522327) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1727523764) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2994088857) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 720174660) {
        ret = 12;
      }
    }
    if(buf[index1] > 249) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 249) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 8) == 0) {
                if((buf[index1] ^ buf[index2]) &  8) {
                  if((buf[index1] ^ buf[index2]) == 249) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}



typedef int (*obfuscation_call_heavyweight)(unsigned char* buf, unsigned int len);
obfuscation_call_heavyweight functions_array[10] = {
  
  w0,
  
  w1,
  
  w2,
  
  w3,
  
  w4,
  
  w5,
  
  w6,
  
  w7,
  
  w8,
  
  w9
};

//get the distance between two points in 1D
float getDist(float p1, float p2) {
	float dist; /*distance between two points*/

	dist = fabsf(p1 - p2); /*in 1D case, that distance would just be the absolute value of the subtraction of two values*/

	return dist;
}

/*get the nearest distance from a given point to other centers
 *return the label number the nearest center
 */
int getNearestDist(float array[], int p, int k) {
	float nearestDist = 300; /*in this particular data set, we're deal with percentage, so the largest distance between two point would be 1*/
	float dist; /*distance between two points*/
	int i; /*counter and index*/
	int label=-1; /*label number of each cluster*/

	//compare the distance between the given point and each center
	for (i = 0; i < k; i++) {
		dist = getDist(array[i], p);
		//find the closest center and update the label to be the label number of this cluster
		if (dist < nearestDist) {
			nearestDist = dist;
			label = i;
		}
	}

	return label;
}

/*get the mean of each cluster
 *set that value as new center
 */
float getMean(unsigned int vArray[], int lArray[], int label, int size) {
	float sum = 0.0; /*sum of distances from center to all points in this cluster*/
	int counter = 0;
	float mean;
	int i = 0;

	for (; i < size; i++) {
		if (lArray[i] == label) {
			sum += vArray[i];
			counter++;
		}
	}

	if (counter != 0) {
		mean = sum / (float)counter;
	}
	else mean = -1;

	return mean;
}


void antifuzz(void){

	#define MAX_FILE_CONTENT_SIZE 512
	unsigned char* fileContentMult = (unsigned char*)"abcdefghijklmnopqrstuvwxyz";
	unsigned int x[0xFFFFF]={0x00,};
	unsigned int y[0xFFFFF]={0x00,};


	char pid_mem_file[50]={0x00,};
	char pid_mem_file_name[50]={0x00,};
	//char shm_name[50]={0x00,};
	//char mem_result[50]={0x00,};
	//char mem_result_after[50]={0x00,};
	//char data_tmp[50]={0x00,};
	


  pid_t pid=getpid();
  sprintf(pid_mem_file, "/proc/%d/maps", pid);
	sprintf(pid_mem_file_name, "./maps_%d", pid);
	//sprintf(shm_name, "./shm/%d", pid);
	//sprintf(mem_result, "./mem_result/%d", pid);
  //sprintf(mem_result_after, "./mem_result_after/%d", pid);
  //sprintf(data_tmp, "./data.txt");
  int canary_index, canary_val;
  while(1){
  	int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
  	if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
        else{
            fake_arr[0]=1;
            break;
        }
        canary_val=rand()%1000;
	if(fake_arr[canary_index]!=canary_val){
		fake_arr[canary_index]=canary_val;
		break;
	}
  }
  //printf("%s\n", pid_mem_file);
  FILE *fp1  = fopen(pid_mem_file, "r");
	FILE *fp2  = fopen(pid_mem_file_name, "w");
	//FILE *fp3  = fopen(shm_name, "w");
	//FILE *fp4  = fopen(mem_result, "w");
	//FILE *fp5  = fopen(mem_result_after, "w");
	//FILE *fp6  = fopen(data_tmp, "w");
  
	char buffer[128];
	regex_t state;
  regmatch_t pmatch[3];

	char addr1[18];
  char addr2[18];
  const char *pattern = "[rwx-]{3}+[s]";
  const char *addr = "([0-9a-fA-F]{12,16})-([0-9a-fA-F]{12,16})";
  strncpy(addr1,"0x",2);
  strncpy(addr2,"0x",2);

  memset(addr1+2, 0x00, sizeof(addr1)-2);
  memset(addr2+2, 0x00, sizeof(addr2)-2);

	regcomp(&state, pattern, REG_EXTENDED);
  int index;
  while (fgets(buffer, 128, fp1) != NULL)
  {	
      fprintf(fp2, "%s", buffer);
			int status = regexec(&state, buffer, 1, pmatch, 0);
			if(status==0){
	    	//fprintf(fp3,"With the whole expression, "
	           // "a matched substring \"%.*s\" is found at position %d to %d.\n",
	            //pmatch[0].rm_eo - pmatch[0].rm_so, &buffer[pmatch[0].rm_so],
	           // pmatch[0].rm_so, pmatch[0].rm_eo - 1);
      	regfree(&state);
      	regcomp(&state, addr, REG_EXTENDED);
      	status = regexec(&state, buffer, 3, pmatch, 0);
      	if(status==0){
					//fprintf(fp3, "With the whole expression, "
					//		"a matched substring \"%.*s\" is found at position %d to %d.\n",
					//		pmatch[0].rm_eo - pmatch[0].rm_so, &buffer[pmatch[0].rm_so],
					//		pmatch[0].rm_so, pmatch[0].rm_eo - 1);
					
        	strncpy(addr1+2,buffer+pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
        	strncpy(addr2+2,buffer+pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
		

        	//fprintf(fp3,"%s\n", addr1);
        	//fprintf(fp3,"%s\n", addr2);
        	start_addr=(unsigned char*)strtoll(addr1,NULL,16);
        	end_addr=(unsigned char*)strtoll(addr2,NULL,16);
        	//fprintf(fp3,"%lld\n",(long long int)start_addr);
        	//fprintf(fp3,"%lld\n",(long long int)end_addr);
        	//fprintf(fp3,"%d\n", start_addr[0]);

			
					for (int i = 0; i < end_addr-start_addr; i++) {
						x[i]=i;
						y[i] = start_addr[i];
					}
					//fprintf(fp4, "SHM_SIZE: %d\n", end_addr-start_addr);
					//fprintf(fp6, "SHM_SIZE: %d\n", end_addr-start_addr);
					//for(int i=0;i<end_addr-start_addr;i++){
					//	fprintf(fp4, "(%d, %d)\n",i,y[i]);
					//}

    
		      for(int i = 0; i <130; i++) {
		        functions_array[0](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i <110; i++) {
		        functions_array[1](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i < 90; i++) {
		        functions_array[2](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i < 70; i++) {
		        functions_array[3](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i < 50; i++) {
		        functions_array[4](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }

		      int k=0;
		      for (int i = 0; i < end_addr-start_addr; i++){
						x[i]=i;
					  int tmp_value=start_addr[i]-y[i];
					  if(tmp_value>30 && tmp_value<150)y[k++]=tmp_value;
					}

					//fprintf(fp5, "SHM_SIZE: %d\n", end_addr-start_addr);
					//for(int i=0;i<end_addr-start_addr;i++){
					//	fprintf(fp5, "(%d, %d)\n",i,y[i]);
					//}

					
					//for(int i=0;i<k-1;i++){
					//	fprintf(fp6, "%d, ", y[i]);
					//}
					//fprintf(fp6, "%d\nNUM: %d\n", y[k-1],k);
		      int labelArray[k];
		      int orig_centerArray[5] = { 50,70,90,110,130 };
		      float centerArray[5] = { 50,70,90,110,130 };/* store the mean value of each cluster at corresponding location */
		      float tmp_centerArray[5] = { 0, };
      

		      int loop = 0;
		      while(1) {

		        loop++;
		        //write in label for each data point
		        for (int i = 0; i < k; i++) {
		          int label = getNearestDist(centerArray, y[i], 5);
		          labelArray[i] = label;
		       	}

		        /*calculate the mean value of each cluster
		        *set it as the new center
		        */
		        int finish = 0;

		        for (int i = 0; i < 5; i++) {
		          tmp_centerArray[i] = centerArray[i];
		          centerArray[i] = getMean(y, labelArray, i, k);
		          if (tmp_centerArray[i] == centerArray[i]) {
		            finish += 1;
		          }
		          
          	//printf("new center is %f\n", centerArray[i]);
        		}
		        if (finish == 5) {
		          //fprintf(fp6, "%d DONE\n", loop);
		          break;
		        }
      		}

		      float distance = 0;
		      int tmp_index=5;
		      for (int i = 0; i < tmp_index; i++) {
		        //fprintf(fp6, "center %d  is %f\n", i, centerArray[i]);
		        if (centerArray[i] == -1)tmp_index -= 1;
		        else distance+=(centerArray[i] - orig_centerArray[i])*(centerArray[i]-orig_centerArray[i]);
		      }
		      distance /=tmp_index;
		      //fprintf(fp6, "distance is %f\n", distance);

					if(distance<10){
						//fprintf(fp6, "===FUZZ DETECTED===ANTIFUZZ START===\n");					
						antifuzz_check=1;
						for (int i = 0; i < end_addr-start_addr; i++) {
						  start_addr[i]=0x80;
						}
					}
      		//else fprintf(fp6, "FUZZ NOT DETECTED!\n");
      
					break;
     	}
   	}
 	}
	regfree(&state);

  
	
  fclose(fp1);
	fclose(fp2);
	//fclose(fp3);
	//fclose(fp4);
  	//fclose(fp5);
	//fclose(fp6);
 
  unlink(pid_mem_file_name);
	//unlink(shm_name);
	//unlink(mem_result);
	//unlink(mem_result_after);

}

int main(int argc, char **argv)
{
	antifuzz();
	jas_image_t *image;
	cmdopts_t *cmdopts;
	jas_stream_t *in;
	jas_stream_t *out;
	jas_tmr_t dectmr;
	jas_tmr_t enctmr;
	double dectime;
	double enctime;
	int_fast16_t numcmpts;
	int i;

	/* Determine the base name of this command. */
	if ((cmdname = strrchr(argv[0], '/'))) {
		++cmdname;
	} else {
		cmdname = argv[0];
	}

	if (jas_init()) {
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;abort();}
	}

	/* Parse the command line options. */
	if (!(cmdopts = cmdopts_parse(argc, argv))) {
		fprintf(stderr, "error: cannot parse command line\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
	}

	if (cmdopts->version) {
		printf("%s\n", JAS_VERSION);
		fprintf(stderr, "libjasper %s\n", jas_getversion());
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_SUCCESS);}
	}

	jas_setdbglevel(cmdopts->debug);
#if defined(JAS_DEFAULT_MAX_MEM_USAGE)
	jas_set_max_mem_usage(cmdopts->max_mem);
#endif

	if (cmdopts->verbose) {
		cmdinfo();
	}

	/* Open the input image file. */
	if (cmdopts->infile) {
		/* The input image is to be read from a file. */
		if (!(in = jas_stream_fopen(cmdopts->infile, "rb"))) {
			fprintf(stderr, "error: cannot open input image file %s\n",
			  cmdopts->infile);
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
		}
	} else {
		/* The input image is to be read from standard input. */
		if (!(in = jas_stream_fdopen(0, "rb"))) {
			fprintf(stderr, "error: cannot open standard input\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
		}
	}

	/* Open the output image file. */
	if (cmdopts->outfile) {
		/* The output image is to be written to a file. */
		if (!(out = jas_stream_fopen(cmdopts->outfile, "w+b"))) {
			fprintf(stderr, "error: cannot open output image file %s\n",
			  cmdopts->outfile);
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
		}
	} else {
		/* The output image is to be written to standard output. */
		if (!(out = jas_stream_fdopen(1, "w+b"))) {
			fprintf(stderr, "error: cannot open standard output\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
		}
	}

	if (cmdopts->infmt < 0) {
		if ((cmdopts->infmt = jas_image_getfmt(in)) < 0) {
			fprintf(stderr, "error: input image has unknown format\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
		}
	}

	/* Get the input image data. */
	jas_tmr_start(&dectmr);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	if (!(image = jas_image_decode(in, cmdopts->infmt, cmdopts->inopts))) {
		fprintf(stderr, "error: cannot load image data\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
	}
	jas_tmr_stop(&dectmr);
	dectime = jas_tmr_get(&dectmr);

	/* If requested, throw away all of the components except one.
	  Why might this be desirable?  It is a hack, really.
	  None of the image formats other than the JPEG-2000 ones support
	  images with two, four, five, or more components.  This hack
	  allows such images to be decoded with the non-JPEG-2000 decoders,
	  one component at a time. */
	numcmpts = jas_image_numcmpts(image);
	if (cmdopts->cmptno >= 0 && cmdopts->cmptno < numcmpts) {
		for (i = numcmpts - 1; i >= 0; --i) {
			if (i != cmdopts->cmptno) {
				jas_image_delcmpt(image, i);
			}
		}
	}

	if (cmdopts->srgb) {
		jas_image_t *newimage;
		jas_cmprof_t *outprof;
		jas_eprintf("forcing conversion to sRGB\n");
		if (!(outprof = jas_cmprof_createfromclrspc(JAS_CLRSPC_SRGB))) {
			jas_eprintf("cannot create sRGB profile\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
		}
		if (!(newimage = jas_image_chclrspc(image, outprof, JAS_CMXFORM_INTENT_PER))) {
			jas_eprintf("cannot convert to sRGB\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
		}
		jas_image_destroy(image);
		jas_cmprof_destroy(outprof);
		image = newimage;
	}

	/* Generate the output image data. */
	jas_tmr_start(&enctmr);
	if (jas_image_encode(image, out, cmdopts->outfmt, cmdopts->outopts)) {
		fprintf(stderr, "error: cannot encode image\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
	}
	jas_stream_flush(out);
	jas_tmr_stop(&enctmr);
	enctime = jas_tmr_get(&enctmr);

	if (cmdopts->verbose) {
		fprintf(stderr, "decoding time = %f\n", dectime);
		fprintf(stderr, "encoding time = %f\n", enctime);
	}

	/* If this fails, we don't care. */
	(void) jas_stream_close(in);

	/* Close the output image stream. */
	if (jas_stream_close(out)) {
		fprintf(stderr, "error: cannot close output image file\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
	}

	cmdopts_destroy(cmdopts);
	jas_image_destroy(image);
	jas_image_clearfmts();

	/* Success at last! :-) */
	{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return EXIT_SUCCESS;}
}

cmdopts_t *cmdopts_parse(int argc, char **argv)
{

	typedef enum {
		CMDOPT_HELP = 0,
		CMDOPT_VERBOSE,
		CMDOPT_INFILE,
		CMDOPT_INFMT,
		CMDOPT_INOPT,
		CMDOPT_OUTFILE,
		CMDOPT_OUTFMT,
		CMDOPT_OUTOPT,
		CMDOPT_VERSION,
		CMDOPT_DEBUG,
		CMDOPT_CMPTNO,
		CMDOPT_SRGB,
		CMDOPT_MAXMEM
	} cmdoptid_t;

	static jas_opt_t cmdoptions[] = {
		{CMDOPT_HELP, "help", 0},
		{CMDOPT_VERBOSE, "verbose", 0},
		{CMDOPT_INFILE, "input", JAS_OPT_HASARG},
		{CMDOPT_INFILE, "f", JAS_OPT_HASARG},
		{CMDOPT_INFMT, "input-format", JAS_OPT_HASARG},
		{CMDOPT_INFMT, "t", JAS_OPT_HASARG},
		{CMDOPT_INOPT, "input-option", JAS_OPT_HASARG},
		{CMDOPT_INOPT, "o", JAS_OPT_HASARG},
		{CMDOPT_OUTFILE, "output", JAS_OPT_HASARG},
		{CMDOPT_OUTFILE, "F", JAS_OPT_HASARG},
		{CMDOPT_OUTFMT, "output-format", JAS_OPT_HASARG},
		{CMDOPT_OUTFMT, "T", JAS_OPT_HASARG},
		{CMDOPT_OUTOPT, "output-option", JAS_OPT_HASARG},
		{CMDOPT_OUTOPT, "O", JAS_OPT_HASARG},
		{CMDOPT_VERSION, "version", 0},
		{CMDOPT_DEBUG, "debug-level", JAS_OPT_HASARG},
		{CMDOPT_CMPTNO, "cmptno", JAS_OPT_HASARG},
		{CMDOPT_SRGB, "force-srgb", 0},
		{CMDOPT_SRGB, "S", 0},
#if defined(JAS_DEFAULT_MAX_MEM_USAGE)
		{CMDOPT_MAXMEM, "memory-limit", JAS_OPT_HASARG},
#endif
		{-1, 0, 0}
	};

	cmdopts_t *cmdopts;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	int c;

	if (!(cmdopts = malloc(sizeof(cmdopts_t)))) {
		fprintf(stderr, "error: insufficient memory\n");
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
	}

	cmdopts->infile = 0;
	cmdopts->infmt = -1;
	cmdopts->inopts = 0;
	cmdopts->inoptsbuf[0] = '\0';
	cmdopts->outfile = 0;
	cmdopts->outfmt = -1;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	cmdopts->outopts = 0;
	cmdopts->outoptsbuf[0] = '\0';
	cmdopts->verbose = 0;
	cmdopts->version = 0;
	cmdopts->cmptno = -1;
	cmdopts->debug = 0;
	cmdopts->srgb = 0;
#if defined(JAS_DEFAULT_MAX_MEM_USAGE)
	cmdopts->max_mem = JAS_DEFAULT_MAX_MEM_USAGE;
#endif

	while ((c = jas_getopt(argc, argv, cmdoptions)) != EOF) {
		switch (c) {
		case CMDOPT_HELP:
			cmdusage();
			break;
		case CMDOPT_VERBOSE:
			cmdopts->verbose = 1;
			break;
		case CMDOPT_VERSION:
			cmdopts->version = 1;
			break;
		case CMDOPT_DEBUG:
			cmdopts->debug = atoi(jas_optarg);
			break;
		case CMDOPT_INFILE:
			cmdopts->infile = jas_optarg;
			break;
		case CMDOPT_INFMT:
			if ((cmdopts->infmt = jas_image_strtofmt(jas_optarg)) < 0) {
				fprintf(stderr, "warning: ignoring invalid input format %s\n",
				  jas_optarg);
				cmdopts->infmt = -1;
			}
			break;
		case CMDOPT_INOPT:
			addopt(cmdopts->inoptsbuf, OPTSMAX, jas_optarg);
			cmdopts->inopts = cmdopts->inoptsbuf;
			break;
		case CMDOPT_OUTFILE:
			cmdopts->outfile = jas_optarg;
			break;
		case CMDOPT_OUTFMT:
			if ((cmdopts->outfmt = jas_image_strtofmt(jas_optarg)) < 0) {
				fprintf(stderr, "error: invalid output format %s\n", jas_optarg);
				badusage();
			}
			break;
		case CMDOPT_OUTOPT:
			addopt(cmdopts->outoptsbuf, OPTSMAX, jas_optarg);
			cmdopts->outopts = cmdopts->outoptsbuf;
			break;
		case CMDOPT_CMPTNO:
			cmdopts->cmptno = atoi(jas_optarg);
			break;
		case CMDOPT_SRGB:
			cmdopts->srgb = 1;
			break;
		case CMDOPT_MAXMEM:
			cmdopts->max_mem = strtoull(jas_optarg, 0, 10);
			break;
		default:
			badusage();
			break;
		}
	}

	while (jas_optind < argc) {
		fprintf(stderr,
		  "warning: ignoring bogus command line argument %s\n",
		  argv[jas_optind]);
		++jas_optind;
	}

	if (cmdopts->version) {
		goto done;
	}

	if (cmdopts->outfmt < 0 && cmdopts->outfile) {
		if ((cmdopts->outfmt = jas_image_fmtfromname(cmdopts->outfile)) < 0) {
			fprintf(stderr,
			  "error: cannot guess image format from output file name\n");
		}
	}

	if (cmdopts->outfmt < 0) {
		fprintf(stderr, "error: no output format specified\n");
		badusage();
	}

done:
	{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return cmdopts;}
}

void cmdopts_destroy(cmdopts_t *cmdopts)
{
	free(cmdopts);
}

int addopt(char *optstr, int maxlen, char *s)
{
	size_t n;
	size_t m;

	n = strlen(optstr);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	m = n + strlen(s) + 1;
	if (m > JAS_CAST(size_t, maxlen)) {
		{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return 1;}
	}
	if (n > 0) {
		strcat(optstr, "\n");
	}
	strcat(optstr, s);
	{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return 0;}
}

void cmdinfo()
{
	fprintf(stderr, "JasPer Transcoder (Version %s).\n",
	  JAS_VERSION);
	fprintf(stderr, "%s\n", JAS_COPYRIGHT);
	fprintf(stderr, "%s\n", JAS_NOTES);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
}

static char *helpinfo[] = {
"The following options are supported:\n",
"    --help                  Print this help information and exit.\n",
"    --version               Print version information and exit.\n",
"    --verbose               Enable verbose mode.\n",
"    --debug-level $lev      Set the debug level to $lev.\n",
"    --input $file           Read the input image from the file named $file\n",
"                            instead of standard input.\n",
"    --input-format $fmt     Specify the format of the input image as $fmt.\n",
"                            (See below for the list of supported formats.)\n",
"    --input-option $opt     Provide the option $opt to the decoder.\n",
"    --output $file          Write the output image to the file named $file\n",
"                            instead of standard output.\n",
"    --output-format $fmt    Specify the format of the output image as $fmt.\n",
"                            (See below for the list of supported formats.)\n",
"    --output-option $opt    Provide the option $opt to the encoder.\n",
"    --force-srgb            Force conversion to the sRGB color space.\n",
"Some of the above option names can be abbreviated as follows:\n",
"    --input = -f, --input-format = -t, --input-option = -o,\n",
"    --output = -F, --output-format = -T, --output-option = -O\n",
0
};

void cmdusage()
{
	int fmtid;
	jas_image_fmtinfo_t *fmtinfo;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	char *s;
	int i;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	cmdinfo();
	fprintf(stderr, "usage: %s [options]\n", cmdname);
	for (i = 0, s = helpinfo[i]; s; ++i, s = helpinfo[i]) {
		fprintf(stderr, "%s", s);
	}
	fprintf(stderr, "The following formats are supported:\n");
	for (fmtid = 0;; ++fmtid) {
		if (!(fmtinfo = jas_image_lookupfmtbyid(fmtid))) {
			break;
		}
		fprintf(stderr, "    %-5s    %s\n", fmtinfo->name,
		  fmtinfo->desc);
	}
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
}

void badusage()
{
	fprintf(stderr,
	  "For more information on how to use this command, type:\n");
	fprintf(stderr, "    %s --help\n", cmdname);
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EXIT_FAILURE);}
}

#if 0
jas_image_t *converttosrgb(jas_image_t *inimage)
{
	jas_image_t *outimage;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	jas_cmpixmap_t inpixmap;
	jas_cmpixmap_t outpixmap;
	jas_cmcmptfmt_t incmptfmts[16];
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	jas_cmcmptfmt_t outcmptfmts[16];

	outprof = jas_cmprof_createfromclrspc(JAS_CLRSPC_SRGB);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;assert(outprof);}
	xform = jas_cmxform_create(jas_image_cmprof(inimage), outprof, 0, JAS_CMXFORM_FWD, JAS_CMXFORM_INTENT_PER, 0);
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;assert(xform);}

	inpixmap.numcmpts = jas_image_numcmpts(oldimage);
	outpixmap.numcmpts = 3;
	for (i = 0; i < inpixmap.numcmpts; ++i) {
		inpixmap.cmptfmts[i] = &incmptfmts[i];
	}
	for (i = 0; i < outpixmap.numcmpts; ++i)
		outpixmap.cmptfmts[i] = &outcmptfmts[i];
	if (jas_cmxform_apply(xform, &inpixmap, &outpixmap))
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;abort();}

	jas_xform_destroy(xform);
	jas_cmprof_destroy(outprof);
	{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return 0;}
}
#endif
