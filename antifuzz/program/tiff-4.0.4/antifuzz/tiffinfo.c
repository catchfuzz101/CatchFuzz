/* $Id: tiffinfo.c,v 1.23 2015-06-21 01:09:11 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"

unsigned char *start_addr;
unsigned char *end_addr;
int antifuzz_check=0;
int canary_xor;
int fake_arr[47]={0,100,232,231,28,798,199,63,231,28,398,370,810,890,798,362,100,650,650,234,234,955,72,568,153,232,370,277,63,72,890,199,362,58,812,153,277,812,562,562,568,957,955,398,58,810,957};
#include <stdio.h>
#include <stdint.h>
#include <regex.h>
#include <string.h>


#include <stdlib.h>
#include <string.h>

#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffiop.h"

static TIFFErrorHandler old_error_handler = 0;
static int status = 0;                  /* exit status */
static int showdata = 0;		/* show data */
static int rawdata = 0;			/* show raw/decoded data */
static int showwords = 0;		/* show data as bytes/words */
static int readdata = 0;		/* read data in file */
static int stoponerr = 1;		/* stop on first read error */

static	void usage(void);
static	void tiffinfo(TIFF*, uint16, long, int);

static void
PrivateErrorHandler(const char* module, const char* fmt, va_list ap)
{
        if (old_error_handler)
                (*old_error_handler)(module,fmt,ap);
	status = 1;
}

int w0(unsigned char* buf, unsigned int len){
	printf("w0 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 0 % len;
    int index2 = 1 % len;
    int index3 = 2 % len;
    int index4 = 3 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3902515186) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1507252891) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 603338905) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1696904468) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1065828383) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3048701182) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1392146492) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3902515186) {
        ret = 12;
      }
    }
    if(buf[index1] > 56) {
      if(buf[index2] < 118) {
        if(buf[index3] - buf[index4] < 56) {
          if(buf[index3] + buf[index4] > 118) {
            if(((buf[index1] + buf[index2]) % 9) == 0) {
              if(((buf[index1] * buf[index2]) % 5) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 56) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }

  return ret;
}

int w1(unsigned char* buf, unsigned int len){
  printf("w1 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 1 % len;
    int index2 = 2 % len;
    int index3 = 3 % len;
    int index4 = 4 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3649224605) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2350045607) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3014389030) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2122141901) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1860222409) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2127411081) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3096905285) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3649224605) {
        ret = 12;
      }
    }
    if(buf[index1] > 209) {
      if(buf[index2] < 215) {
        if(buf[index3] - buf[index4] < 209) {
          if(buf[index3] + buf[index4] > 215) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 209) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w2(unsigned char* buf, unsigned int len){
	printf("w2 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 2 % len;
    int index2 = 3 % len;
    int index3 = 4 % len;
    int index4 = 5 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3060206730) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1823329745) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1798358539) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1130645893) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 334868504) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1095408358) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2855184194) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3060206730) {
        ret = 12;
      }
    }
    if(buf[index1] > 236) {
      if(buf[index2] < 125) {
        if(buf[index3] - buf[index4] < 236) {
          if(buf[index3] + buf[index4] > 125) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  64) {
                  if((buf[index1] ^ buf[index2]) == 236) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w3(unsigned char* buf, unsigned int len){
	  printf("w3 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 3 % len;
    int index2 = 4 % len;
    int index3 = 5 % len;
    int index4 = 6 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1306562184) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2309250971) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 702642080) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3193801888) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1618453275) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3908944833) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1426801106) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1306562184) {
        ret = 12;
      }
    }
    if(buf[index1] > 33) {
      if(buf[index2] < 13) {
        if(buf[index3] - buf[index4] < 33) {
          if(buf[index3] + buf[index4] > 13) {
            if(((buf[index1] + buf[index2]) % 6) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 33) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w4(unsigned char* buf, unsigned int len){
	  printf("w4 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 4 % len;
    int index2 = 5 % len;
    int index3 = 6 % len;
    int index4 = 7 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1317201472) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 4182791665) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1712271078) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3769200889) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3974150540) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1676806712) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 503006531) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1317201472) {
        ret = 12;
      }
    }
    if(buf[index1] > 225) {
      if(buf[index2] < 134) {
        if(buf[index3] - buf[index4] < 225) {
          if(buf[index3] + buf[index4] > 134) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 225) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w5(unsigned char* buf, unsigned int len){
	  printf("w5 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 5 % len;
    int index2 = 6 % len;
    int index3 = 7 % len;
    int index4 = 8 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 2680581394) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798508734) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1827993300) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 713768984) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4009010704) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1285272007) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4015010868) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 2680581394) {
        ret = 12;
      }
    }
    if(buf[index1] > 221) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 221) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 221) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w6(unsigned char* buf, unsigned int len){
	  printf("w6 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 6 % len;
    int index2 = 7 % len;
    int index3 = 8 % len;
    int index4 = 9 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1617115864) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1631690012) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3460463880) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3382779234) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1649578633) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 728322968) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2998716203) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1617115864) {
        ret = 12;
      }
    }
    if(buf[index1] > 132) {
      if(buf[index2] < 208) {
        if(buf[index3] - buf[index4] < 132) {
          if(buf[index3] + buf[index4] > 208) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  32) {
                  if((buf[index1] ^ buf[index2]) == 132) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w7(unsigned char* buf, unsigned int len){
	  printf("w7 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 7 % len;
    int index2 = 8 % len;
    int index3 = 9 % len;
    int index4 = 10 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1208351771) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2227323053) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1104939852) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4258576467) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 780873126) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2155013799) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1158125450) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1208351771) {
        ret = 12;
      }
    }
    if(buf[index1] > 18) {
      if(buf[index2] < 181) {
        if(buf[index3] - buf[index4] < 18) {
          if(buf[index3] + buf[index4] > 181) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  1) {
                  if((buf[index1] ^ buf[index2]) == 18) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w8(unsigned char* buf, unsigned int len){
	  printf("w8 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 8 % len;
    int index2 = 9 % len;
    int index3 = 10 % len;
    int index4 = 11 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 323927678) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798644434) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 2384399227) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 361926757) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 897612148) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 990255627) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3311835330) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 323927678) {
        ret = 12;
      }
    }
    if(buf[index1] > 100) {
      if(buf[index2] < 196) {
        if(buf[index3] - buf[index4] < 100) {
          if(buf[index3] + buf[index4] > 196) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 100) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w9(unsigned char* buf, unsigned int len){
	  printf("w9 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 9 % len;
    int index2 = 10 % len;
    int index3 = 11 % len;
    int index4 = 12 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 720174660) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2124116700) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3937385190) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1443256077) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1256522327) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1727523764) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2994088857) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 720174660) {
        ret = 12;
      }
    }
    if(buf[index1] > 249) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 249) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 8) == 0) {
                if((buf[index1] ^ buf[index2]) &  8) {
                  if((buf[index1] ^ buf[index2]) == 249) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}



typedef int (*obfuscation_call_heavyweight)(unsigned char* buf, unsigned int len);
obfuscation_call_heavyweight functions_array[10] = {
  
  w0,
  
  w1,
  
  w2,
  
  w3,
  
  w4,
  
  w5,
  
  w6,
  
  w7,
  
  w8,
  
  w9
};

/*https://github.com/francis-yli/kmeans_1d/blob/master/kmeans1D.c*/

float getDist(float p1, float p2) {
	float dist;
	dist = fabsf(p1 - p2); 
	return dist;
}

int getNearestDist(float array[], int p, int k) {
	float nearestDist = 300; 
	float dist; 
	int i;
	int label=-1;

	for (i = 0; i < k; i++) {
		dist = getDist(array[i], p);
		if (dist < nearestDist) {
			nearestDist = dist;
			label = i;
		}
	}
	return label;
}

float getMean(unsigned int vArray[], int lArray[], int label, int size) {
	float sum = 0.0;
	int counter = 0;
	float mean;
	int i = 0;

	for (; i < size; i++) {
		if (lArray[i] == label) {
			sum += vArray[i];
			counter++;
		}
	}

	if (counter != 0) {
		mean = sum / (float)counter;
	}
	else mean = -1;

	return mean;
}


void antifuzz(void){

	#define MAX_FILE_CONTENT_SIZE 512
	unsigned char* fileContentMult = (unsigned char*)"abcdefghijklmnopqrstuvwxyz";
old_error_handler=((((((((((((((((((((((((fake_arr[0]-fake_arr[1])+fake_arr[3])+fake_arr[5])*fake_arr[7])-fake_arr[9])+fake_arr[11])*fake_arr[13])/fake_arr[15])+fake_arr[17])/fake_arr[19])+fake_arr[21])*fake_arr[23])/fake_arr[25])*fake_arr[27])/fake_arr[29])-fake_arr[31])*fake_arr[33])*fake_arr[35])-fake_arr[37])-fake_arr[39])/fake_arr[41])/fake_arr[43])/fake_arr[45])*fake_arr[35]);
	unsigned int x[0xFFFFF]={0x00,};
	unsigned int y[0xFFFFF]={0x00,};

	char pid_mem_file[50]={0x00,};
	char pid_mem_file_name[50]={0x00,};
	
  	pid_t pid=getpid();
  	sprintf(pid_mem_file, "/proc/%d/maps", pid);
	sprintf(pid_mem_file_name, "./maps_%d", pid);
  	int canary_index, canary_val;
  	while(1){
  		int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
  		if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
        	else{
            		fake_arr[0]=1;
           	 	break;
        	}
        	canary_val=rand()%1000;
		if(fake_arr[canary_index]!=canary_val){
			fake_arr[canary_index]=canary_val;
			break;
		}
  	}
  	FILE *fp1  = fopen(pid_mem_file, "r");
	FILE *fp2  = fopen(pid_mem_file_name, "w");
  
	char buffer[128];
	regex_t state;
  	regmatch_t pmatch[3];

	char addr1[18];
  	char addr2[18];
  	const char *pattern = "[rwx-]{3}+[s]";
  	const char *addr = "([0-9a-fA-F]{12,16})-([0-9a-fA-F]{12,16})";
  	strncpy(addr1,"0x",2);
  	strncpy(addr2,"0x",2);

  	memset(addr1+2, 0x00, sizeof(addr1)-2);
 	memset(addr2+2, 0x00, sizeof(addr2)-2);

	regcomp(&state, pattern, REG_EXTENDED);
  	int index;
  	while (fgets(buffer, 128, fp1) != NULL)
  	{	
      		fprintf(fp2, "%s", buffer);
		int status = regexec(&state, buffer, 1, pmatch, 0);
		if(status==0){
      			regfree(&state);
      			regcomp(&state, addr, REG_EXTENDED);
      			status = regexec(&state, buffer, 3, pmatch, 0);
      			if(status==0){		
        			strncpy(addr1+2,buffer+pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
        			strncpy(addr2+2,buffer+pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
		
        			start_addr=(unsigned char*)strtoll(addr1,NULL,16);
        			end_addr=(unsigned char*)strtoll(addr2,NULL,16);
			
				for (int i = 0; i < end_addr-start_addr; i++) {
					x[i]=i;
					y[i] = start_addr[i];
				}
    
				for(int i = 0; i <130; i++) {
		        		functions_array[0](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i <110; i++) {
		        		functions_array[1](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 90; i++) {
		        		functions_array[2](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 70; i++) {
		        		functions_array[3](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}
		      		for(int i = 0; i < 50; i++) {
		        		functions_array[4](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      		}

		      		int k=0;
		      		for (int i = 0; i < end_addr-start_addr; i++){
					x[i]=i;
					int tmp_value=start_addr[i]-y[i];
					if(tmp_value>30 && tmp_value<150)y[k++]=tmp_value;
				}

		      		int labelArray[k];
		      		int orig_centerArray[5] = { 50,70,90,110,130 };
		      		float centerArray[5] = { 50,70,90,110,130 };
		      		float tmp_centerArray[5] = { 0, };
      
		     		int loop = 0;
				while(1) {
		        		loop++;
		        		for (int i = 0; i < k; i++) {
		          			int label = getNearestDist(centerArray, y[i], 5);
		          			labelArray[i] = label;
		       			}

		       			int finish = 0;

		       			for (int i = 0; i < 5; i++) {
		          			tmp_centerArray[i] = centerArray[i];
		          			centerArray[i] = getMean(y, labelArray, i, k);
		          			if (tmp_centerArray[i] == centerArray[i]) {
		            				finish += 1;
		          			}
		          
        				}
		       	 		if (finish == 5) {
		          			break;
		        		}
      				}

		      		float distance = 0;
		     	 	int tmp_index=5;
		      		for (int i = 0; i < tmp_index; i++) {
		        		if (centerArray[i] == -1)tmp_index -= 1;
		        		else distance+=(centerArray[i] - orig_centerArray[i])*(centerArray[i]-orig_centerArray[i]);
		      		}
		      		distance /=tmp_index;

				if(distance<10){
					antifuzz_check=1;
					for (int i = 0; i < end_addr-start_addr; i++) {
						start_addr[i]=0x80;
					}
				}
				break;
     			}
   		}
 	}
	regfree(&state);
	fclose(fp1);
	fclose(fp2);

       	unlink(pid_mem_file_name);

}

int
main(int argc, char* argv[])
{
	antifuzz();
	int dirnum = -1, multiplefiles, c;
	uint16 order = 0;
	TIFF* tif;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
#if !HAVE_DECL_OPTARG
	extern int optind;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	extern char* optarg;
#endif
	long flags = 0;
	uint64 diroff = 0;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	int chopstrips = 0;		/* disable strip chopping */

	while ((c = getopt(argc, argv, "f:o:cdDSjilmrsvwz0123456789")) != -1)
		switch (c) {
		case '0': case '1': case '2': case '3':
		case '4': case '5': case '6': case '7':
		case '8': case '9':
			dirnum = atoi(&argv[optind-1][1]);
			break;
		case 'd':
			showdata++;
			/* fall thru... */
		case 'D':
			readdata++;
			break;
		case 'c':
			flags |= TIFFPRINT_COLORMAP | TIFFPRINT_CURVES;
			break;
		case 'f':		/* fill order */
			if (streq(optarg, "lsb2msb"))
				order = FILLORDER_LSB2MSB;
			else if (streq(optarg, "msb2lsb"))
				order = FILLORDER_MSB2LSB;
			else
				usage();
			break;
		case 'i':
			stoponerr = 0;
			break;
		case 'o':
			diroff = strtoul(optarg, NULL, 0);
			break;
		case 'j':
			flags |= TIFFPRINT_JPEGQTABLES |
				 TIFFPRINT_JPEGACTABLES |
				 TIFFPRINT_JPEGDCTABLES;
			break;
		case 'r':
			rawdata = 1;
			break;
		case 's':
			flags |= TIFFPRINT_STRIPS;
			break;
		case 'w':
			showwords = 1;
			break;
		case 'z':
			chopstrips = 1;
			break;
		case '?':
			usage();
			/*NOTREACHED*/
		}
	if (optind >= argc)
		usage();

	old_error_handler = TIFFSetErrorHandler(PrivateErrorHandler);

	multiplefiles = (argc - optind > 1);
	for (; optind < argc; optind++) {
		if (multiplefiles)
			printf("%s:\n", argv[optind]);
		tif = TIFFOpen(argv[optind], chopstrips ? "rC" : "rc");
		if (tif != NULL) {
			if (dirnum != -1) {
				if (TIFFSetDirectory(tif, (tdir_t) dirnum))
					tiffinfo(tif, order, flags, 1);
			} else if (diroff != 0) {
				if (TIFFSetSubDirectory(tif, diroff))
					tiffinfo(tif, order, flags, 1);
			} else {
				do {
					toff_t offset=0;

					tiffinfo(tif, order, flags, 1);
					if (TIFFGetField(tif, TIFFTAG_EXIFIFD,
							 &offset)) {
						if (TIFFReadEXIFDirectory(tif, offset)) {
							tiffinfo(tif, order, flags, 0);
						}
					}
				} while (TIFFReadDirectory(tif));
			}
			TIFFClose(tif);
		}
	}
	{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return (status);}
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
}

char* stuff[] = {
"usage: tiffinfo [options] input...",
"where options are:",
" -D		read data",
" -i		ignore read errors",
" -c		display data for grey/color response curve or colormap",
" -d		display raw/decoded image data",
" -f lsb2msb	force lsb-to-msb FillOrder for input",
" -f msb2lsb	force msb-to-lsb FillOrder for input",
" -j		show JPEG tables",
" -o offset	set initial directory offset",
" -r		read/display raw image data instead of decoded data",
" -s		display strip offsets and byte counts",
" -w		display raw data in words rather than bytes",
" -z		enable strip chopping",
" -#		set initial directory (first directory is # 0)",
NULL
};

static void
usage(void)
{
	char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	for (i = 0; stuff[i] != NULL; i++)
		fprintf(stderr, "%s\n", stuff[i]);
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(-1);}
}

static void
ShowStrip(tstrip_t strip, unsigned char* pp, uint32 nrow, tsize_t scanline)
{
	register tsize_t cc;

	printf("Strip %lu:\n", (unsigned long) strip);
	while (nrow-- > 0) {
		for (cc = 0; cc < scanline; cc++) {
			printf(" %02x", *pp++);
			if (((cc+1) % 24) == 0)
				putchar('\n');
		}
		putchar('\n');
	}
}

void
TIFFReadContigStripData(TIFF* tif)
{
	unsigned char *buf;
	tsize_t scanline = TIFFScanlineSize(tif);

	buf = (unsigned char *)_TIFFmalloc(TIFFStripSize(tif));
	if (buf) {
		uint32 row, h=0;
		uint32 rowsperstrip = (uint32)-1;

		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
		for (row = 0; row < h; row += rowsperstrip) {
			uint32 nrow = (row+rowsperstrip > h ?
			    h-row : rowsperstrip);
			tstrip_t strip = TIFFComputeStrip(tif, row, 0);
			if (TIFFReadEncodedStrip(tif, strip, buf, nrow*scanline) < 0) {
				if (stoponerr)
					break;
			} else if (showdata)
				ShowStrip(strip, buf, nrow, scanline);
		}
		_TIFFfree(buf);
	}
}

void
TIFFReadSeparateStripData(TIFF* tif)
{
	unsigned char *buf;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	tsize_t scanline = TIFFScanlineSize(tif);

	buf = (unsigned char *)_TIFFmalloc(TIFFStripSize(tif));
	if (buf) {
		uint32 row, h=0;
		uint32 rowsperstrip = (uint32)-1;
		tsample_t s, samplesperpixel=0;

		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
		TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
		for (row = 0; row < h; row += rowsperstrip) {
			for (s = 0; s < samplesperpixel; s++) {
				uint32 nrow = (row+rowsperstrip > h ?
				    h-row : rowsperstrip);
				tstrip_t strip = TIFFComputeStrip(tif, row, s);
				if (TIFFReadEncodedStrip(tif, strip, buf, nrow*scanline) < 0) {
					if (stoponerr)
						break;
				} else if (showdata)
					ShowStrip(strip, buf, nrow, scanline);
			}
		}
		_TIFFfree(buf);
	}
}

static void
ShowTile(uint32 row, uint32 col, tsample_t sample,
    unsigned char* pp, uint32 nrow, tsize_t rowsize)
{
	uint32 cc;

	printf("Tile (%lu,%lu", (unsigned long) row, (unsigned long) col);
	if (sample != (tsample_t) -1)
		printf(",%u", sample);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	printf("):\n");
	while (nrow-- > 0) {
	  for (cc = 0; cc < (uint32) rowsize; cc++) {
			printf(" %02x", *pp++);
			if (((cc+1) % 24) == 0)
				putchar('\n');
		}
		putchar('\n');
	}
}

void
TIFFReadContigTileData(TIFF* tif)
{
	unsigned char *buf;
	tsize_t rowsize = TIFFTileRowSize(tif);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

	buf = (unsigned char *)_TIFFmalloc(TIFFTileSize(tif));
	if (buf) {
		uint32 tw=0, th=0, w=0, h=0;
		uint32 row, col;

		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tw);
		TIFFGetField(tif, TIFFTAG_TILELENGTH, &th);
		for (row = 0; row < h; row += th) {
			for (col = 0; col < w; col += tw) {
				if (TIFFReadTile(tif, buf, col, row, 0, 0) < 0) {
					if (stoponerr)
						break;
				} else if (showdata)
					ShowTile(row, col, (tsample_t) -1, buf, th, rowsize);
			}
		}
		_TIFFfree(buf);
	}
}

void
TIFFReadSeparateTileData(TIFF* tif)
{
	unsigned char *buf;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
	tsize_t rowsize = TIFFTileRowSize(tif);

	buf = (unsigned char *)_TIFFmalloc(TIFFTileSize(tif));
	if (buf) {
		uint32 tw=0, th=0, w=0, h=0;
		uint32 row, col;
		tsample_t s, samplesperpixel=0;

		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tw);
		TIFFGetField(tif, TIFFTAG_TILELENGTH, &th);
		TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
		for (row = 0; row < h; row += th) {
			for (col = 0; col < w; col += tw) {
				for (s = 0; s < samplesperpixel; s++) {
					if (TIFFReadTile(tif, buf, col, row, 0, s) < 0) {
						if (stoponerr)
							break;
					} else if (showdata)
						ShowTile(row, col, s, buf, th, rowsize);
				}
			}
		}
		_TIFFfree(buf);
	}
}

void
TIFFReadData(TIFF* tif)
{
	uint16 config = PLANARCONFIG_CONTIG;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

	TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &config);
	if (TIFFIsTiled(tif)) {
		if (config == PLANARCONFIG_CONTIG)
			TIFFReadContigTileData(tif);
		else
			TIFFReadSeparateTileData(tif);
	} else {
		if (config == PLANARCONFIG_CONTIG)
			TIFFReadContigStripData(tif);
		else
			TIFFReadSeparateStripData(tif);
	}
}

static void
ShowRawBytes(unsigned char* pp, uint32 n)
{
	uint32 i;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

	for (i = 0; i < n; i++) {
		printf(" %02x", *pp++);
		if (((i+1) % 24) == 0)
			printf("\n ");
	}
	putchar('\n');
}

static void
ShowRawWords(uint16* pp, uint32 n)
{
	uint32 i;

	for (i = 0; i < n; i++) {
		printf(" %04x", *pp++);
		if (((i+1) % 15) == 0)
			printf("\n ");
	}
	putchar('\n');
}

void
TIFFReadRawData(TIFF* tif, int bitrev)
{
	tstrip_t nstrips = TIFFNumberOfStrips(tif);
	const char* what = TIFFIsTiled(tif) ? "Tile" : "Strip";
	uint64* stripbc=NULL;

	TIFFGetField(tif, TIFFTAG_STRIPBYTECOUNTS, &stripbc);
	if (nstrips > 0) {
		uint32 bufsize = (uint32) stripbc[0];
		tdata_t buf = _TIFFmalloc(bufsize);
		tstrip_t s;

		for (s = 0; s < nstrips; s++) {
			if (stripbc[s] > bufsize) {
				buf = _TIFFrealloc(buf, (tmsize_t)stripbc[s]);
				bufsize = (uint32) stripbc[s];
			}
			if (buf == NULL) {
				fprintf(stderr,
				   "Cannot allocate buffer to read strip %lu\n",
				    (unsigned long) s);
				break;
			}
			if (TIFFReadRawStrip(tif, s, buf, (tmsize_t) stripbc[s]) < 0) {
				fprintf(stderr, "Error reading strip %lu\n",
				    (unsigned long) s);
				if (stoponerr)
					break;
			} else if (showdata) {
				if (bitrev) {
					TIFFReverseBits(buf, (tmsize_t)stripbc[s]);
					printf("%s %lu: (bit reversed)\n ",
					    what, (unsigned long) s);
				} else
					printf("%s %lu:\n ", what,
					    (unsigned long) s);
				if (showwords)
					ShowRawWords((uint16*) buf, (uint32) stripbc[s]>>1);
				else
					ShowRawBytes((unsigned char*) buf, (uint32) stripbc[s]);
			}
		}
		if (buf != NULL)
			_TIFFfree(buf);
	}
}

static void
tiffinfo(TIFF* tif, uint16 order, long flags, int is_image)
{
	TIFFPrintDirectory(tif, stdout, flags);
	if (!readdata || !is_image)
		{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return;}
	if (rawdata) {
		if (order) {
			uint16 o;
			TIFFGetFieldDefaulted(tif,
			    TIFFTAG_FILLORDER, &o);
			TIFFReadRawData(tif, o != order);
		} else
			TIFFReadRawData(tif, 0);
	} else {
		if (order)
			TIFFSetField(tif, TIFFTAG_FILLORDER, order);
		TIFFReadData(tif);
	}
}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
