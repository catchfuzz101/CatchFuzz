/*https://github.com/francis-yli/kmeans_1d/blob/master/kmeans1D.c*/

float getDist(float p1, float p2) {
	float dist;
	dist = fabsf(p1 - p2); 
	return dist;
}

int getNearestDist(float array[], int p, int k) {
	float nearestDist = 300; 
	float dist; 
	int i;
	int label=-1;

	for (i = 0; i < k; i++) {
		dist = getDist(array[i], p);
		if (dist < nearestDist) {
			nearestDist = dist;
			label = i;
		}
	}
	return label;
}

float getMean(unsigned int vArray[], int lArray[], int label, int size) {
	float sum = 0.0;
	int counter = 0;
	float mean;
	int i = 0;

	for (; i < size; i++) {
		if (lArray[i] == label) {
			sum += vArray[i];
			counter++;
		}
	}

	if (counter != 0) {
		mean = sum / (float)counter;
	}
	else mean = -1;

	return mean;
}

