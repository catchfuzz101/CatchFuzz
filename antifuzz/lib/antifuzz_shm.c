void antifuzz(void){

	#define MAX_FILE_CONTENT_SIZE 512
	unsigned char* fileContentMult = (unsigned char*)"abcdefghijklmnopqrstuvwxyz";
	unsigned int x[0xFFFFF]={0x00,};
	unsigned int y[0xFFFFF]={0x00,};

	char pid_mem_file[50]={0x00,};
	char pid_mem_file_name[50]={0x00,};
	
  	pid_t pid=getpid();
  	sprintf(pid_mem_file, "/proc/%d/maps", pid);
	sprintf(pid_mem_file_name, "./maps_%d", pid);
  	int canary_index, canary_val;
  	while(1){
  		int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
  		if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
        	else{
            		fake_arr[0]=1;
           	 	break;
        	}
        	canary_val=rand()%1000;
		if(fake_arr[canary_index]!=canary_val){
			fake_arr[canary_index]=canary_val;
			break;
		}
  	}
  	FILE *fp1  = fopen(pid_mem_file, "r");
	FILE *fp2  = fopen(pid_mem_file_name, "w");
  
	char buffer[128];
	regex_t state;
  	regmatch_t pmatch[3];

	char addr1[18];
  	char addr2[18];
  	const char *pattern = "[rwx-]{3}+[s]";
  	const char *addr = "([0-9a-fA-F]{12,16})-([0-9a-fA-F]{12,16})";
  	strncpy(addr1,"0x",2);
  	strncpy(addr2,"0x",2);

  	memset(addr1+2, 0x00, sizeof(addr1)-2);
 	memset(addr2+2, 0x00, sizeof(addr2)-2);

	regcomp(&state, pattern, REG_EXTENDED);
  	
  	int index;

  	unsigned char* start_addr_list[100];
  	unsigned char* end_addr_list[100];
  	int shm_cnt=0;

  	while (fgets(buffer, 128, fp1) != NULL)
  	{	
      	fprintf(fp2, "%s", buffer);
		int status = regexec(&state, buffer, 1, pmatch, 0);
		if(status==0){
      			regfree(&state);
      			regcomp(&state, addr, REG_EXTENDED);
      			status = regexec(&state, buffer, 3, pmatch, 0);
      			if(status==0){		
        			strncpy(addr1+2,buffer+pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
        			strncpy(addr2+2,buffer+pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
		
        			start_addr=(unsigned char*)strtoll(addr1,NULL,16);
        			end_addr=(unsigned char*)strtoll(addr2,NULL,16);
        			start_addr_list[shm_cnt]=start_addr;
        			end_addr_list[shm_cnt]=end_addr;
        			shm_cnt+=1;
				}
		}
	}
	if(shm_cnt){
		int offset=0;
		for(int shm=0;shm<shm_cnt;shm++){
			start_addr=start_addr[shm];
			end_addr=end_addr[shm];
			for (int i = 0; i < end_addr-start_addr; i++) {
				x[i+offset]=i;
				y[i+offset]=start_addr[i];
			}
			offset+=end_addr-start_addr;
		}

		for(int i = 0; i <130; i++) {
	    	functions_array[0](fileContentMult, MAX_FILE_CONTENT_SIZE);
	  	}
	  	for(int i = 0; i <110; i++) {
	    	functions_array[1](fileContentMult, MAX_FILE_CONTENT_SIZE);
	  	}
	  	for(int i = 0; i < 90; i++) {
	    	functions_array[2](fileContentMult, MAX_FILE_CONTENT_SIZE);
	  	}
	  	for(int i = 0; i < 70; i++) {
	    	functions_array[3](fileContentMult, MAX_FILE_CONTENT_SIZE);
	  	}
	  	for(int i = 0; i < 50; i++) {
	    	functions_array[4](fileContentMult, MAX_FILE_CONTENT_SIZE);
	  	}


	  	offset=0;
	  	int k=0;
		for(int shm=0;shm<shm_cnt;shm++){
			start_addr=start_addr[shm];
			end_addr=end_addr[shm];
			for (int i = 0; i < end_addr-start_addr; i++){
				x[i+offset]=i;
				int tmp_value=start_addr[i]-y[i+offset];
				if(tmp_value>30 && tmp_value<150)y[k++]=tmp_value;
			}
			offset+=end_addr-start_addr;
		}

		int labelArray[k];
		int orig_centerArray[5] = { 50,70,90,110,130 };
		float centerArray[5] = { 50,70,90,110,130 };
		float tmp_centerArray[5] = { 0, };
		int loop = 0;
		while(1) {
	    	loop++;
	    	for (int i = 0; i < k; i++) {
	      		int label = getNearestDist(centerArray, y[i], 5);
	      		labelArray[i] = label;
	   		}

	   		int finish = 0;

	   		for (int i = 0; i < 5; i++) {
	      		tmp_centerArray[i] = centerArray[i];
	      		centerArray[i] = getMean(y, labelArray, i, k);
	      		if (tmp_centerArray[i] == centerArray[i]) {
	        		finish += 1;
	      		}
			}
	   	 	if (finish == 5) {
	      		break;
	    	}
		}

	  	float distance = 0;
	 	int tmp_index=5;
	  	for (int i = 0; i < tmp_index; i++) {
	    	if (centerArray[i] == -1)tmp_index -= 1;
	    	else distance+=(centerArray[i] - orig_centerArray[i])*(centerArray[i]-orig_centerArray[i]);
	  	}
	  	distance /=tmp_index;

		if(distance<10){
			antifuzz_check=1;
			for (int i = 0; i < end_addr-start_addr; i++) {
				start_addr[i]=0x80;
			}
		}
	}
	
regfree(&state);
fclose(fp1);
fclose(fp2);
unlink(pid_mem_file_name);

}
