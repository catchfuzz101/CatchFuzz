if [ $# -ne 1 ]
  then
    echo "[*] Usage: ./install.sh bison"
    exit 1
fi

echo "[+] Copy all source code files used for compilation to the \"./src\" folder"
mkdir ./program/$1 | true
mkdir ./program/$1/antifuzz | true
mkdir ./program/$1/src | true
