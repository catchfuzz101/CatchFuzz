import sys
import os
import commands
import re
import random

def add_slash(string):
	new_string=""
	for i in string:
		if i in ["*", "[","]","\"","\""]:
			new_string+="\\"
		new_string+=i		
	return new_string

if(len(sys.argv)!=2):
    print("[+] Usage: python stage3.py bison")		
    exit()

filelist=os.listdir("./program/"+sys.argv[1]+"/src/")

found_main=False
for i in filelist:
	os.system("ctags ./program/"+sys.argv[1]+"/src/"+i)
	f=open("./tags",'r')
	while(1):
		tag_data=f.readline()
		if tag_data=="":
			f.close()
			break
		elif tag_data.startswith("main\t"):
			main_data=tag_data.split("\t")[2]
			if main_data.startswith("/^") and main_data.endswith("$/;\""):
				main_data=add_slash(main_data[1:].split("$/;\"")[0])
				found_main=True
				filelist.remove(i)
				f.close()
				break
if(found_main==False):
	print("ERROR! no main!\n")
	exit(-1)

print("[+] Result File:")
for i in filelist:
	file_name="./program/"+sys.argv[1]+"/src/"+i
	command="grep -n \"#include\" "+file_name
	status, output=commands.getstatusoutput(command)
	header=[]
	for j in output.split("\n")[1:]:
		header.append(int(j.split(":")[0]))


	f2=open(file_name,'r')
	result_file="./program/"+sys.argv[1]+"/antifuzz/"+i
	f3=open(result_file,'w')

	index=0
	stack=[]
	header_check=False
	total_data=""
	while(1):
		index+=1
		line=f2.readline()
		if(header_check==False and "#if" in line):
			stack.append(index)
		if(header_check==False and "#endif" in line):
			stack.pop()

		if(line==""):
			f2.close()
			f3.write(total_data)	
			f3.close()
			break


		if(header_check==False and len(stack)==0 and index in header):
               		line=line+"\nextern unsigned char *start_addr;\nextern unsigned char *end_addr;\nextern int antifuzz_check;\n"
			header_check=True
	
		total_data+=line		
	print(result_file)
os.unlink("./tags")
