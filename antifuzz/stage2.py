import sys
import os
import commands
import re
import random
fake_arr=[0]
fake_arr_name="fake_arr"
pwd="./lib"

def div_c(a,b):
    if(a>=0)!=(b>=0) and a%b:
        return a//b+1
    else:
        return a//b

def add_slash(string):
    new_string=""
    for i in string:
        if i in ["*", "[","]","\"","\""]:
            new_string+="\\"
        new_string+=i
    return new_string


def equation_maker(num):
	global fake_arr
	prev_len=len(fake_arr)
	n=random.randint(10,50)
	while(1):
		x=[]
		y=[]
		equation=fake_arr_name+"[0]"
		for i in range(n):
			x.append(random.randint(0,3))
			y.append(random.randint(1,1000))
		result=0
		for i in range(n):
			if y[i] not in fake_arr:
				fake_arr.append(y[i])
				fake_arr.append(y[i])
			if(x[i]==0):
				result+=y[i]
				equation+="+"+fake_arr_name+"["+str(fake_arr.index(y[i]))+"])"
			elif(x[i]==1):
				result-=y[i]
				equation+="-"+fake_arr_name+"["+str(fake_arr.index(y[i]))+"])"
			elif(x[i]==2):
				result*=y[i]
				equation+="*"+fake_arr_name+"["+str(fake_arr.index(y[i]))+"])"
			elif(x[i]==3):
				result=div_c(result,y[i])
				equation+="/"+fake_arr_name+"["+str(fake_arr.index(y[i]))+"])"
			
		if(result==num):
			equation="("*equation.count(")")+equation
			return equation,fake_arr
			break
		else:
			fake_arr=fake_arr[:prev_len]

if(len(sys.argv)!=2):
    print("[*] Usage: python stage2.py ./program/bison/src/bison.c")		
    #print("[*] Insert antifuzz code to main code")
    exit(1)

os.system("ctags "+sys.argv[1])
f=open("./tags",'r')
data=[]


while(1):
	line=f.readline()
	if(line==""):
		f.close()
		break
	elif "lava" not in line: #for LAVA-M dataset
		if(line.split("\t")[1]==sys.argv[1]):
			data.append(line.split("\t"))
variable={}
for i in data:
	if i[3]=='v':
		pattern=""
		if i[2].startswith("/^") and i[2].endswith("$/;\""):
			pattern=i[2][1:].split("$/;\"")[0]
		variable[i[0]]=add_slash(pattern)
line_output={}
for i in variable.keys():
	command="grep -n \""+variable[i]+"\" "+sys.argv[1]
	status, output=commands.getstatusoutput(command)
	line_output[i]=output
replace_data={}
p=re.compile("^\s?-?\d+\s?$")
for i in line_output.keys():
	if "=" in line_output[i] and line_output[i].endswith(";"):
		replace_num=line_output[i].split("=")[1].split(";")[0].strip()
		if(p.search(replace_num)):
			equation, addr=equation_maker(int(replace_num))
			replace_data[int(line_output[i].split(":")[0])]=(line_output[i].split(":")[1],equation,i)


fake_arr_tmp=[i for i in fake_arr[1::2]]
random.shuffle(fake_arr_tmp)

for i in range(len(fake_arr)/2):
	fake_arr[(i+1)*2]=fake_arr_tmp[i]


tmp_file=sys.argv[1].replace(sys.argv[1].split('/')[-1],"")+"../antifuzz/tmp_"+sys.argv[1].split('/')[-1]

f1=open(sys.argv[1],'r')
f2=open(tmp_file,'w')
index=0
stack=[]
antifuzz_variable=""
while(1):
	index+=1
	line=f1.readline()
	if(line==""):
		f1.close()
		f2.close()
		break
	elif("{" in line and "}" in line):
		if(line.count("{")>line.count("}")):
			for i in range(line.count("{")-line.count("}")):
				stack.append(index)
		else:
			for i in range(line.count("}")-line.count("{")):
				stack.pop()
	elif("{" in line):
		for i in range(line.count("{")):
			stack.append(index)
	elif("}" in line):
		for i in range(line.count("}")):
			stack.pop()
	if(index in replace_data.keys()):
		head=line.split("=")[0]
		if(len(stack)>0):
			if("static" in head):
				f2.write(line+replace_data[index][2]+"="+replace_data[index][1]+";\n")
			else:
				f2.write(head+"="+replace_data[index][1]+";\n")
		else:
			if("static" in head):
				f2.write(line)
				antifuzz_variable+=replace_data[index][2]+"="+replace_data[index][1]+";\n"
			else:	
				f2.write(head+"="+replace_data[index][1]+";\n")
				
	else:
		f2.write(line)	

f=open("./tags",'r')
while(1):
	tag_data=f.readline()
	if tag_data=="":
		f.close()
		break
	elif tag_data.startswith("main\t"):
		main_data=tag_data.split("\t")[2]
		if main_data.startswith("/^") and main_data.endswith("$/;\""):
			main_data=add_slash(main_data[1:].split("$/;\"")[0])
			f.close()
			break

command="grep -n \""+main_data+"\" "+tmp_file
status, output=commands.getstatusoutput(command)
main_index=int(output.split(":")[0])
main_after=main_index+1
if(main_data[1:].startswith("main")):
	main_index=main_index-1
if("{" not in main_data):
	main_after=main_after+1


command="grep -n \"#include\" "+tmp_file
status, output=commands.getstatusoutput(command)
header_index=[]
for i in output.split("\n")[1:]:
	header_index.append(int(i.split(":")[0]))

command="grep -n \";$\" "+tmp_file
status, output=commands.getstatusoutput(command)
canary_check_point=[]
for i in output.split("\n")[:-1]:
	canary_check_point.append(int(i.split(":")[0]))
random.shuffle(canary_check_point)

f2=open(tmp_file,'r')
result_file=sys.argv[1].replace(sys.argv[1].split('/')[-1],"")+"../antifuzz/"+sys.argv[1].split('/')[-1]
f3=open(result_file,'w')

before_header=""
after_header=""
index=0
fake_arr_data=""
stack=[]
stack2=[]
canary_check=False
header_check=False
canary_xor="\tcanary_xor=0;\n\tfor(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];\n\tif(canary_xor==0)exit(-1);\n"
while(1):
	index+=1
	line=f2.readline()
	if(line==""):
		for i in fake_arr:
			fake_arr_data+=str(i)+","
		f2.close()
		block=after_header.split(canary_xor)
		canary_block=[]
		for i in range(10):
			canary_block.append(random.randint(0,len(block)-2))
		after_header_add_canary=""
		for i in range(len(block)):
			if(i in canary_block):
				after_header_add_canary+=(block[i]+canary_xor)
			else:
				after_header_add_canary+=block[i]
		
		f3.write(before_header+"unsigned char *start_addr;\nunsigned char *end_addr;\nint antifuzz_check=0;\nint canary_xor;\nint "+fake_arr_name+"["+str(len(fake_arr))+"]={"+fake_arr_data[:-1]+"};\n"+after_header_add_canary)
		f3.close()
		break
	if(header_check==False and "#if" in line):
		stack2.append(index)
	elif(header_check==False and "#endif" in line):
		stack2.pop()
	
	if(index==main_index):
		f=open(pwd+"/antifuzz.c", 'r')
		data=f.readlines()
		data_tmp=""
		for i in data[4:]:
			data_tmp+=i
		data=data[0]+data[1]+data[2]+data[3]+antifuzz_variable+data_tmp+"\n"
		f.close()

		f=open(pwd+"/kmeans.c",'r')
		data_tmp=""
		for i in f.readlines():
			data_tmp+=i
		data=data_tmp+"\n"+data
		f.close()

		f=open(pwd+"/fakecode.c",'r')
		data_tmp=""
		for i in f.readlines():
			data_tmp+=i
		data=data_tmp+"\n"+data
		f.close()

		line=data+line
	if(index==main_after):
		line="\tantifuzz();\n"+line
		stack.append(index)
	if(index>main_after):
		if("{" in line and "}" in line):
			if(line.count("{")>line.count("}")):
				for i in range(line.count("{")-line.count("}")):
					stack.append(index)
			else:
				for i in range(line.count("}")-line.count("{")):
					stack.pop()
		elif("{" in line):
			for i in range(line.count("{")):
				stack.append(index)
		elif("}" in line):
			for i in range(line.count("}")):
				stack.pop()
		if "return" in line:
			line=line.split("return")[0]+"{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;"+line.replace(line.split("return")[0],"").strip()+"}\n"

				
	if(index in canary_check_point and len(stack)==1):
		line=line+canary_xor;

	if(header_check==False and len(stack2)==0 and index in header_index):
		header_check=True
		f=open(pwd+"/header.c",'r')
                data_tmp=""
                for i in f.readlines():
                        data_tmp+=i
                line=line+data_tmp+"\n"
                f.close()

	if(header_check==False):
		before_header+=line
	else:
		after_header+=line
		
print("[+] Result File: "+result_file)
os.unlink(tmp_file)
os.unlink("./tags")
