# CatchFuzz

<img src="https://gitlab.com/catchfuzz101/CatchFuzz/-/raw/master/figure/logo1.png" width=400>

CatchFuzz prevents attackers from performing fuzzing. CatchFuzz overcomes current limitations and dramatically increases both the efficiency and effectiveness of anti-fuzzing technology. CatchFuzz can accurately identify the fuzzing environment with a protector algorithm that can detect anti-fuzzing code removal, and then performs effective anti-fuzzing. 

## Environment
Tested on both Ubuntu 22.04 64bit, Ubuntu 16.04 64bit with LLVM 6.0.


## Requirements
```
# You need ctags program. Install with one of the following two commands, depending on your OS version.
$ sudo apt-get install universal-ctags
$ sudo apt-get install ctags
```    

## Usage

```
$ git clone https://gitlab.com/catchfuzz101/CatchFuzz.git
$ cd CatchFuzz
```

Let's assume we want to protect `bison-3.3` program with CatchFuzz!

1. Make `src` folder and `antifuzz` folder under
	[*CatchFuzz/antifuzz/program/bison-3.3*]
	```
	$ cd CatchFuzz/antifuzz
	$ ./install.sh bison-3.3
	```
2. Copy all source files used for compilation to the `src` folder
3. CatchFuzz proceeds from **stage1** to **stage4** as follows
	*  stage1
	```
	$ python stage1.py bison-3.3
	 
	--> [+] Found main file: ./program/bison-3.3/src/main.c
	```
	* stage2

	Stage2 needs the result of Stage1(the path of the main file) as the second argument.

	```
	$ python stage2.py ./program/bison-3.3/src/main.c
			
	--> [+] Result File: ./program/bison-3.3/src/../antifuzz/main.c
	```
	* stage3
	```
	$ python stage3.py bison-3.3
		
	--> [+] Result File:
	./program/bison-3.3/antifuzz/scan-code.c
	./program/bison-3.3/antifuzz/reduce.c
	...
	./program/bison-3.3/antifuzz/state.c
	./program/bison-3.3/antifuzz/fixits.c
	```
	* stage 4

	*Coverage-Faker* is inserted into the source code at the end of execution. However, if the end instruction consists of multiple lines, parsing may be work wrongly, so manual editing is required. CatchFuzz tells you where manual editing is required.
	```
	$ python stage4.py bison-3.3
		
	--> [*] Result: Succeed in patching 27 points
	***************************************************
	Needs Manual Editing!
	***************************************************
	----------------------------------------------------------
	[\*] File: ./program/bison-3.3/antifuzz/files.c
	[-] Before:     error (EXIT_FAILURE, get_errno (),
	[+] After: {if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error (EXIT_FAILURE, get_errno (),}
	----------------------------------------------------------
	```

4. Replace the original source code files(`[CatchFuzz/target_program/bison-3.3/src]`)
to CatchFuzz-applied source files(`[CatchFuzz/antifuzz/program/bison-3.3/antifuzz/*]`) .

	* CatchFuzz provides `make.sh` to automate this replacing 
	 * You should give the relative path where the original source files are located as the second argument
	```
	$ cd CatchFuzz/target_program
	$ ./make.sh bison-3.3 src
	```
	* Since several programs are packaged in the *binutils* program, we prepared a separate script `make_bison.sh` for the *binutils* program.
	```
	$ ./make_binutils.sh readelf
	```

5. **Compile** and **Fuzz** as usual!
	```
	# Compile
	$ cd CatchFuzz/target_program/bison-3.3
	$ CC=[path_for_afl]/afl-clang-fast CXX=[path_for_afl]/afl-clang-fast++ ./configure
	$ make -j 4
	
	# Fuzz
	$ cd CatchFuzz/target_program
	$ afl-fuzz -i ../seed/bison-3.3/ -o ./out ./bison-3.3/src/bison --defines=/dev/null --xml=/dev/null --graph=/dev/null -o /dev/null @@
	```
