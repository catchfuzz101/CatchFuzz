
- bison
> CC=[path_for_afl]/afl-clang-fast CXX=[path_for_afl]/afl-clang-fast++ ./configure
> make -j 4

- cflow
> CC=[path_for_afl]/afl-clang-fast CXX=[path_for_afl]/afl-clang-fast++ ./configure
> make -j 4
- nasm
> make clean
> CC=[path_for_afl]/afl-clang-fast CXX=[path_for_afl]/afl-clang-fast++ ./configure
> sed -i 's/-Werror=implicit//g' ./Makefile
> sed -i 's/-Werror=strict-prototypes//g' ./Makefile
> sed -i 's/-Werror=missing-prototypes//g' ./Makefile
> sed -i 's/-Werror=vla//g' ./Makefile
> make -j 4

- jasper
>cd ./build
>cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_COMPILER=[path_for_afl]/afl-clang-fast -DCMAKE_CXX_COMPILER=[path_for_afl]/afl-clang-fast++
    make clean
    make -j 4

- dact
> make clean
> CC=[path_for_afl]/afl-clang-fast CXX=[path_for_afl]/afl-clang-fast++ ./configure 
> make 

- readelf
> cd ./binutils
> 
> [path_for_afl]/afl-clang-fast -DHAVE_CONFIG_H -I.  -I. -I. -I../bfd -I./../bfd -I./../include -DLOCALEDIR="\"/usr/local/share/locale\"" -Dbin_dummy_emulation=bin_vanilla_emulation  -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -MT readelf.o -MD -MP -MF .deps/readelf.Tpo -c -o readelf.o readelf.c
> 
> [path_for_afl]/afl-clang-fast -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -o readelf readelf.o version.o unwind-ia64.o dwarf.o elfcomm.o  ../libiberty/libiberty.a -lz

- objcopy
> cd ./binutils
> 
> [path_for_afl]/afl-clang-fast -DHAVE_CONFIG_H -I.  -I. -I. -I../bfd -I./../bfd -I./../include -DLOCALEDIR="\"/usr/local/share/locale\"" -Dbin_dummy_emulation=bin_vanilla_emulation  -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -MT objcopy.o -MD -MP -MF .deps/objcopy.Tpo -c -o objcopy.o objcopy.c
> 
> [path_for_afl]/afl-clang-fast -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -o objcopy objcopy.o not-strip.o rename.o rddbg.o debug.o stabs.o ieee.o rdcoff.o wrstabs.o bucomm.o version.o filemode.o  ../bfd/.libs/libbfd.a ../libiberty/libiberty.a -lz

- objdump
> cd ./binutils
> 
> [path_for_afl]/afl-clang-fast -DHAVE_CONFIG_H -I.  -I. -I. -I../bfd -I./../bfd -I./../include -DLOCALEDIR="\"/usr/local/share/locale\"" -Dbin_dummy_emulation=bin_vanilla_emulation  -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -MT objdump.o -MD -MP -MF .deps/objdump.Tpo -c -o objdump.o -DOBJDUMP_PRIVATE_VECTORS="" ./objdump.c
> 
> [path_for_afl]//home/sonysame/Desktop/fuzzer/afl-2.52b/afl-clang-fast -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -o objdump objdump.o dwarf.o prdbg.o rddbg.o debug.o stabs.o ieee.o rdcoff.o bucomm.o version.o filemode.o elfcomm.o  ../opcodes/.libs/libopcodes.a ../bfd/.libs/libbfd.a ../libiberty/libiberty.a -lz

- nm
> cd ./binutils
> 
> [path_for_afl]/afl-clang-fast -DHAVE_CONFIG_H -I.  -I. -I. -I../bfd -I./../bfd -I./../include -DLOCALEDIR="\"/usr/local/share/locale\"" -Dbin_dummy_emulation=bin_vanilla_emulation  -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -MT nm.o -MD -MP -MF .deps/nm.Tpo -c -o nm.o nm.c
> 
> [path_for_afl]/afl-clang-fast -W -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -g -O2 -o nm nm.o bucomm.o version.o filemode.o  ../bfd/.libs/libbfd.a ../libiberty/libiberty.a -lz
