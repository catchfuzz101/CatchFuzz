
|program         |argument                                          |
|----------------|-------------------------------------------------------------|
|bison|`--defines=/dev/null --xml=/dev/null --graph=/dev/null -o /dev/null @@`                      |
|cflow|`-o /dev/null @@`            
|nasm|`-o /dev/null @@`
|jasper|`-o /dev/null -T mif -f @@`          
|dact|`-dcf @@`     
|readelf|`-a @@`
|objcopy|`-S @@`        
|objdump|`-d @@`        
|nm|`@@`

