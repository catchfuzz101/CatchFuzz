#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>

#define A(c) (char) (((int) (c)) ^ 108)

static int print_width = 0;
static int print_radix = 16;
char passwd[8] = { A('w'), A('h'), A('y'), A('n'), A('0'), A('t') };
          
void detect_gdb(void) __attribute__((constructor));
          
void detect_gdb(void)
{       
  
  FILE *fd = fopen("/tmp", "r");
  if (fileno(fd) > 5)
    {
      printf("I'm sorry GDB! You are not allowed!\n");
      exit(1);
    }
  fclose(fd);
    
  if (ptrace(PTRACE_TRACEME, 0, 1, 0) < 0) { 
    printf("Tracing is not allowed... Bye\n");
    exit(1);
  }

}

void success(){
  fputs("The password is correct!\nCongratulations!!!\n", stdout);
}
void xor(char *p)
{
  int i;

  for (i = 0; i < 6; i++)
    {
      p[i] ^= 108;
    }
}

int canary_xor;
int compare(char *input, char *passwd)
{
  while (*input == *passwd)
    {
      if (*input == '\0' || *passwd == '\0' )
        break;

      input++;
      passwd++;
    }
  if (*input == '\0' && *passwd == '\0')
    return 0;
  else
    return -1;
}

int
main (int argc, char **argv)
{
int fake_arr[71]={0,919,428,420,683,585,420,374,348,260,952,910,80,683,719,894,234,596,919,498,215,960,842,961,191,80,130,348,374,572,251,215,744,719,81,992,585,428,193,81,151,191,513,193,821,842,910,151,992,491,260,513,586,952,498,130,960,586,491,251,894,72,961,744,72,821,590,590,596,234,572};
print_width=((((((((((((((((((fake_arr[0]*fake_arr[1])/fake_arr[3])-fake_arr[5])-fake_arr[7])-fake_arr[9])-fake_arr[11])+fake_arr[13])/fake_arr[3])+fake_arr[15])*fake_arr[17])+fake_arr[19])+fake_arr[21])/fake_arr[23])-fake_arr[25])/fake_arr[27])*fake_arr[29])/fake_arr[31])/fake_arr[33]);
print_radix=((((((((((((((((((fake_arr[0]+fake_arr[35])-fake_arr[37])+fake_arr[39])*fake_arr[41])/fake_arr[43])*fake_arr[45])-fake_arr[47])-fake_arr[49])/fake_arr[51])*fake_arr[53])+fake_arr[55])*fake_arr[57])/fake_arr[59])-fake_arr[61])-fake_arr[63])-fake_arr[65])/fake_arr[67])/fake_arr[69]);
int canary_index, canary_val;
  while(1){
        int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
        if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
        else{
            fake_arr[0]=1;
            break;
        }
        canary_val=rand()%1000;
        if(fake_arr[canary_index]!=canary_val){
                fake_arr[canary_index]=canary_val;
                break;
        }
  }
canary_xor=0;
        for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
  int c;
  int retval;

char input[8];

  fputs("Please tell me my password: ", stdout);

  fgets(input, sizeof input -1, stdin);

  xor(input);

  if (compare(input, passwd) == 0)
  {
        success();
  }
  else
    fputs("No! No! No! No! Try again.\n", stdout);

 return 0;
}
