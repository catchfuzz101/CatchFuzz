
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>

#define A(c) (char) (((int) (c)) ^ 108)

char passwd[8] = { A('w'), A('h'), A('y'), A('n'), A('0'), A('t') };

void detect_gdb(void) __attribute__((constructor));

void detect_gdb(void)
{
  
  FILE *fd = fopen("/tmp", "r");
  if (fileno(fd) > 5)
    {
      printf("I'm sorry GDB! You are not allowed!\n");
      exit(1);
    }
  fclose(fd);

  if (ptrace(PTRACE_TRACEME, 0, 1, 0) < 0) {
    printf("Tracing is not allowed... Bye\n");
    exit(1);
  }

}

void success(){
  fputs("The password is correct!\nCongratulations!!!\n", stdout);
}
void xor(char *p)
{
  int i;

  for (i = 0; i < 6; i++)
    {
      p[i] ^= 108;
    }
}

int compare(char *input, char *passwd)
{
  while (*input == *passwd)
    {
      if (*input == '\0' || *passwd == '\0' )
	break;

      input++;
      passwd++;
    }
  if (*input == '\0' && *passwd == '\0')
    return 0;
  else
    return -1;
}

int main() {

  char input[8];
  int k=123;
int newvar_1=0;
if (k > 0 && k < 255)
{
    for (int i=0; i<=255; i++){
        if (i == k){
            newvar_1 = i;
            break;
        }
    }
}
else {
    // give up anti-taint (to avoid long loop)
    // but we change anyway ...
    newvar_1 = k;
}


//////////////// ANTI-TAINT-STR //////////////////
char a[20]="hello world!";
char newvar_2[strlen(a)];
if (strlen(a) < 30){    
    for (int i=0;i<strlen(a);i++){
        int ch=0;
        int temp = 0;
        int temp2 = 0;
        for (int j=0; j<8;j++){
            temp = a[i];
            temp2 = temp & (1<<j);
            if (temp2 !=0){
                ch |= 1<<j;
            }
        }
        newvar_2[i] = ch;
    }
}
else{    
    strncpy(newvar_2, a, strlen(a));
}
//////////////////////////////////////////////////

  fputs("Please tell me my password: ", stdout);

  fgets(input, sizeof input -1, stdin);

  xor(input);

  if (compare(input, passwd) == 0)
  {
	success();
  }
  else
    fputs("No! No! No! No! Try again.\n", stdout);

  return 0;

}
