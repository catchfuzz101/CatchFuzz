
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include "antifuzz.h"


#define A(c) (char) (((int) (c)) ^ 108)

char passwd[8] = { A('w'), A('h'), A('y'), A('n'), A('0'), A('t') };

//void detect_gdb(void) __attribute__((constructor));

void detect_gdb(void)
{
  
  FILE *fd = fopen("/tmp", "r");
  if (fileno(fd) > 5)
    {
      printf("I'm sorry GDB! You are not allowed!\n");
      exit(1);
    }
  fclose(fd);

  if (ptrace(PTRACE_TRACEME, 0, 1, 0) < 0) {
    printf("Tracing is not allowed... Bye\n");
    exit(1);
  }

}

void success(){
  fputs("The password is correct!\nCongratulations!!!\n", stdout);
}
void xor(char *p)
{
  int i;

  for (i = 0; i < 6; i++)
    {
      p[i] ^= 108;
    }
}

int compare(char *input, char *passwd)
{
  while (*input == *passwd)
    {
      if (*input == '\0' || *passwd == '\0' )
	break;

      input++;
      passwd++;
    }
  if (*input == '\0' && *passwd == '\0')
    return 0;
  else
    return -1;
}

int main(int argc,char* argv[]) {

  antifuzz_init(argv[0], FLAG_FILLBITMAP|FLAG_HEAVWEIGHTBB);
  char *antifuzzELF = "ELF";
  char *antifuzzELF2 = "ELF";

 //FILE *file = fopen("./README.md", "rb");

 //fseek(file, 0, SEEK_END);
    //size_t fileSize = sizeof(antifuzzELF2);
  //  fseek(file, 0, SEEK_SET);

    // Read the file content into a buffer
    //char *data = (char *)malloc(fileSize);
    //fread(data, 1, fileSize, file);
    //fclose(file);

   // antifuzz_encrypt_decrypt_buf(antifuzzELF2, fileSize);
   //free(data);
    // Print the original data

    // Call the encryption and decryption function
if(antifuzz_str_equal(antifuzzELF, "ELF")==0){
  char input[8];

  fputs("Please tell me my password: ", stdout);

  fgets(input, sizeof input -1, stdin);

  xor(input);

  if (compare(input, passwd) == 0)
  {
	success();
  }
  else
    fputs("No! No! No! No! Try again.\n", stdout);
}
  return 0;

}
