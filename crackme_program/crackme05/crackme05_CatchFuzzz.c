#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cpuid.h>

// Require that the input string pass a state machine matching:
// Qz+327x*[^n]n+(l9)+

void succeed(char* string) {
    printf("Yes, %s is correct!\n", string);
    exit(0);
}

void fail(char* string) {
    printf("No, %s is not correct.\n", string);
    exit(1);
}

static int print_width = 0;
static int print_radix = 16;
int canary_xor;
int compare(char *input, char *passwd)
{
  while (*input == *passwd)
    {
      if (*input == '\0' || *passwd == '\0' )
        break;

      input++;
      passwd++;
    }
  if (*input == '\0' && *passwd == '\0')
    return 0;
  else
    return -1;
}
int main(int argc, char** argv) {
int fake_arr[71]={0,919,428,420,683,585,420,374,348,260,952,910,80,683,719,894,234,596,919,498,215,960,842,961,191,80,130,348,374,572,251,215,744,719,81,992,585,428,193,81,151,191,513,193,821,842,910,151,992,491,260,513,586,952,498,130,960,586,491,251,894,72,961,744,72,821,590,590,596,234,572};
print_width=((((((((((((((((((fake_arr[0]*fake_arr[1])/fake_arr[3])-fake_arr[5])-fake_arr[7])-fake_arr[9])-fake_arr[11])+fake_arr[13])/fake_arr[3])+fake_arr[15])*fake_arr[17])+fake_arr[19])+fake_arr[21])/fake_arr[23])-fake_arr[25])/fake_arr[27])*fake_arr[29])/fake_arr[31])/fake_arr[33]);
print_radix=((((((((((((((((((fake_arr[0]+fake_arr[35])-fake_arr[37])+fake_arr[39])*fake_arr[41])/fake_arr[43])*fake_arr[45])-fake_arr[47])-fake_arr[49])/fake_arr[51])*fake_arr[53])+fake_arr[55])*fake_arr[57])/fake_arr[59])-fake_arr[61])-fake_arr[63])-fake_arr[65])/fake_arr[67])/fake_arr[69]);
int canary_index, canary_val;
while(1){
    int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
    if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
    else{
        fake_arr[0]=1;
        break;
    }
    canary_val=rand()%1000;
    if(fake_arr[canary_index]!=canary_val){
            fake_arr[canary_index]=canary_val;
            break;
    }
}
canary_xor=0;
for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
    char input[1000];

    fputs("Please tell me my password: ", stdout);

    fgets(input, sizeof input -1, stdin);

    char state = 0;
    for (int i = 0; input[i] != '\0'; i++) {
        char c =input[i];
        switch (state) {
            case 0:
                if (c == 'Q') { state = 1; } else { state = 12; }
                break;
            case 1:
                if (c == 'z') { state = 2; } else { state = 12; }
                break;
            case 2:
                if (c == 'z') { state = 2; }
                else if (c == '3')  { state = 3; }
                else { state = 12; }
                break;
            case 3:
                if (c == '2') { state = 4; } else { state = 12; }
                break;
            case 4:
                if (c == '7') { state = 5; } else { state = 12; }
                break;
            case 5:
                if (c == 'x') { state = 7; } else { state = 6; }
                break;
            case 6:
                if (c == 'n') { state = 8; } else { state = 12; }
                break;
            case 7:
                if (c != 'x') { state = 6; }
                break;
            case 8:
                if (c == 'l') { state = 9; }
                else if (c != 'n') { state = 12; }
                break; 
            case 9:
                if (c == '9') { state = 10; } else { state = 12; }
                break;
            case 10:
                if (c == 'l') { state = 9; } else { state = 12; }
                break;
            default:
                state = 12;
                break;
        }
    } 
    if (state == 10) {
        succeed(input);
    } else {
        fail(input);
    }
}
