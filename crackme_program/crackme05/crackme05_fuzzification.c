#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cpuid.h>

// Require that the input string pass a state machine matching:
// Qz+327x*[^n]n+(l9)+

void succeed(char* string) {
    printf("Yes, %s is correct!\n", string);
    exit(0);
}

void fail(char* string) {
    printf("No, %s is not correct.\n", string);
    exit(1);
}

int main(int argc, char** argv) {
int k=123;
int newvar_1=0;
if (k > 0 && k < 255)
{
    for (int i=0; i<=255; i++){
        if (i == k){
            newvar_1 = i;
            break;
        }
    }
}
else {
    // give up anti-taint (to avoid long loop)
    // but we change anyway ...
    newvar_1 = k;
}


//////////////// ANTI-TAINT-STR //////////////////
char a[20]="hello world!";
char newvar_2[strlen(a)];
if (strlen(a) < 30){    
    for (int i=0;i<strlen(a);i++){
        int ch=0;
        int temp = 0;
        int temp2 = 0;
        for (int j=0; j<8;j++){
            temp = a[i];
            temp2 = temp & (1<<j);
            if (temp2 !=0){
                ch |= 1<<j;
            }
        }
        newvar_2[i] = ch;
    }
}
else{    
    strncpy(newvar_2, a, strlen(a));
}
//////////////////////////////////////////////////

    char input[1000];

    fputs("Please tell me my password: ", stdout);

    fgets(input, sizeof input -1, stdin);

    char state = 0;
    for (int i = 0; input[i] != '\0'; i++) {
        char c =input[i];
        switch (state) {
            case 0:
                if (c == 'Q') { state = 1; } else { state = 12; }
                break;
            case 1:
                if (c == 'z') { state = 2; } else { state = 12; }
                break;
            case 2:
                if (c == 'z') { state = 2; }
                else if (c == '3')  { state = 3; }
                else { state = 12; }
                break;
            case 3:
                if (c == '2') { state = 4; } else { state = 12; }
                break;
            case 4:
                if (c == '7') { state = 5; } else { state = 12; }
                break;
            case 5:
                if (c == 'x') { state = 7; } else { state = 6; }
                break;
            case 6:
                if (c == 'n') { state = 8; } else { state = 12; }
                break;
            case 7:
                if (c != 'x') { state = 6; }
                break;
            case 8:
                if (c == 'l') { state = 9; }
                else if (c != 'n') { state = 12; }
                break; 
            case 9:
                if (c == '9') { state = 10; } else { state = 12; }
                break;
            case 10:
                if (c == 'l') { state = 9; } else { state = 12; }
                break;
            default:
                state = 12;
                break;
        }
    } 
    if (state == 10) {
        succeed(input);
    } else {
        fail(input);
    }
}
