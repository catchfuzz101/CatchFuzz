#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cpuid.h>

// Requires that the user input the CPUID,
// plus the bytes "3" and "Q";

void succeed(char* string) {
    printf("Yes, %s is correct!\n", string);
    exit(0);
}

void fail(char* string) {
    printf("No, %s is not correct.\n", string);
    exit(1);
}

void shift_int_to_char(int i, char* buff) {
    buff[0] = (i) & 0xFF;
    buff[1] = (i >> 8) & 0xFF;
    buff[2] = (i >> 16) & 0xFF;
    buff[3] = (i >> 24) & 0xFF;
}

int main(int argc, char** argv) {
int k=123;
int newvar_1=0;
if (k > 0 && k < 255)
{
    for (int i=0; i<=255; i++){
        if (i == k){
            newvar_1 = i;
            break;
        }
    }
}
else {
    // give up anti-taint (to avoid long loop)
    // but we change anyway ...
    newvar_1 = k;
}


//////////////// ANTI-TAINT-STR //////////////////
char a[20]="hello world!";
char newvar_2[strlen(a)];
if (strlen(a) < 30){    
    for (int i=0;i<strlen(a);i++){
        int ch=0;
        int temp = 0;
        int temp2 = 0;
        for (int j=0; j<8;j++){
            temp = a[i];
            temp2 = temp & (1<<j);
            if (temp2 !=0){
                ch |= 1<<j;
            }
        }
        newvar_2[i] = ch;
    }
}
else{    
    strncpy(newvar_2, a, strlen(a));
}
//////////////////////////////////////////////////


    unsigned int eax, ebx, ecx, edx;
    char* buff = malloc(sizeof(char) * 15);
    __get_cpuid(0, &eax, &ebx, &ecx, &edx);
    shift_int_to_char(ebx, buff);
    shift_int_to_char(edx, buff + 4);
    shift_int_to_char(ecx, buff + 8);
    buff[12] = '3';
    buff[13] = 'Q';
    buff[14] = '\0';
   

     char input[1000];

    fputs("Please tell me my password: ", stdout);

    fgets(input, sizeof input -1, stdin);
 
    int correct = (strcmp(buff, input) == 0);
    free(buff);

    if (correct) {
        succeed(input);
    } else {
        fail(input);
    }
}
