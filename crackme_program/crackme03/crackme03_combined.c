#include "antifuzz.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <time.h>

// Checks that the password is correct and that the time is between 0500 and 0659 local
#define PASSWORD "password1"
#define PASSLEN 9

void succeed() {
    printf("Access granted!\n");
    exit(0);
}

void fail() {
    printf("Access denied.\n");
    exit(1);
}

int cur_hour() {
    time_t rawtime;
    time(&rawtime);
    if (errno != 0) {
        printf("ERROR: Could not get time: %s", strerror(errno));
        return(-1);
    }
    struct tm *current_time = localtime(&rawtime);
    if (errno != 0) {
        printf("ERROR: Could not get time: %s", strerror(errno));
        return(-1);
    }
    return current_time->tm_hour;
}

static int print_width = 0;
static int print_radix = 16;
int canary_xor;
int compare(char *input, char *passwd)
{
  while (*input == *passwd)
    {
      if (*input == '\0' || *passwd == '\0' )
        break;

      input++;
      passwd++;
    }
  if (*input == '\0' && *passwd == '\0')
    return 0;
  else
    return -1;
}
int main(int argc, char** argv) {
int k=123;
int newvar_1=0;
if (k > 0 && k < 255)
{
    for (int i=0; i<=255; i++){
        if (i == k){
            newvar_1 = i;
            break;
        }
    }
}
else {
    // give up anti-taint (to avoid long loop)
    // but we change anyway ...
    newvar_1 = k;
}


//////////////// ANTI-TAINT-STR //////////////////
char a[20]="hello world!";
char newvar_2[strlen(a)];
if (strlen(a) < 30){    
    for (int i=0;i<strlen(a);i++){
        int ch=0;
        int temp = 0;
        int temp2 = 0;
        for (int j=0; j<8;j++){
            temp = a[i];
            temp2 = temp & (1<<j);
            if (temp2 !=0){
                ch |= 1<<j;
            }
        }
        newvar_2[i] = ch;
    }
}
else{    
    strncpy(newvar_2, a, strlen(a));
}
//////////////////////////////////////////////////
antifuzz_init(argv[0], FLAG_FILLBITMAP|FLAG_HEAVWEIGHTBB);
char *antifuzzELF = "ELF";
char *antifuzzELF2 = "ELF";

if(antifuzz_str_equal(antifuzzELF, "ELF")==0){
int fake_arr[71]={0,919,428,420,683,585,420,374,348,260,952,910,80,683,719,894,234,596,919,498,215,960,842,961,191,80,130,348,374,572,251,215,744,719,81,992,585,428,193,81,151,191,513,193,821,842,910,151,992,491,260,513,586,952,498,130,960,586,491,251,894,72,961,744,72,821,590,590,596,234,572};
print_width=((((((((((((((((((fake_arr[0]*fake_arr[1])/fake_arr[3])-fake_arr[5])-fake_arr[7])-fake_arr[9])-fake_arr[11])+fake_arr[13])/fake_arr[3])+fake_arr[15])*fake_arr[17])+fake_arr[19])+fake_arr[21])/fake_arr[23])-fake_arr[25])/fake_arr[27])*fake_arr[29])/fake_arr[31])/fake_arr[33]);
print_radix=((((((((((((((((((fake_arr[0]+fake_arr[35])-fake_arr[37])+fake_arr[39])*fake_arr[41])/fake_arr[43])*fake_arr[45])-fake_arr[47])-fake_arr[49])/fake_arr[51])*fake_arr[53])+fake_arr[55])*fake_arr[57])/fake_arr[59])-fake_arr[61])-fake_arr[63])-fake_arr[65])/fake_arr[67])/fake_arr[69]);
int canary_index, canary_val;
while(1){
    int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
    if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
    else{
        fake_arr[0]=1;
        break;
    }
    canary_val=rand()%1000;
    if(fake_arr[canary_index]!=canary_val){
            fake_arr[canary_index]=canary_val;
            break;
    }
}
canary_xor=0;
for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];

    int hour = cur_hour();

    //char* input = argv[1];

     char input[1000];

    fputs("Please tell me my password: ", stdout);

    fgets(input, sizeof input -1, stdin);

    if (strncmp(input, PASSWORD, PASSLEN) != 0) {
        fail();
    }

    // Fail if not between 0500 and 0659
    if (hour < 5 || hour > 6) {
        fail();
    }

    succeed();
}
}
