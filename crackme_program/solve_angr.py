import angr
import claripy
import time
def solve_target_code():
    # Create an Angr project
    project = angr.Project("a.out", auto_load_libs=False)

    # Define symbolic input
    input_length = 20  # Adjust based on the size of the input in the target code
    input_symbolic = claripy.BVS("input", 8 * input_length)

    # Create initial state with symbolic input
    initial_state = project.factory.entry_state(stdin=input_symbolic)

    # Create a simulation manager
    simulation = project.factory.simulation_manager(initial_state)

    # Define the address to reach (where the success message is printed)
    success_address = 0x0040088c  # Replace with the actual success address in the binary

    # Define a constraint to reach the success address
    simulation.explore(find=success_address)

    # Check if a solution was found
    if simulation.found:
        solution_state = simulation.found[0]
        solution_input = solution_state.solver.eval(input_symbolic, cast_to=bytes)
        print("Solution found! Input:", solution_input.decode('utf-8'))
    else:
        print("Solution not found.")

if __name__ == "__main__":
    start=time.time()
    solve_target_code()
    print(time.time()-start)
