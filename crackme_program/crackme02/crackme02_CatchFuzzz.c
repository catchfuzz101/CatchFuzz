#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Splits the string up into 4 segments and checks the modulus of their sums
// with a few fixed characters

void succeed(char* string) {
    printf("Yes, %s is correct!\n", string);
    exit(0);
}

void fail(char* string) {
    printf("No, %s is not correct.\n", string);
    exit(1);
}

int check_with_mod(char* substring, int n, int mod) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum = sum + substring[i];
    }
    return (sum % mod) == 0;
}

static int print_width = 0;
static int print_radix = 16;
int canary_xor;
int compare(char *input, char *passwd)
{
  while (*input == *passwd)
    {
      if (*input == '\0' || *passwd == '\0' )
        break;

      input++;
      passwd++;
    }
  if (*input == '\0' && *passwd == '\0')
    return 0;
  else
    return -1;
}
int main(int argc, char** argv) {
int fake_arr[71]={0,919,428,420,683,585,420,374,348,260,952,910,80,683,719,894,234,596,919,498,215,960,842,961,191,80,130,348,374,572,251,215,744,719,81,992,585,428,193,81,151,191,513,193,821,842,910,151,992,491,260,513,586,952,498,130,960,586,491,251,894,72,961,744,72,821,590,590,596,234,572};
print_width=((((((((((((((((((fake_arr[0]*fake_arr[1])/fake_arr[3])-fake_arr[5])-fake_arr[7])-fake_arr[9])-fake_arr[11])+fake_arr[13])/fake_arr[3])+fake_arr[15])*fake_arr[17])+fake_arr[19])+fake_arr[21])/fake_arr[23])-fake_arr[25])/fake_arr[27])*fake_arr[29])/fake_arr[31])/fake_arr[33]);
print_radix=((((((((((((((((((fake_arr[0]+fake_arr[35])-fake_arr[37])+fake_arr[39])*fake_arr[41])/fake_arr[43])*fake_arr[45])-fake_arr[47])-fake_arr[49])/fake_arr[51])*fake_arr[53])+fake_arr[55])*fake_arr[57])/fake_arr[59])-fake_arr[61])-fake_arr[63])-fake_arr[65])/fake_arr[67])/fake_arr[69]);
int canary_index, canary_val;
while(1){
    int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
    if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
    else{
        fake_arr[0]=1;
        break;
    }
    canary_val=rand()%1000;
    if(fake_arr[canary_index]!=canary_val){
            fake_arr[canary_index]=canary_val;
            break;
    }
}
canary_xor=0;
for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
    char input[1000];

    fputs("Please tell me my password: ", stdout);

    fgets(input, sizeof input -1, stdin);


    int len = strnlen(input, 1000);
    if (len != 16) {
        fail(input);
    }

    // EEBD ,,,, 2222 QQOO
    //  %3   %4   %5   %4
    // ..B. .... .... .Q..


    // Add some fixed characters
    if (input[2] != 'B') {
        fail(input);
    }

    if (input[13] != 'Q') {
        fail(input);
    }


    // Make the actual modulo checks
    if (!check_with_mod(input, 4, 3)) {
        fail(input);
    }

    if (!check_with_mod(input + 4, 4, 4)) {
        fail(input);
    }

    if (!check_with_mod(input + 8, 4, 5)) {
        fail(input);
    }

    if (!check_with_mod(input + 12, 4, 4)) {
        fail(input);
    }

    succeed(input);
}
