#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Splits the string up into 4 segments and checks the modulus of their sums
// with a few fixed characters

void succeed(char* string) {
    printf("Yes, %s is correct!\n", string);
    exit(0);
}

void fail(char* string) {
    printf("No, %s is not correct.\n", string);
    exit(1);
}

int check_with_mod(char* substring, int n, int mod) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum = sum + substring[i];
    }
    return (sum % mod) == 0;
}

int main(int argc, char** argv) {
int k=123;
int newvar_1=0;
if (k > 0 && k < 255)
{
    for (int i=0; i<=255; i++){
        if (i == k){
            newvar_1 = i;
            break;
        }
    }
}
else {
    // give up anti-taint (to avoid long loop)
    // but we change anyway ...
    newvar_1 = k;
}


//////////////// ANTI-TAINT-STR //////////////////
char a[20]="hello world!";
char newvar_2[strlen(a)];
if (strlen(a) < 30){    
    for (int i=0;i<strlen(a);i++){
        int ch=0;
        int temp = 0;
        int temp2 = 0;
        for (int j=0; j<8;j++){
            temp = a[i];
            temp2 = temp & (1<<j);
            if (temp2 !=0){
                ch |= 1<<j;
            }
        }
        newvar_2[i] = ch;
    }
}
else{    
    strncpy(newvar_2, a, strlen(a));
}
//////////////////////////////////////////////////
char input[1000];

fputs("Please tell me my password: ", stdout);

fgets(input, sizeof input -1, stdin);

    
    int len = strnlen(input, 1000);
    if (len != 16) {
        fail(input);
    }

    // EEBD ,,,, 2222 QQOO
    //  %3   %4   %5   %4
    // ..B. .... .... .Q..


    // Add some fixed characters
    if (input[2] != 'B') {
        fail(input);
    }

    if (input[13] != 'Q') {
        fail(input);
    }


    // Make the actual modulo checks
    if (!check_with_mod(input, 4, 3)) {
        fail(input);
    }

    if (!check_with_mod(input + 4, 4, 4)) {
        fail(input);
    }

    if (!check_with_mod(input + 8, 4, 5)) {
        fail(input);
    }

    if (!check_with_mod(input + 12, 4, 4)) {
        fail(input);
    }

    succeed(input);
}
