#[src location]
#bison   -> src
#cflow   -> src
#nasm    -> asm
#dact    -> .
#jasper  -> src/appl
#readelf -> binutils
#objdump -> binutils
#objcopy -> binutils
#nm 	 -> binutils

if [ $# -ne 2 ]
  then
    echo "[*] Usage: ./make.sh bison-3.3 [src folder]"
    exit 1
fi

rm -rf ./$1 || true
tar -xvf $1.tar.xz




cp ../antifuzz/program/$1/antifuzz/*.c ./$1/$2



