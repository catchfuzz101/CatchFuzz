# Bison.
# Copyright (C) 2018 Free Software Foundation, Inc.
# This file is distributed under the same license as the bison package.
# Pedro Albuquerque <palbuquerque73@gmail.com>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: bison-3.2.91\n"
"Report-Msgid-Bugs-To: bug-bison@gnu.org\n"
"POT-Creation-Date: 2019-01-26 11:33+0100\n"
"PO-Revision-Date: 2019-01-20 16:47+0000\n"
"Last-Translator: Pedro Albuquerque <palbuquerque73@gmail.com>\n"
"Language-Team: Portuguese <translation-team-pt@lists.sourceforge.net>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=2; plural=n != 1;\\n\n"
"X-Generator: Gtranslator 2.91.7\n"

#: src/complain.c:321
msgid "fatal error"
msgstr "erro fatal"

#: src/complain.c:322
msgid "error"
msgstr "erro"

#: src/complain.c:323
msgid "warning"
msgstr "aviso"

#: src/complain.c:386
#, c-format
msgid "POSIX Yacc does not support %s"
msgstr "POSIX Yacc não suporta %s"

#: src/complain.c:394
#, c-format
msgid "deprecated directive, use %s"
msgstr "directiva obsoleta, use %s"

#: src/complain.c:398
#, c-format
msgid "deprecated directive: %s, use %s"
msgstr "directiva obsoleta: %s, use %s"

#: src/complain.c:411
#, c-format
msgid "duplicate directive"
msgstr "directiva duplicada"

#: src/complain.c:413
#, c-format
msgid "duplicate directive: %s"
msgstr "directiva duplicada: %s"

#: src/complain.c:415 src/complain.c:426 src/reader.c:135 src/symtab.c:286
#: src/symtab.c:299
#, c-format
msgid "previous declaration"
msgstr "declaração anterior"

#: src/complain.c:424
#, c-format
msgid "only one %s allowed per rule"
msgstr "só uma %s permitida por regra"

#: src/conflicts.c:77
#, c-format
msgid "    Conflict between rule %d and token %s resolved as shift"
msgstr "    Conflito entre regra %d e símbolo %s resolvido como troca"

#: src/conflicts.c:86
#, c-format
msgid "    Conflict between rule %d and token %s resolved as reduce"
msgstr "    Conflito entre regra %d e símbolo %s resolvido como redução"

#: src/conflicts.c:94
#, c-format
msgid "    Conflict between rule %d and token %s resolved as an error"
msgstr "    Conflito entre regra %d e símbolo %s resolvido como erro"

#: src/conflicts.c:594
#, c-format
msgid "State %d "
msgstr "Estado %d"

#: src/conflicts.c:597
#, c-format
msgid "conflicts: %d shift/reduce, %d reduce/reduce\n"
msgstr "conflitos: %d troca/redução, %d redução/redução\n"

#: src/conflicts.c:600
#, c-format
msgid "conflicts: %d shift/reduce\n"
msgstr "conflitos: %d troca/redução\n"

#: src/conflicts.c:602
#, c-format
msgid "conflicts: %d reduce/reduce\n"
msgstr "conflitos: %d redução/redução\n"

#: src/conflicts.c:638
#, c-format
msgid "shift/reduce conflicts for rule %d: %d found, %d expected"
msgstr "conflitos troca/redução para a regra %d: %d encontrados, %d esperados"

#: src/conflicts.c:644
#, c-format
msgid "reduce/reduce conflicts for rule %d: %d found, %d expected"
msgstr ""
"conflitos redução/redução para a regra %d: %d encontrados, %d esperados"

#: src/conflicts.c:662
#, c-format
msgid "%%expect-rr applies only to GLR parsers"
msgstr "%%expect-rr só se aplica a analisadores GLR"

#: src/conflicts.c:679
#, c-format
msgid "shift/reduce conflicts: %d found, %d expected"
msgstr "conflitos troca/redução: %d encontrados, %d esperados"

#: src/conflicts.c:684
#, c-format
msgid "%d shift/reduce conflict"
msgid_plural "%d shift/reduce conflicts"
msgstr[0] "%d conflito troca/redução"
msgstr[1] "%d conflitos troca/redução"

#: src/conflicts.c:701
#, c-format
msgid "reduce/reduce conflicts: %d found, %d expected"
msgstr "conflitos redução/redução: %d encontrados, %d esperados"

#: src/conflicts.c:706
#, c-format
msgid "%d reduce/reduce conflict"
msgid_plural "%d reduce/reduce conflicts"
msgstr[0] "%d conflito troca/redução"
msgstr[1] "%d conflitos troca/redução"

#: src/files.c:123
#, c-format
msgid "%s: cannot open"
msgstr "%s: impossível abrir"

#: src/files.c:139
#, c-format
msgid "input/output error"
msgstr "erro de entrada/saída"

#: src/files.c:142
#, c-format
msgid "cannot close file"
msgstr "impossível fechar o ficheiro"

#: src/files.c:372
#, c-format
msgid "refusing to overwrite the input file %s"
msgstr "recusa de sobrescrever ficheiro de entrada %s"

#: src/files.c:380
#, c-format
msgid "conflicting outputs to file %s"
msgstr "saidas conflituosas para ficheiro %s"

#: src/fixits.c:117
#, c-format
msgid "%s: cannot backup"
msgstr "%s: impossível criar segurança"

#: src/getargs.c:257
#, c-format
msgid "Try '%s --help' for more information.\n"
msgstr "Tente \"%s --help\" para mais informação.\n"

#: src/getargs.c:266
#, c-format
msgid "Usage: %s [OPTION]... FILE\n"
msgstr "Uso: %s [OPÇÃO]... FICHEIRO\n"

#: src/getargs.c:267
msgid ""
"Generate a deterministic LR or generalized LR (GLR) parser employing\n"
"LALR(1), IELR(1), or canonical LR(1) parser tables.  IELR(1) and\n"
"canonical LR(1) support is experimental.\n"
"\n"
msgstr ""
"Gerar um analisador LR determinista ou LR generalizado (GLR) usando\n"
"tabelas de análise LALR(1), IELR(1) ou LR(1) canónica. O suporte a\n"
"IELR(1) e LR(1) canónica é experimental.\n"
"\n"

#: src/getargs.c:274
msgid ""
"Mandatory arguments to long options are mandatory for short options too.\n"
msgstr ""
"Argumentos obrigatórios para opções longas também são obrigatórios para "
"opções curtas.\n"

#: src/getargs.c:277
msgid "The same is true for optional arguments.\n"
msgstr "Igualmente verdadeiro para argumentos opcionais.\n"

#: src/getargs.c:281
#, fuzzy
#| msgid ""
#| "\n"
#| "Operation modes:\n"
#| "  -h, --help                 display this help and exit\n"
#| "  -V, --version              output version information and exit\n"
#| "      --print-localedir      output directory containing locale-dependent "
#| "data\n"
#| "                             and exit\n"
#| "      --print-datadir        output directory containing skeletons and "
#| "XSLT\n"
#| "                             and exit\n"
#| "  -u, --update               apply fixes to the source grammar file\n"
#| "  -y, --yacc                 emulate POSIX Yacc\n"
#| "  -W, --warnings[=CATEGORY]  report the warnings falling in CATEGORY\n"
#| "  -f, --feature[=FEATURE]    activate miscellaneous features\n"
#| "\n"
msgid ""
"\n"
"Operation modes:\n"
"  -h, --help                 display this help and exit\n"
"  -V, --version              output version information and exit\n"
"      --print-localedir      output directory containing locale-dependent "
"data\n"
"                             and exit\n"
"      --print-datadir        output directory containing skeletons and XSLT\n"
"                             and exit\n"
"  -u, --update               apply fixes to the source grammar file and "
"exit\n"
"  -y, --yacc                 emulate POSIX Yacc\n"
"  -W, --warnings[=CATEGORY]  report the warnings falling in CATEGORY\n"
"  -f, --feature[=FEATURE]    activate miscellaneous features\n"
"\n"
msgstr ""
"\n"
"Modos de operação:\n"
"  -h, --help                 mostra esta ajuda e sai\n"
"  -V, --version              mostra informação da versão e sai\n"
"      --print-localedir      mostra a pasta com dados dependentes da "
"definição regional\n"
"                             e sai\n"
"      --print-datadir        mostra a pasta com esqueletos e XSLT\n"
"                             e sai\n"
"  -u, --update               aplica reparações ao ficheiro fonte de "
"gramática\n"
"  -y, --yacc                 emula POSIX Yacc\n"
"  -W, --warnings[=CATEGORY]  reporta os avisos dentro de CATEGORIA\n"
"  -f, --feature[=FEATURE]    activa várias funcionalidades\n"
"\n"

#: src/getargs.c:297
#, c-format
msgid ""
"Parser:\n"
"  -L, --language=LANGUAGE          specify the output programming language\n"
"  -S, --skeleton=FILE              specify the skeleton to use\n"
"  -t, --debug                      instrument the parser for tracing\n"
"                                   same as '-Dparse.trace'\n"
"      --locations                  enable location support\n"
"  -D, --define=NAME[=VALUE]        similar to '%define NAME \"VALUE\"'\n"
"  -F, --force-define=NAME[=VALUE]  override '%define NAME \"VALUE\"'\n"
"  -p, --name-prefix=PREFIX         prepend PREFIX to the external symbols\n"
"                                   deprecated by '-Dapi.prefix=PREFIX'\n"
"  -l, --no-lines                   don't generate '#line' directives\n"
"  -k, --token-table                include a table of token names\n"
msgstr ""
"Analisador:\n"
"  -L, --language=LINGUAGEM         especifa linguagem de programação de "
"saída\n"
"  -S, --skeleton=FICHEIRO          especifica o esqueleto a usar\n"
"  -t, --debug                      instrui o analisador para rastreio\n"
"                                   tal como \"-Dparse.trace\"\n"
"      --locations                  activa suporte a localização\n"
"  -D, --define=NOME[=VALOR]        similar a \"%define NOME \"VALOR\"\"\n"
"  -F, --force-define=NOME[=VALOR]  sobrepõe \"%define NOME \"VALOR\"\"\n"
"  -p, --name-prefix=PREFIXO        apõe PREFIXO aos símbolos externos\n"
"                                   obsoleto desde \"-Dapi.prefix=PREFIXO\"\n"
"  -l, --no-lines                   não gera directivas \"#line\"\n"
"  -k, --token-table                inclui uma tabela de nomes de símbolos\n"

#: src/getargs.c:315
msgid ""
"Output:\n"
"      --defines[=FILE]       also produce a header file\n"
"  -d                         likewise but cannot specify FILE (for POSIX "
"Yacc)\n"
"  -r, --report=THINGS        also produce details on the automaton\n"
"      --report-file=FILE     write report to FILE\n"
"  -v, --verbose              same as '--report=state'\n"
"  -b, --file-prefix=PREFIX   specify a PREFIX for output files\n"
"  -o, --output=FILE          leave output to FILE\n"
"  -g, --graph[=FILE]         also output a graph of the automaton\n"
"  -x, --xml[=FILE]           also output an XML report of the automaton\n"
"                             (the XML schema is experimental)\n"
msgstr ""
"Saída:\n"
"      --defines[=FICHEIRO]   produz também um ficheiro de cabeçalho\n"
"  -d                         igual mas sem especificar FICHEIRO (para POSIX "
"Yacc)\n"
"  -r, --report=COISAS        produz também detalhes do automaton\n"
"      --report-file=FICHEIRO escreve relatório em FICHEIRO\n"
"  -v, --verbose              igual a \"--report=state\"\n"
"  -b, --file-prefix=PREFIXO  especifica PREFIXO para ficheiros de saída\n"
"  -o, --output=FICHEIRO      deixar saída em FICHEIRO\n"
"  -g, --graph[=FICHEIRO]     produz também um gráfco do automaton\n"
"  -x, --xml[=FICHEIRO]       produz também um relatório XML do automaton\n"
"                             (o esquema XML é experimental)\n"

#: src/getargs.c:330
#, c-format
msgid ""
"Warning categories include:\n"
"  'midrule-values'    unset or unused midrule values\n"
"  'yacc'              incompatibilities with POSIX Yacc\n"
"  'conflicts-sr'      S/R conflicts (enabled by default)\n"
"  'conflicts-rr'      R/R conflicts (enabled by default)\n"
"  'deprecated'        obsolete constructs\n"
"  'empty-rule'        empty rules without %empty\n"
"  'precedence'        useless precedence and associativity\n"
"  'other'             all other warnings (enabled by default)\n"
"  'all'               all the warnings except 'yacc'\n"
"  'no-CATEGORY'       turn off warnings in CATEGORY\n"
"  'none'              turn off all the warnings\n"
"  'error[=CATEGORY]'  treat warnings as errors\n"
msgstr ""
"Categorias de aviso incluem:\n"
"  \"midrule-values\"    valores midrule não usados ou indefinidos\n"
"  \"yacc\"              incompatibilidades com POSIX Yacc\n"
"  \"conflicts-sr\"      conflitos S/R (activo por predefinição)\n"
"  \"conflicts-rr\"      conflitos R/R (activo por predefinição)\n"
"  \"deprecated\"        construções obsoletas\n"
"  \"empty-rule\"        regras vazias sem %empty\n"
"  \"precedence\"        precedência e associatividade inúteis\n"
"  \"other\"             todos os outros avisos (activo por predefinição)\n"
"  \"all\"               todos os avisos excepto \"yacc\"\n"
"  \"no-CATEGORIA\"      desliga avisos em CATEGORIA\n"
"  \"none\"              desliga todos os avisos\n"
"  \"error[=CATEGORIA]\" trata avisos como erros\n"

#: src/getargs.c:347
msgid ""
"THINGS is a list of comma separated words that can include:\n"
"  'state'        describe the states\n"
"  'itemset'      complete the core item sets with their closure\n"
"  'lookahead'    explicitly associate lookahead tokens to items\n"
"  'solved'       describe shift/reduce conflicts solving\n"
"  'all'          include all the above information\n"
"  'none'         disable the report\n"
msgstr ""
"COISAS é uma lista de palavras entre vírgulas que pode incluir:\n"
"  \"state\"        descreve o estado\n"
"  \"itemset\"      completa conjuntos de itens do núcleo com o fecho\n"
"  \"lookahead\"    associa explicitamente símbolos lookahead a itens\n"
"  \"solved\"       descreve resolução de conflitos troca/redução\n"
"  \"all\"          inclui toda a informação acima\n"
"  \"none\"         desactiva o relatório\n"

#: src/getargs.c:358
msgid ""
"FEATURE is a list of comma separated words that can include:\n"
"  'caret'        show errors with carets\n"
"  'all'          all of the above\n"
"  'none'         disable all of the above\n"
"  "
msgstr ""
"FUNCIONALIDADE é uma lista de palavras entre vírgulas que pode incluir:\n"
"  \"caret\"        mostra erros com circunflexos\n"
"  \"all\"          todos os acima\n"
"  \"none\"         desactiva todos os acima\n"
".."

#: src/getargs.c:366
#, c-format
msgid "Report bugs to <%s>.\n"
msgstr "Reportar erros a <%s>.\n"

#: src/getargs.c:367
#, c-format
msgid "%s home page: <%s>.\n"
msgstr "Página inicial de %s: <%s>.\n"

#: src/getargs.c:368
msgid "General help using GNU software: <http://www.gnu.org/gethelp/>.\n"
msgstr "Ajuda geral para usar programas GNU: <http://www.gnu.org/gethelp/>.\n"

#. TRANSLATORS: Replace LANG_CODE in this URL with your language
#. code <http://translationproject.org/team/LANG_CODE.html> to
#. form one of the URLs at http://translationproject.org/team/.
#. Otherwise, replace the entire URL with your translation team's
#. email address.
#: src/getargs.c:383
msgid "Report translation bugs to <http://translationproject.org/team/>.\n"
msgstr "Reportar erros de tradução a <http://translationproject.org/team/>.\n"

#: src/getargs.c:386
msgid "For complete documentation, run: info bison.\n"
msgstr "Para documentação completa, execute: info bison.\n"

#: src/getargs.c:402
#, c-format
msgid "bison (GNU Bison) %s"
msgstr "bison (GNU Bison) %s"

#: src/getargs.c:404
msgid "Written by Robert Corbett and Richard Stallman.\n"
msgstr "Escrito por Robert Corbett e Richard Stallman.\n"

#: src/getargs.c:408
#, c-format
msgid "Copyright (C) %d Free Software Foundation, Inc.\n"
msgstr "Copyright (C) %d Free Software Foundation, Inc.\n"

#: src/getargs.c:411
msgid ""
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
msgstr ""
"é um programa gratuito; veja a fonte para condições de cópia. NÃO há "
"QUALQUER\n"
"garantia; nem mesmo para COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM PROPÓSITO "
"PARTICULAR.\n"

#: src/getargs.c:433
#, c-format
msgid "multiple skeleton declarations are invalid"
msgstr "múltiplas declarações de esqueleto são inválidas"

#: src/getargs.c:450
#, c-format
msgid "%s: invalid language"
msgstr "%s: linguagem inválida"

#: src/getargs.c:453
msgid "multiple language declarations are invalid"
msgstr "múltiplas declarações de linguagem são inválidas"

#: src/getargs.c:740
#, c-format
msgid "missing operand"
msgstr "operando em falta"

#: src/getargs.c:742
#, c-format
msgid "extra operand %s"
msgstr "operando extra %s"

#: src/gram.c:178
msgid "Grammar"
msgstr "Gramática"

#: src/graphviz.c:46
#, c-format
msgid ""
"// Generated by %s.\n"
"// Report bugs to <%s>.\n"
"// Home page: <%s>.\n"
"\n"
msgstr ""
"// Gerado por %s.\n"
"// Reportar erros a <%s>.\n"
"// Página inicial: <%s>.\n"
"\n"

#: src/location.c:94 src/scan-gram.l:937
#, c-format
msgid "line number overflow"
msgstr "transporte de nº de linha"

#: src/location.c:96
#, c-format
msgid "column number overflow"
msgstr "transporte de nº de coluna"

#: src/main.c:154
msgid "rule useless in parser due to conflicts"
msgstr "regra inútil no analisador devidoa aconflitos"

#: src/main.c:244
#, c-format
msgid "fix-its can be applied.  Rerun with option '--update'."
msgstr "podem ser aplicados fix-its. Re-executar com a opção \"--update\"."

#: src/muscle-tab.c:536
#, c-format
msgid "%%define variable %s redefined"
msgstr "Variável %%define %s redefinida"

#: src/muscle-tab.c:540 src/symtab.c:312
#, c-format
msgid "previous definition"
msgstr "definição anterior"

#: src/muscle-tab.c:599 src/muscle-tab.c:700 src/muscle-tab.c:759
#, c-format
msgid "%s: undefined %%define variable %s"
msgstr "%s: variável %%define %s indefinida"

#: src/muscle-tab.c:694
#, c-format
msgid "invalid value for %%define Boolean variable %s"
msgstr "valor inválido para variável %%define %s booliana"

#: src/muscle-tab.c:746
#, c-format
msgid "invalid value for %%define variable %s: %s"
msgstr "valor inválido para variável %%define %s: %s"

#: src/muscle-tab.c:751
#, c-format
msgid "accepted value: %s"
msgstr "valor aceite: %s"

#: src/parse-gram.y:724
msgid "character literals cannot be nonterminals"
msgstr "literais de caracteres não podem ser não-terminais"

#: src/parse-gram.y:856
#, c-format
msgid "missing identifier in parameter declaration"
msgstr "identificador em falta na declaração do parâmetro"

#: src/parse-gram.y:948 src/parse-gram.y:956
#, c-format
msgid "invalid version requirement: %s"
msgstr "requerimento de versão inválido: %s"

#: src/parse-gram.y:970
#, c-format
msgid "require bison %s, but have %s"
msgstr "requerido bison %s, obtido %s"

#: src/print.c:153
#, c-format
msgid "shift, and go to state %d\n"
msgstr "troca e vai para estado %d\n"

#: src/print.c:155
#, c-format
msgid "go to state %d\n"
msgstr "vai para estado %d\n"

#: src/print.c:190
msgid "error (nonassociative)\n"
msgstr "erro (não associativo)\n"

#: src/print.c:212
#, c-format
msgid "reduce using rule %d (%s)"
msgstr "redução com regra %d (%s)"

#: src/print.c:215
#, c-format
msgid "accept"
msgstr "aceitar"

#: src/print.c:252 src/print.c:326
msgid "$default"
msgstr "$default"

#: src/print.c:362 src/print_graph.c:61
#, c-format
msgid "State %d"
msgstr "Estado %d"

#: src/print.c:392
msgid "Terminals, with rules where they appear"
msgstr "Terminais, com regras onde aparecem"

#: src/print.c:428
msgid "Nonterminals, with rules where they appear"
msgstr "Não terminais, com regras onde aparecem"

#: src/print.c:459
#, c-format
msgid " on left:"
msgstr " à esquerda:"

#: src/print.c:476
#, c-format
msgid " on right:"
msgstr " à direita:"

#: src/print.c:502
msgid "Rules useless in parser due to conflicts"
msgstr "Regras inúteis no analisador devido a conflitos"

#: src/reader.c:59
#, c-format
msgid "multiple %s declarations"
msgstr "múltiplas declarações %s"

#: src/reader.c:128
#, c-format
msgid "result type clash on merge function %s: <%s> != <%s>"
msgstr "choque no tipo de resultado na função de união %s: <%s> != <%s>"

#: src/reader.c:201
#, c-format
msgid "duplicated symbol name for %s ignored"
msgstr "nome de símbolo duplicado para %s ignorado"

#: src/reader.c:243
#, c-format
msgid "rule given for %s, which is a token"
msgstr "regra dada para %s, que é um símbolo"

#: src/reader.c:300
#, c-format
msgid "type clash on default action: <%s> != <%s>"
msgstr "choque de tipo em acção predefinida: <%s> != <%s>"

#: src/reader.c:323
#, c-format
msgid "empty rule for typed nonterminal, and no action"
msgstr "regra vazia para não terminais digitados e sem acção"

#: src/reader.c:339
#, c-format
msgid "unused value: $%d"
msgstr "valor não usado: $%d"

#: src/reader.c:341
#, c-format
msgid "unset value: $$"
msgstr "valor não definido: $$"

#: src/reader.c:350
#, c-format
msgid "%%empty on non-empty rule"
msgstr "%%empty em regra não-vazia"

#: src/reader.c:356
#, c-format
msgid "empty rule without %%empty"
msgstr "regra vazia sem %%empty"

#: src/reader.c:366
#, c-format
msgid "token for %%prec is not defined: %s"
msgstr "símbolo para %%prec não está definido: %s"

#: src/reader.c:371
#, c-format
msgid "only midrule actions can be typed: %s"
msgstr "só acções na regra podem ser inseridas: %s"

#: src/reader.c:498 src/reader.c:520 src/reader.c:591
#, c-format
msgid "%s affects only GLR parsers"
msgstr "%s só afecta analisadores GLR"

#: src/reader.c:501
#, c-format
msgid "%s must be followed by positive number"
msgstr "%s tem de ser seguido por um nº positivo"

#: src/reader.c:556
#, c-format
msgid "POSIX Yacc does not support typed midrule actions"
msgstr "POSIX Yacc não suporta inserção de acções na regra"

#: src/reader.c:661
#, c-format
msgid "rule is too long"
msgstr "regra muito longa"

#: src/reader.c:791
#, c-format
msgid "no rules in the input grammar"
msgstr "sem regras na gramática de entrada"

#: src/reduce.c:223
msgid "rule useless in grammar"
msgstr "regra inútil na gramática"

#: src/reduce.c:280
#, c-format
msgid "nonterminal useless in grammar: %s"
msgstr "não-terminal inútil na gramática: %s"

#: src/reduce.c:323
msgid "Nonterminals useless in grammar"
msgstr "não-terminais inúteis na gramática"

#: src/reduce.c:335
msgid "Terminals unused in grammar"
msgstr "Terminais não usados na gramática"

#: src/reduce.c:344
msgid "Rules useless in grammar"
msgstr "Regras inúteis na gramática"

#: src/reduce.c:357
#, c-format
msgid "%d nonterminal useless in grammar"
msgid_plural "%d nonterminals useless in grammar"
msgstr[0] "%d não-terminal inútil na gramática"
msgstr[1] "%d não-terminais inúteis na gramática"

#: src/reduce.c:362
#, c-format
msgid "%d rule useless in grammar"
msgid_plural "%d rules useless in grammar"
msgstr[0] "%d regra inútil na gramática"
msgstr[1] "%d regras inúteis na gramática"

#: src/reduce.c:389
#, c-format
msgid "start symbol %s does not derive any sentence"
msgstr "símbolo %s inicial não deriva nenhuma frase"

# pt_PT: escolhida a palavra "errante" para evitar masculino/feminino
#: src/scan-code.l:157
#, c-format
msgid "stray '%s'"
msgstr "\"%s\" errante"

#: src/scan-code.l:329
#, c-format
msgid "refers to: %c%s at %s"
msgstr "refere-se a: %c%s em %s"

#: src/scan-code.l:353
#, c-format
msgid "possibly meant: %c"
msgstr "possível significado: %c"

#: src/scan-code.l:362
#, c-format
msgid ", hiding %c"
msgstr ", a ocultar %c"

#: src/scan-code.l:370
#, c-format
msgid " at %s"
msgstr " em %s"

#: src/scan-code.l:374
#, c-format
msgid ", cannot be accessed from midrule action at $%d"
msgstr ", não pode ser acedido de acção a meia regra em $%d"

#: src/scan-code.l:422 src/scan-gram.l:860
#, c-format
msgid "integer out of range: %s"
msgstr "inteiro fora do intervalo: %s"

#: src/scan-code.l:497
#, c-format
msgid "invalid reference: %s"
msgstr "referência inválida: %s"

#: src/scan-code.l:505
#, c-format
msgid "syntax error after '%c', expecting integer, letter, '_', '[', or '$'"
msgstr "erro de sintaxe após \"%c\", esperado inteiro, letra, '_', '[', ou '$'"

#: src/scan-code.l:511
#, c-format
msgid "symbol not found in production before $%d: %.*s"
msgstr "símbolo não encontrado em produção antes de $%d: %.*s"

#: src/scan-code.l:516
#, c-format
msgid "symbol not found in production: %.*s"
msgstr "símbolo não encontrado em produção: %.*s"

#: src/scan-code.l:531
#, c-format
msgid "misleading reference: %s"
msgstr "referência enganosa: %s"

#: src/scan-code.l:547
#, c-format
msgid "ambiguous reference: %s"
msgstr "referência ambígua: %s"

#: src/scan-code.l:583
#, c-format
msgid "explicit type given in untyped grammar"
msgstr "tipo explícito indicado em gramática atípica"

#: src/scan-code.l:638
#, c-format
msgid "$$ for the midrule at $%d of %s has no declared type"
msgstr "$$ para midrule em $%d de %s não tem tipo declarado"

#: src/scan-code.l:644
#, c-format
msgid "$$ of %s has no declared type"
msgstr "$$ de %s não tem tipo declarado"

#: src/scan-code.l:670
#, c-format
msgid "$%s of %s has no declared type"
msgstr "$%s de %s não tem tipo declarado"

#: src/scan-code.l:691
#, c-format
msgid "multiple occurrences of $%d with api.value.automove"
msgstr "múltiplas ocorrências de $%d com api.value.automove"

#: src/scan-gram.l:190
#, c-format
msgid "stray ',' treated as white space"
msgstr "\",\" extraviada tratada como espaço"

#: src/scan-gram.l:285
#, c-format
msgid "invalid directive: %s"
msgstr "directiva inválida: %s"

#: src/scan-gram.l:305
#, c-format
msgid "invalid identifier: %s"
msgstr "identificador inválido: %s"

#: src/scan-gram.l:357
msgid "invalid character"
msgid_plural "invalid characters"
msgstr[0] "carácter inválido"
msgstr[1] "caracteres inválidos"

#: src/scan-gram.l:375
#, c-format
msgid "invalid null character"
msgstr "carácter nulo inválido"

#: src/scan-gram.l:428
#, c-format
msgid "unexpected identifier in bracketed name: %s"
msgstr "identificador inesperado em nome entre parênteses: %s"

#: src/scan-gram.l:450
#, c-format
msgid "an identifier expected"
msgstr "esperado um identificador"

#: src/scan-gram.l:455
msgid "invalid character in bracketed name"
msgid_plural "invalid characters in bracketed name"
msgstr[0] "carácter inválido em nome entre parênteses"
msgstr[1] "caracteres inválidos em nome entre parênteses"

#: src/scan-gram.l:526
#, c-format
msgid "POSIX Yacc does not support string literals"
msgstr "POSIX Yacc não suporta literais de cadeia"

#: src/scan-gram.l:548
#, c-format
msgid "empty character literal"
msgstr "literal de carácter vazio"

#: src/scan-gram.l:554
#, c-format
msgid "extra characters in character literal"
msgstr "caracteres extra em literal de carácter"

#: src/scan-gram.l:601 src/scan-gram.l:611 src/scan-gram.l:631
#, c-format
msgid "invalid number after \\-escape: %s"
msgstr "número inválido após \\-escape: %s"

#: src/scan-gram.l:643
#, c-format
msgid "invalid character after \\-escape: %s"
msgstr "carácter inválido após \\-escape: %s"

#: src/scan-gram.l:854
#, c-format
msgid "POSIX Yacc does not support hexadecimal literals"
msgstr "POSIX Yacc não suporta literais hexadecimais"

#: src/scan-gram.l:989
#, c-format
msgid "missing %s at end of file"
msgstr "%s em falta no fim do ficheiro"

#: src/scan-gram.l:1000
#, c-format
msgid "missing %s at end of line"
msgstr "%s em falta no fim da linha"

#: src/scan-skel.l:140
#, c-format
msgid "unclosed %s directive in skeleton"
msgstr "directiva %s por fechar no esqueleto"

#: src/scan-skel.l:257
#, c-format
msgid "too few arguments for %s directive in skeleton"
msgstr "poucos argumentos na directiva %s no esqueleto"

#: src/scan-skel.l:264
#, c-format
msgid "too many arguments for %s directive in skeleton"
msgstr "muitos argumentos na directiva %s no esqueleto"

#: src/symtab.c:98
#, c-format
msgid "POSIX Yacc forbids dashes in symbol names: %s"
msgstr "POSIX Yacc proíbe traços em nomes de símbolos: %s"

#: src/symtab.c:108
#, c-format
msgid "too many symbols in input grammar (limit is %d)"
msgstr "muitos símbolos na gramática de entrada (limite é %d)"

#: src/symtab.c:283
#, c-format
msgid "%s redeclaration for %s"
msgstr "re-declaração de %s para %s"

#: src/symtab.c:296
#, c-format
msgid "%s redeclaration for <%s>"
msgstr "re-declaração de %s para <%s>"

#: src/symtab.c:308
#, c-format
msgid "symbol %s redeclared as a token"
msgstr "símbolo %s re-declarado como síbolo"

#: src/symtab.c:309
#, c-format
msgid "symbol %s redeclared as a nonterminal"
msgstr "símbolo %s re-declarado como não-terminal"

#: src/symtab.c:460
#, c-format
msgid "symbol %s redeclared"
msgstr "símbolo %s re-declarado"

#: src/symtab.c:478
#, c-format
msgid "nonterminals cannot be given an explicit number"
msgstr "não-terminais não podem receber um número explicíto"

#: src/symtab.c:481
#, c-format
msgid "redefining user token number of %s"
msgstr "a redefinir número de símbolo do utilizador de %s"

#: src/symtab.c:514
#, c-format
msgid "symbol %s is used, but is not defined as a token and has no rules"
msgstr "símbolo %s usado mas não é definido como símbolo  não tem regras"

#: src/symtab.c:548
#, c-format
msgid "useless %s for type <%s>"
msgstr "%s inútil para tipo <%s>"

#: src/symtab.c:553
#, c-format
msgid "type <%s> is used, but is not associated to any symbol"
msgstr "tipo <%s> usado mas não está associado a nenhum símbolo"

#: src/symtab.c:614
#, c-format
msgid "nonterminals cannot be given a string alias"
msgstr "não terminais não podem receber um aliás de cadeia"

#: src/symtab.c:617
#, c-format
msgid "symbol %s used more than once as a literal string"
msgstr "símbolo %s usado mais de uma vez como cadeia literal"

#: src/symtab.c:620
#, c-format
msgid "symbol %s given more than one literal string"
msgstr "símbolo %s tem mais de uma cadeia literal"

#: src/symtab.c:662
#, c-format
msgid "user token number %d redeclaration for %s"
msgstr "re-declaração de número de símbolo do utilizador %d para %s"

#: src/symtab.c:666
#, c-format
msgid "previous declaration for %s"
msgstr "declaração anterior para %s"

#: src/symtab.c:1033
#, c-format
msgid "the start symbol %s is undefined"
msgstr "símbolo inicial %s indefinido"

#: src/symtab.c:1037
#, c-format
msgid "the start symbol %s is a token"
msgstr "símbolo inicial %s é um símbolo"

#: src/symtab.c:1208
#, c-format
msgid "useless precedence and associativity for %s"
msgstr "precedência e associatividade inúteis para %s"

#: src/symtab.c:1211
#, c-format
msgid "useless precedence for %s"
msgstr "precedência inútil para %s"

#: src/symtab.c:1215
#, c-format
msgid "useless associativity for %s, use %%precedence"
msgstr "associatividade inútil para %s, use %%precedência"

#: lib/argmatch.c:134
#, c-format
msgid "invalid argument %s for %s"
msgstr "argumento %s inválido para %s"

#: lib/argmatch.c:135
#, c-format
msgid "ambiguous argument %s for %s"
msgstr "argumento %s ambíguo para %s"

#: lib/argmatch.c:154
msgid "Valid arguments are:"
msgstr "Argumentos válidos:"

#: lib/closeout.c:122
msgid "write error"
msgstr "erro de escrita"

#: lib/error.c:195
msgid "Unknown system error"
msgstr "Erro de sistema desconhecido"

#: lib/getopt.c:278
#, c-format
msgid "%s: option '%s%s' is ambiguous\n"
msgstr "%s: a opção \"%s%s\" é ambígua\n"

#: lib/getopt.c:284
#, c-format
msgid "%s: option '%s%s' is ambiguous; possibilities:"
msgstr "%s: opção \"%s%s\" é ambígua; possibilidades:"

#: lib/getopt.c:319
#, c-format
msgid "%s: unrecognized option '%s%s'\n"
msgstr "%s: opção não reconhecida \"%s%s\"\n"

#: lib/getopt.c:345
#, c-format
msgid "%s: option '%s%s' doesn't allow an argument\n"
msgstr "%s: opção \"%s%s\" não permite um argumento\n"

#: lib/getopt.c:360
#, c-format
msgid "%s: option '%s%s' requires an argument\n"
msgstr "%s: opção \"%s%s\" requer um argumento\n"

#: lib/getopt.c:621
#, c-format
msgid "%s: invalid option -- '%c'\n"
msgstr "%s: opção inválida -- \"%c\"\n"

#: lib/getopt.c:636 lib/getopt.c:682
#, c-format
msgid "%s: option requires an argument -- '%c'\n"
msgstr "%s: opção requer um argumento -- \"%c\"\n"

#: lib/obstack.c:338 lib/obstack.c:340 lib/xalloc-die.c:34
msgid "memory exhausted"
msgstr "memória esgotada"

#: lib/spawn-pipe.c:141 lib/spawn-pipe.c:144 lib/spawn-pipe.c:265
#: lib/spawn-pipe.c:268
#, c-format
msgid "cannot create pipe"
msgstr "impossível criar pipe"

#: lib/spawn-pipe.c:235 lib/spawn-pipe.c:349 lib/wait-process.c:282
#: lib/wait-process.c:356
#, c-format
msgid "%s subprocess failed"
msgstr "falha no sub-processo %s"

#. TRANSLATORS:
#. Get translations for open and closing quotation marks.
#. The message catalog should translate "`" to a left
#. quotation mark suitable for the locale, and similarly for
#. "'".  For example, a French Unicode local should translate
#. these to U+00AB (LEFT-POINTING DOUBLE ANGLE
#. QUOTATION MARK), and U+00BB (RIGHT-POINTING DOUBLE ANGLE
#. QUOTATION MARK), respectively.
#.
#. If the catalog has no translation, we will try to
#. use Unicode U+2018 (LEFT SINGLE QUOTATION MARK) and
#. Unicode U+2019 (RIGHT SINGLE QUOTATION MARK).  If the
#. current locale is not Unicode, locale_quoting_style
#. will quote 'like this', and clocale_quoting_style will
#. quote "like this".  You should always include translations
#. for "`" and "'" even if U+2018 and U+2019 are appropriate
#. for your locale.
#.
#. If you don't know what to put here, please see
#. <https://en.wikipedia.org/wiki/Quotation_marks_in_other_languages>
#. and use glyphs suitable for your language.
#: lib/quotearg.c:362
msgid "`"
msgstr "\""

#: lib/quotearg.c:363
msgid "'"
msgstr "\""

#: lib/timevar.c:316
msgid "Execution times (seconds)"
msgstr "Tempos de execução (segundos)"

#: lib/timevar.c:318
msgid "CPU user"
msgstr "utilizador de CPU"

#: lib/timevar.c:318
msgid "CPU system"
msgstr "sistema de CPU"

#: lib/timevar.c:318
msgid "wall clock"
msgstr "relógio de parede"

#: lib/w32spawn.h:49
#, c-format
msgid "_open_osfhandle failed"
msgstr "falha em _open_osfhandle"

#: lib/w32spawn.h:90
#, c-format
msgid "cannot restore fd %d: dup2 failed"
msgstr "impossível restaurar fd %d: falha em dup2"

#: lib/wait-process.c:223 lib/wait-process.c:255 lib/wait-process.c:317
#, c-format
msgid "%s subprocess"
msgstr "sub-processo %s"

#: lib/wait-process.c:274 lib/wait-process.c:346
#, c-format
msgid "%s subprocess got fatal signal %d"
msgstr "sub-processo %s obteve um sinal fatal %d"

#~ msgid "symbol %s redefined"
#~ msgstr "símbolo %s redefinido"

#~ msgid "removing of '%s' failed"
#~ msgstr "falha ao remover \"%s\""

#~ msgid "creation of a temporary file failed"
#~ msgstr "falha ao criar ficheiro temporário"

#~ msgid "saving stdin failed"
#~ msgstr "falha ao gravar stdin"

#~ msgid "saving stdout failed"
#~ msgstr "falha ao gravar stdout"

#~ msgid "opening of tmpfile failed"
#~ msgstr "falha ao abrir tmpfile"

#~ msgid "redirecting bison's stdout to the temporary file failed"
#~ msgstr "falha ao redireccionar a stdout do bison para ficheiro temporário"

#~ msgid "redirecting m4's stdin from the temporary file failed"
#~ msgstr ""
#~ "falha ao redireccionar a stdin do m4 a partir de ficheiro temporário"

#~ msgid "opening of a temporary file failed"
#~ msgstr "falha ao abrir ficheiro temporário"

#~ msgid "redirecting m4's stdout to a temporary file failed"
#~ msgstr "falha ao redireccionar a stdout do m4 para ficheiro temporário"

#~ msgid "subsidiary program '%s' interrupted"
#~ msgstr "programa subsidiário \"%s\" interrompido"

#~ msgid "subsidiary program '%s' not found"
#~ msgstr "programa subsidiário \"%s\" não encontrado"

#~ msgid "redirecting bison's stdin from the temporary file failed"
#~ msgstr ""
#~ "falha ao redireccionar a stdin do bison a partir de ficheiro temporário"

#~ msgid "%u bitset_allocs, %u freed (%.2f%%).\n"
#~ msgstr "%u bitset_allocs, %u freed (%.2f%%).\n"

#~ msgid "%u bitset_sets, %u cached (%.2f%%)\n"
#~ msgstr "%u bitset_sets, %u memorizados (%.2f%%)\n"

#~ msgid "%u bitset_resets, %u cached (%.2f%%)\n"
#~ msgstr "%u bitset_resets, %u memorizados (%.2f%%)\n"

#~ msgid "%u bitset_tests, %u cached (%.2f%%)\n"
#~ msgstr "%u bitset_tests, %u memorizados (%.2f%%)\n"

#~ msgid "%u bitset_lists\n"
#~ msgstr "%u bitset_lists\n"

#~ msgid "count log histogram\n"
#~ msgstr "histograma de registo de contagem\n"

#~ msgid "size log histogram\n"
#~ msgstr "histograma de registo de tamanho\n"

#~ msgid "density histogram\n"
#~ msgstr "histograma de densidade\n"

#~ msgid ""
#~ "Bitset statistics:\n"
#~ "\n"
#~ msgstr ""
#~ "Estatísticas de bitset:\n"
#~ "\n"

#~ msgid "Accumulated runs = %u\n"
#~ msgstr "Execuções acumuladas = %u\n"

#~ msgid "cannot read stats file"
#~ msgstr "impossível ler ficheiro de estatísticas"

#~ msgid "bad stats file size\n"
#~ msgstr "tamanho de ficheiro de estatísticas errado\n"

#~ msgid "cannot write stats file"
#~ msgstr "impossível escrever ficheiro de estatísticas"

#~ msgid "cannot open stats file for writing"
#~ msgstr "impossível abrir ficheiro de estatísticas para escrita"

#~ msgid " TOTAL                 :"
#~ msgstr " TOTAL                 :"

#~ msgid "time in %s: %ld.%06ld (%ld%%)\n"
#~ msgstr "tempo em %s: %ld.%06ld (%ld%%)\n"

#~ msgid " type %d is %s\n"
#~ msgstr " tipo %d é %s\n"

#~ msgid "%s: option '--%s' doesn't allow an argument\n"
#~ msgstr "%s: opção \"--%s\" não permite um argumento\n"

#~ msgid "%s: unrecognized option '--%s'\n"
#~ msgstr "%s: opção não reconhecida \"--%s\"\n"

#~ msgid "%s: option '-W %s' doesn't allow an argument\n"
#~ msgstr "%s: opção \"-W %s\" não permite um argumento\n"

#~ msgid "%s: option '-W %s' requires an argument\n"
#~ msgstr "%s: opção \"-W %s\" requer um argumento\n"
