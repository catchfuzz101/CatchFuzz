#[src location]
#bison   -> src
#cflow   -> src
#nasm    -> asm
#dact    -> .
#jasper  -> src/appl
#readelf -> binutils
#objdump -> binutils
#objcopy -> binutils
#nm 	 -> binutils

if [ $# -ne 2 ]
  then
    echo "[*] Usage: ./make_binutils.sh readelf"
    exit 1
fi

rm -rf ./binutils-2.23 || true
tar -xvf binutils-2.23.tar.xz

cd ./binutils-2.23

./configure --disable-werror --disable-silent-rules

make -j 4



cp ../../antifuzz/program/$1/antifuzz/*.c ./binutils



