/* This file is part of GNU cflow
   Copyright (C) 1997-2019 Sergey Poznyakoff
 
   GNU cflow is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
 
   GNU cflow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cflow.h>
unsigned char *start_addr;
unsigned char *end_addr;
int antifuzz_check=0;
int canary_xor;
int fake_arr[1]={0};
#include <argp.h>
#include <stdint.h>
#include <regex.h>
#include <string.h>


#include <argp-version-etc.h>
#include <progname.h>
#include <stdarg.h>
#include <parser.h>
#include <version-etc.h>

const char *argp_program_bug_address = "<" PACKAGE_BUGREPORT ">";
static char doc[] = N_("generate a program flowgraph")
"\v"
N_("* The effect of each option marked with an asterisk is reversed if the option's long name is prefixed with `no-'. For example, --no-cpp cancels --cpp.");
const char *program_authors[] = {
     "Sergey Poznyakoff",
     NULL
};

enum option_code {
     OPT_DEFINES = 256,
     OPT_LEVEL_INDENT,
     OPT_DEBUG,
     OPT_PREPROCESS,
     OPT_NO_PREPROCESS,
     OPT_EMACS,
     OPT_NO_USE_INDENTATION,
     OPT_NO_ANSI,
     OPT_NO_TREE,
     OPT_NO_BRIEF,
     OPT_NO_EMACS,
     OPT_NO_MAIN,
     OPT_NO_VERBOSE,
     OPT_NO_NUMBER,
     OPT_NO_PRINT_LEVEL,
     OPT_NO_REVERSE,
     OPT_OMIT_ARGUMENTS,
     OPT_NO_OMIT_ARGUMENTS,
     OPT_OMIT_SYMBOL_NAMES,
     OPT_NO_OMIT_SYMBOL_NAMES
};

static struct argp_option options[] = {
#define GROUP_ID 0
     { NULL, 0, NULL, 0,
       N_("General options:"), GROUP_ID },
     { "depth", 'd', N_("NUMBER"), 0,
       N_("Set the depth at which the flowgraph is cut off"), GROUP_ID+1 },
     { "include", 'i', N_("CLASSES"), 0,
       N_("Include specified classes of symbols (see below). Prepend CLASSES with ^ or - to exclude them from the output"), GROUP_ID+1 },
     { "format", 'f', N_("NAME"), 0,
       N_("Use given output format NAME. Valid names are `gnu' (default) and `posix'"),
       GROUP_ID+1 },
     { "reverse", 'r', NULL, 0,
       N_("* Print reverse call tree"), GROUP_ID+1 },
     { "xref", 'x', NULL, 0,
       N_("Produce cross-reference listing only"), GROUP_ID+1 },
     { "print", 'P', N_("OPT"), OPTION_HIDDEN,
       N_("Set printing option to OPT. Valid OPT values are: xref (or cross-ref), tree. Any unambiguous abbreviation of the above is also accepted"),
       GROUP_ID+1 },
     { "output", 'o', N_("FILE"), 0,
       N_("Set output file name (default -, meaning stdout)"),
       GROUP_ID+1 },

     { NULL, 0, NULL, 0, N_("Symbols classes for --include argument"), GROUP_ID+2 },
     {"  x", 0, NULL, OPTION_DOC|OPTION_NO_TRANS,
      N_("all data symbols, both external and static"), GROUP_ID+3 },
     {"  _",  0, NULL, OPTION_DOC|OPTION_NO_TRANS,
      N_("symbols whose names begin with an underscore"), GROUP_ID+3 },
     {"  s",  0, NULL, OPTION_DOC|OPTION_NO_TRANS,
      N_("static symbols"), GROUP_ID+3 },
     {"  t",  0, NULL, OPTION_DOC|OPTION_NO_TRANS,
      N_("typedefs (for cross-references only)"), GROUP_ID+3 },

     
#undef GROUP_ID
#define GROUP_ID 10     
     { NULL, 0, NULL, 0,
       N_("Parser control:"), GROUP_ID },
     { "use-indentation", 'S', NULL, 0,
       N_("* Rely on indentation"), GROUP_ID+1 },
     { "no-use-indentation", OPT_NO_USE_INDENTATION, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "ansi", 'a', NULL, 0,
       N_("* Accept only sources in ANSI C"), GROUP_ID+1 },
     { "no-ansi", OPT_NO_ANSI, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "pushdown", 'p', N_("NUMBER"), 0,
       N_("Set initial token stack size to NUMBER"), GROUP_ID+1 },
     { "symbol", 's', N_("SYMBOL:[=]TYPE"), 0,
       N_("Register SYMBOL with given TYPE, or define an alias (if := is used). Valid types are: keyword (or kw), modifier, qualifier, identifier, type, wrapper. Any unambiguous abbreviation of the above is also accepted"), GROUP_ID+1 },
     { "main", 'm', N_("NAME"), 0,
       N_("Assume main function to be called NAME"), GROUP_ID+1 },
     { "no-main", OPT_NO_MAIN, NULL, 0,
       N_("There's no main function; print graphs for all functions in the program") },
     { "define", 'D', N_("NAME[=DEFN]"), 0,
       N_("Predefine NAME as a macro"), GROUP_ID+1 },
     { "undefine", 'U', N_("NAME"), 0,
       N_("Cancel any previous definition of NAME"), GROUP_ID+1 },
     { "include-dir", 'I', N_("DIR"), 0,
       N_("Add the directory DIR to the list of directories to be searched for header files."), GROUP_ID+1 },
     { "preprocess", OPT_PREPROCESS, N_("COMMAND"), OPTION_ARG_OPTIONAL,
       N_("* Run the specified preprocessor command"), GROUP_ID+1 },
     { "cpp", 0, NULL, OPTION_ALIAS, NULL, GROUP_ID+1 },
     { "no-preprocess", OPT_NO_PREPROCESS, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "no-cpp", 0, NULL, OPTION_ALIAS|OPTION_HIDDEN, NULL, GROUP_ID+1 },
#undef GROUP_ID
#define GROUP_ID 20          
     { NULL, 0, NULL, 0,
       N_("Output control:"), GROUP_ID },
     { "all", 'A', NULL, 0,
       N_("Show all functions, not only those reachable from main"),
       GROUP_ID+1 },
     { "number", 'n', NULL, 0,
       N_("* Print line numbers"), GROUP_ID+1 },
     { "no-number", OPT_NO_NUMBER, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "print-level", 'l', NULL, 0,
       N_("* Print nesting level along with the call tree"), GROUP_ID+1 },
     { "no-print-level", OPT_NO_PRINT_LEVEL, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "level-indent", OPT_LEVEL_INDENT, "ELEMENT", 0,
       N_("Control graph appearance"), GROUP_ID+1 },
     { "tree", 'T', NULL, 0,
       N_("* Draw ASCII art tree"), GROUP_ID+1 },
     { "no-tree", OPT_NO_TREE, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "brief", 'b', NULL, 0,
       N_("* Brief output"), GROUP_ID+1 },
     { "no-brief", OPT_NO_BRIEF, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "emacs", OPT_EMACS, NULL, 0,
       N_("* Additionally format output for use with GNU Emacs"), GROUP_ID+1 },
     { "no-emacs", OPT_NO_EMACS, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "omit-arguments", OPT_OMIT_ARGUMENTS, NULL, 0,
       N_("* Do not print argument lists in function declarations"), GROUP_ID+1 },
     { "no-ignore-arguments", OPT_NO_OMIT_ARGUMENTS, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "omit-symbol-names", OPT_OMIT_SYMBOL_NAMES, NULL, 0,
       N_("* Do not print symbol names in declaration strings"), GROUP_ID+1 },
     { "no-omit-symbol-names", OPT_NO_OMIT_SYMBOL_NAMES, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
#undef GROUP_ID
#define GROUP_ID 30                 
     { NULL, 0, NULL, 0,
       N_("Informational options:"), GROUP_ID },
     { "verbose", 'v', NULL, 0,
       N_("* Verbose error diagnostics"), GROUP_ID+1 },
     { "no-verbose", OPT_NO_VERBOSE, NULL, OPTION_HIDDEN,
       "", GROUP_ID+1 },
     { "debug", OPT_DEBUG, "NUMBER", OPTION_ARG_OPTIONAL,
       N_("Set debugging level"), GROUP_ID+1 },
#undef GROUP_ID     
     { 0, }
};

/* Structure representing various arguments of command line options */
struct option_type {
    char *str;           /* optarg value */
    int min_match;       /* minimal number of characters to match */
    int type;            /* data associated with the arg */
};

int debug;              /* debug level */
char *outname = "-";    /* default output file name */
int print_option = 0;   /* what to print. */
int verbose;            /* be verbose on output */
int use_indentation;    /* Rely on indentation,
			 * i.e. suppose the function body
                         * is necessarily surrounded by the curly braces
			 * in the first column
                         */
int record_defines;     /* Record macro definitions */
int strict_ansi;        /* Assume sources to be written in ANSI C */
int print_line_numbers; /* Print line numbers */
int print_levels;       /* Print level number near every branch */
int print_as_tree;      /* Print as tree */
int brief_listing;      /* Produce short listing */
int reverse_tree;       /* Generate reverse tree */
int max_depth;          /* The depth at which the flowgraph is cut off */
int emacs_option;       /* Format and check for use with Emacs cflow-mode */ 
int omit_arguments_option;    /* Omit arguments from function declaration string */
int omit_symbol_names_option; /* Omit symbol name from symbol declaration string */

#define SM_FUNCTIONS   0x0001
#define SM_DATA        0x0002
#define SM_STATIC      0x0004
#define SM_UNDERSCORE  0x0008
#define SM_TYPEDEF     0x0010
#define SM_UNDEFINED   0x0020

#define CHAR_TO_SM(c) ((c)=='x' ? SM_DATA : \
                        (c)=='_' ? SM_UNDERSCORE : \
                         (c)=='s' ? SM_STATIC : \
                          (c)=='t' ? SM_TYPEDEF : \
                           (c)=='u' ? SM_UNDEFINED : 0)
#define SYMBOL_INCLUDE(c) (symbol_map |= CHAR_TO_SM(c))
#define SYMBOL_EXCLUDE(c) (symbol_map &= ~CHAR_TO_SM(c))
int symbol_map;  /* A bitmap of symbols included in the graph. */

char *level_indent[] = { NULL, NULL };
char *level_end[] = { "", "" };
char *level_begin = "";

int preprocess_option = 0; /* Do they want to preprocess sources? */

char *start_name = "main"; /* Name of start symbol */
int all_functions;  
struct linked_list *arglist;        /* List of command line arguments */

/* Given the option_type array and (possibly abbreviated) option argument
 * find the type corresponding to that argument.
 * Return 0 if the argument does not match any one of OPTYPE entries
 */
static int
find_option_type(struct option_type *optype, const char *str, int len)
{
     if (len == 0)
	  len = strlen(str);
     for ( ; optype->str; optype++) {
	  if (len >= optype->min_match &&
	      memcmp(str, optype->str, len) == 0) {
	       return optype->type;
	  }
     }
     return 0;
}

/* Args for --symbol option */
static struct option_type symbol_optype[] = {
     { "keyword", 2, WORD },
     { "kw", 2, WORD },
     { "modifier", 1, MODIFIER },
     { "identifier", 1, IDENTIFIER },
     { "type", 1, TYPE },
     { "wrapper", 1, PARM_WRAPPER },
     { "qualifier", 1, QUALIFIER },
     { 0 },
};

/* Parse the string STR and store the symbol in the temporary symbol table.
 * STR is the string of form: NAME:TYPE
 * NAME means symbol name, TYPE means symbol type (possibly abbreviated)
 */
static void
symbol_override(const char *str)
{
     const char *ptr;
     char *name;
     Symbol *sp;
     
     ptr = strchr(str, ':');
     if (!ptr)
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("%s: no symbol type supplied"), str);}
     else {
	  name = strndup(str, ptr - str);
	  if (ptr[1] == '=') {
	       Symbol *alias = lookup(ptr+2);
	       if (!alias) {
		    alias = install(xstrdup(ptr+2), INSTALL_OVERWRITE);
		    alias->type = SymToken;
		    alias->token_type = 0;
		    alias->source = NULL;
		    alias->def_line = -1;
		    alias->ref_line = NULL;
	       }
	       sp = install(name, INSTALL_OVERWRITE);
	       sp->type = SymToken;
	       sp->alias = alias;
	       sp->flag = symbol_alias;
	  } else {
	       int type = find_option_type(symbol_optype, ptr+1, 0);
	       if (type == 0)
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("unknown symbol type: %s"), ptr+1);}
	       sp = install(name, INSTALL_OVERWRITE);
	       sp->type = SymToken;
	       sp->token_type = type;
	  }
	  sp->source = NULL;
	  sp->def_line = -1;
	  sp->ref_line = NULL;
     }
}

/* Args for --print option */
static struct option_type print_optype[] = {
     { "xref", 1, PRINT_XREF },
     { "cross-ref", 1, PRINT_XREF },
     { "tree", 1, PRINT_TREE },
     { 0 },
};

static void
set_print_option(char *str)
{
     int opt;
     
     opt = find_option_type(print_optype, str, 0);
     if (opt == 0) {
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("unknown print option: %s"), str);}
	  return;
     }
     print_option |= opt;
}

/* Convert first COUNT bytes of the string pointed to by STR_PTR
 * to integer using BASE. Move STR_PTR to the point where the
 * conversion stopped.
 * Return the number obtained.
 */
static int
number(const char **str_ptr, int base, int count)
{
     int  c, n;
     unsigned i;
     const char *str = *str_ptr;
     
     for (n = 0; *str && count; count--) {
	  c = *str++;
	  if (isdigit(c))
	       i = c - '0';
	  else
	       i = toupper(c) - 'A' + 10;
	  if (i > base) {
	       break;
	  }
	  n = n * base + i;
     }
     *str_ptr = str - 1;
     return n;
}

/* Processing for --level option
 * The option syntax is
 *    --level NUMBER
 * or
 *    --level KEYWORD=STR
 * where
 *    KEYWORD is one of "begin", "0", ", "1", "end0", "end1",
 *    or an abbreviation thereof,
 *    STR is the value to be assigned to the parameter.
 *  
 * STR can contain usual C escape sequences plus \e meaning '\033'.
 * Apart from this any character followed by xN suffix (where N is
 * a decimal number) is expanded to the sequence of N such characters.
 * 'x' looses its special meaning at the start of the string.
 */
#define MAXLEVELINDENT 216
#define LEVEL_BEGIN 1
#define LEVEL_INDENT0 2
#define LEVEL_INDENT1 3
#define LEVEL_END0 4
#define LEVEL_END1 5

static struct option_type level_indent_optype[] = {
     { "begin", 1, LEVEL_BEGIN },
     { "start", 1, LEVEL_BEGIN },
     { "0", 1, LEVEL_INDENT0 },
     { "1", 1, LEVEL_INDENT1 },
     { "end0", 4, LEVEL_END0 },
     { "end1", 4, LEVEL_END1 },
};

static void
parse_level_string(const char *str, char **return_ptr)
{
     static char text[MAXLEVELINDENT];
     char *p;
     int i, c, num;
    
     p = text;
     memset(text, ' ', sizeof(text));
     text[sizeof(text)-1] = 0;
     
     while (*str) {
	  switch (*str) {
	  case '\\':
	       switch (*++str) {
	       case 'a':
		    *p++ = '\a';
		    break;
	       case 'b':
		    *p++ = '\b';
		    break;
	       case 'e':
		    *p++ = '\033';
		    break;
	       case 'f':
		    *p++ = '\f';
		    break;
	       case 'n':
		    *p++ = '\n';
		    break;
	       case 'r':
		    *p++ = '\r';
		    break;
	       case 't':
		    *p++ = '\t';
		    break;
	       case 'x':
	       case 'X':
		    ++str;
		    *p++ = number(&str,16,2);
		    break;
	       case '0':
		    ++str;
		    *p++ = number(&str,8,3);
		    break;
	       default:
		    *p++ = *str;
	       }
	       ++str;
	       break;
	  case 'x':
	       if (p == text) {
		    goto copy;
	       }
	       num = strtol(str+1, (char**)&str, 10);
	       c = p[-1];
	       for (i = 1; i < num; i++) {
		    *p++ = c;
		    if (*p == 0)
			  {if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0,
			       _("level indent string is too long"));}
	       }
	       break;
	  default:
	  copy:
	       *p++ = *str++;
	       if (*p == 0)
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("level indent string is too long"));}
	  }
     }
     *p = 0;
     *return_ptr = strdup(text);
}

static void
set_level_indent(const char *str)
{
     long n;
     const char *p;
     char *q;
     
     n = strtol(str, &q, 0);
     if (*q == 0 && n > 0) {
	  char *s = xmalloc(n+1);
	  memset(s, ' ', n-1);
	  s[n-1] = 0;
	  level_indent[0] = level_indent[1] = s;
	  return;
     }
     
     p = str;
     while (*p != '=') {
	  if (*p == 0)
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("level-indent syntax"));}
	  p++;
     }
     ++p;
    
     switch (find_option_type(level_indent_optype, str, p - str - 1)) {
     case LEVEL_BEGIN:
	  parse_level_string(p, &level_begin);
	  break;
     case LEVEL_INDENT0:
	  parse_level_string(p, &level_indent[0]);
	  break;
     case LEVEL_INDENT1:
	  parse_level_string(p, &level_indent[1]);
	  break;
     case LEVEL_END0:
	  parse_level_string(p, &level_end[0]);
	  break;
     case LEVEL_END1:
	  parse_level_string(p, &level_end[1]);
	  break;
     default:
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("unknown level indent option: %s"), str);}
     }
}

static void
add_name(const char *name)
{
     linked_list_append(&arglist, (void*) name);
}

static void
add_preproc_option(int key, const char *arg)
{
     char *opt = xmalloc(3 + strlen(arg));
     sprintf(opt, "-%c%s", key, arg);
     add_name(opt);
     preprocess_option = 1;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
     int num;
     
     switch (key) {
     case 'A':
	  all_functions++;
	  break;
     case 'a':
	  strict_ansi = 1;
	  break;
     case OPT_NO_ANSI:
	  strict_ansi = 0;
	  break;
     case OPT_DEBUG:
	  debug = arg ? atoi(arg) : 1;
	  break;
     case 'P':
	  set_print_option(arg);
	  break;
     case 'S':
	  use_indentation = 1;
	  break;
     case OPT_NO_USE_INDENTATION:
	  use_indentation = 0;
	  break;
     case 'T':
	  print_as_tree = 1;
	  set_level_indent("0=  "); /* two spaces */
	  set_level_indent("1=| ");
	  set_level_indent("end0=+-");
	  set_level_indent("end1=\\\\-");
	  break;
     case OPT_NO_TREE:
	  print_as_tree = 0;
	  level_indent[0] = level_indent[1] = NULL;
	  level_end[0] = level_end[1] = NULL;
	  break;
     case 'b':
	  brief_listing = 1;
	  break;
     case OPT_NO_BRIEF:
	  brief_listing = 0;
	  break;
     case 'd':
	  max_depth = atoi(arg);
	  if (max_depth < 0)
	       max_depth = 0;
	  break;
     case OPT_DEFINES: /* FIXME: Not used. */
	  record_defines = 1;
	  break;
     case OPT_EMACS:
	  emacs_option = 1;
	  break;
     case OPT_NO_EMACS:
	  emacs_option = 0;
	  break;
     case 'f':
	  if (select_output_driver(arg))
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("%s: No such output driver"), optarg);}
	  output_init();
	  break;
     case OPT_LEVEL_INDENT:
	  set_level_indent(arg);
	  break;
     case 'i':
	  num = 1;
	  for (; *arg; arg++) 
	       switch (*arg) {
	       case '-':
	       case '^':
		    num = 0;
		    break;
	       case '+':
		    num = 1;
		    break;
	       case 'x':
	       case '_':
	       case 's':
	       case 't':
	       case 'u':
		    if (num)
			 SYMBOL_INCLUDE(*arg);
		    else
			 SYMBOL_EXCLUDE(*arg);
		    break;
	       default:
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("Unknown symbol class: %c"), *arg);}
	       }
	  break;
     case OPT_OMIT_ARGUMENTS:
	  omit_arguments_option = 1;
	  break;
     case OPT_NO_OMIT_ARGUMENTS:
	  omit_arguments_option = 0;
	  break;
     case OPT_OMIT_SYMBOL_NAMES:
	  omit_symbol_names_option = 1;
	  break;
     case OPT_NO_OMIT_SYMBOL_NAMES:
	  omit_symbol_names_option = 0;
	  break;
     case 'l':
	  print_levels = 1;
	  break;
     case OPT_NO_PRINT_LEVEL:
	  print_levels = 0;
	  break;
     case 'm':
	  start_name = strdup(arg);
	  break;
     case OPT_NO_MAIN:
	  start_name = NULL;
	  break;
     case 'n':
	  print_line_numbers = 1;
	  break;
     case OPT_NO_NUMBER:
	  print_line_numbers = 0;
	  break;
     case 'o':
	  outname = strdup(arg);
	  break;
     case 'p':
	  num = atoi(arg);
	  if (num > 0)
	       token_stack_length = num;
	  break;
     case 'r':
	  reverse_tree = 1;
	  break;
     case OPT_NO_REVERSE:
	  reverse_tree = 0;
	  break;
     case 's':
	  symbol_override(arg);
	  break;
     case 'v':
	  verbose = 1;
	  break;
     case OPT_NO_VERBOSE:
	  verbose = 0;
	  break;
     case 'x':
	  print_option = PRINT_XREF;
	  SYMBOL_EXCLUDE('s'); /* Exclude static symbols by default */
	  break;
     case OPT_PREPROCESS:
	  preprocess_option = 1;
	  set_preprocessor(arg ? arg : CFLOW_PREPROC);
	  break;
     case OPT_NO_PREPROCESS:
	  preprocess_option = 0;
	  break;
     case ARGP_KEY_ARG:
	  add_name(arg);
	  break;
     case 'I':
     case 'D':
     case 'U':
	  add_preproc_option(key, arg);
	  break;
     default:
	  return ARGP_ERR_UNKNOWN;
     }
     return 0;
}

static struct argp argp = {
     options,
     parse_opt,
     N_("[FILE]..."),
     doc,
     NULL,
     NULL,
     NULL
};

int
globals_only()
{
     return !(symbol_map & SM_STATIC);
}

int
include_symbol(Symbol *sym)
{
     int type = 0;
     
     if (!sym)
	  return 0;
     
     if (sym->type == SymIdentifier) {
	  if (sym->name[0] == '_' && !(symbol_map & SM_UNDERSCORE))
	       return 0;

	  if (sym->storage == StaticStorage)
	       type |= SM_STATIC;
	  if (sym->arity == -1 && sym->storage != AutoStorage)
	       type |= SM_DATA;
	  else if (sym->arity >= 0)
	       type |= SM_FUNCTIONS;

	  if (!sym->source)
	       type |= SM_UNDEFINED;
	  
     } else if (sym->type == SymToken) {
	  if (sym->token_type == TYPE && sym->source)
	       type |= SM_TYPEDEF;
	  else
	       return 0;
     }
     return (symbol_map & type) == type;
}

void
xalloc_die(void)
{
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_FATAL, ENOMEM, _("Exiting"));}
     abort();
}

void
init()
{
     if (level_indent[0] == NULL) 
	  level_indent[0] = "    "; /* 4 spaces */
     if (level_indent[1] == NULL)
	  level_indent[1] = level_indent[0];
     if (level_end[0] == NULL)
	  level_end[0] = "";
     if (level_end[1] == NULL)
	  level_end[1] = "";
     
     init_lex(debug > 2);
     init_parse();
}

const char version_etc_copyright[] =
  /* Do *not* mark this string for translation.  %s is a copyright
     symbol suitable for this locale, and %d is the copyright
     year.  */
  "Copyright %s 2005-%d Sergey Poznyakoff";

int w0(unsigned char* buf, unsigned int len){
	printf("w0 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 0 % len;
    int index2 = 1 % len;
    int index3 = 2 % len;
    int index4 = 3 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3902515186) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1507252891) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 603338905) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1696904468) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1065828383) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3048701182) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1392146492) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3902515186) {
        ret = 12;
      }
    }
    if(buf[index1] > 56) {
      if(buf[index2] < 118) {
        if(buf[index3] - buf[index4] < 56) {
          if(buf[index3] + buf[index4] > 118) {
            if(((buf[index1] + buf[index2]) % 9) == 0) {
              if(((buf[index1] * buf[index2]) % 5) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 56) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }

  return ret;
}

int w1(unsigned char* buf, unsigned int len){
  printf("w1 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 1 % len;
    int index2 = 2 % len;
    int index3 = 3 % len;
    int index4 = 4 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3649224605) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2350045607) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3014389030) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2122141901) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1860222409) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2127411081) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3096905285) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3649224605) {
        ret = 12;
      }
    }
    if(buf[index1] > 209) {
      if(buf[index2] < 215) {
        if(buf[index3] - buf[index4] < 209) {
          if(buf[index3] + buf[index4] > 215) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 209) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w2(unsigned char* buf, unsigned int len){
	printf("w2 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 2 % len;
    int index2 = 3 % len;
    int index3 = 4 % len;
    int index4 = 5 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 3060206730) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1823329745) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1798358539) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1130645893) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 334868504) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1095408358) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2855184194) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 3060206730) {
        ret = 12;
      }
    }
    if(buf[index1] > 236) {
      if(buf[index2] < 125) {
        if(buf[index3] - buf[index4] < 236) {
          if(buf[index3] + buf[index4] > 125) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  64) {
                  if((buf[index1] ^ buf[index2]) == 236) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w3(unsigned char* buf, unsigned int len){
	  printf("w3 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 3 % len;
    int index2 = 4 % len;
    int index3 = 5 % len;
    int index4 = 6 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1306562184) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2309250971) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 702642080) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3193801888) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1618453275) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3908944833) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1426801106) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1306562184) {
        ret = 12;
      }
    }
    if(buf[index1] > 33) {
      if(buf[index2] < 13) {
        if(buf[index3] - buf[index4] < 33) {
          if(buf[index3] + buf[index4] > 13) {
            if(((buf[index1] + buf[index2]) % 6) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 33) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w4(unsigned char* buf, unsigned int len){
	  printf("w4 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 4 % len;
    int index2 = 5 % len;
    int index3 = 6 % len;
    int index4 = 7 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1317201472) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 4182791665) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1712271078) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3769200889) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3974150540) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1676806712) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 503006531) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1317201472) {
        ret = 12;
      }
    }
    if(buf[index1] > 225) {
      if(buf[index2] < 134) {
        if(buf[index3] - buf[index4] < 225) {
          if(buf[index3] + buf[index4] > 134) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 225) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w5(unsigned char* buf, unsigned int len){
	  printf("w5 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 5 % len;
    int index2 = 6 % len;
    int index3 = 7 % len;
    int index4 = 8 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 2680581394) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798508734) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1827993300) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 713768984) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4009010704) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1285272007) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4015010868) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 2680581394) {
        ret = 12;
      }
    }
    if(buf[index1] > 221) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 221) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 4) == 0) {
              if(((buf[index1] * buf[index2]) % 6) == 0) {
                if((buf[index1] ^ buf[index2]) &  4) {
                  if((buf[index1] ^ buf[index2]) == 221) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w6(unsigned char* buf, unsigned int len){
	  printf("w6 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 6 % len;
    int index2 = 7 % len;
    int index3 = 8 % len;
    int index4 = 9 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1617115864) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 1631690012) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3460463880) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3382779234) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1649578633) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 728322968) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2998716203) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1617115864) {
        ret = 12;
      }
    }
    if(buf[index1] > 132) {
      if(buf[index2] < 208) {
        if(buf[index3] - buf[index4] < 132) {
          if(buf[index3] + buf[index4] > 208) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 4) == 0) {
                if((buf[index1] ^ buf[index2]) &  32) {
                  if((buf[index1] ^ buf[index2]) == 132) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w7(unsigned char* buf, unsigned int len){
	  printf("w7 START\n");
  int ret = 0;
  if(len >= 1) {
    int index1 = 7 % len;
    int index2 = 8 % len;
    int index3 = 9 % len;
    int index4 = 10 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 1208351771) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2227323053) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 1104939852) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 4258576467) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 780873126) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2155013799) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1158125450) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 1208351771) {
        ret = 12;
      }
    }
    if(buf[index1] > 18) {
      if(buf[index2] < 181) {
        if(buf[index3] - buf[index4] < 18) {
          if(buf[index3] + buf[index4] > 181) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 2) == 0) {
                if((buf[index1] ^ buf[index2]) &  1) {
                  if((buf[index1] ^ buf[index2]) == 18) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w8(unsigned char* buf, unsigned int len){
	  printf("w8 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 8 % len;
    int index2 = 9 % len;
    int index3 = 10 % len;
    int index4 = 11 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 323927678) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 798644434) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 2384399227) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 361926757) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 897612148) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 990255627) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 3311835330) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 323927678) {
        ret = 12;
      }
    }
    if(buf[index1] > 100) {
      if(buf[index2] < 196) {
        if(buf[index3] - buf[index4] < 100) {
          if(buf[index3] + buf[index4] > 196) {
            if(((buf[index1] + buf[index2]) % 2) == 0) {
              if(((buf[index1] * buf[index2]) % 7) == 0) {
                if((buf[index1] ^ buf[index2]) &  16) {
                  if((buf[index1] ^ buf[index2]) == 100) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}

int w9(unsigned char* buf, unsigned int len){
	  printf("w9 START\n");

  int ret = 0;
  if(len >= 1) {
    int index1 = 9 % len;
    int index2 = 10 % len;
    int index3 = 11 % len;
    int index4 = 12 % len;
    uint32_t buf32 = ((unsigned char)buf[index1] << 24) + ((unsigned char)buf[index2] << 16) + \
             ((unsigned char)buf[index3] << 8) + (unsigned char)buf[index4];
    //printf("comparing %x\n", buf32);
    if(buf32 == 720174660) {
      ret = 9;
    } else {
      ret = -9;
    }
    if(buf32 == 2124116700) {
      ret = 10;
    } else {
      ret = -10;
    }
    if(buf32 == 3937385190) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1443256077) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1256522327) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 1727523764) {
      ret = 11;
    } else {
      ret = -11;
    }
    if(buf32 == 2994088857) {
      ret = 11;
    } else {
      ret = -11;
    }

    for(int i = 0; i < 100; i++) {
      if(buf32 == 720174660) {
        ret = 12;
      }
    }
    if(buf[index1] > 249) {
      if(buf[index2] < 19) {
        if(buf[index3] - buf[index4] < 249) {
          if(buf[index3] + buf[index4] > 19) {
            if(((buf[index1] + buf[index2]) % 5) == 0) {
              if(((buf[index1] * buf[index2]) % 8) == 0) {
                if((buf[index1] ^ buf[index2]) &  8) {
                  if((buf[index1] ^ buf[index2]) == 249) {
                    ret = buf[index1] + buf[index2]; 
                  } else {
                    ret = 8;
                  }
                } else {
                  ret = 7;
                }
              } else {
                ret = 6;
              }
            } else {
              ret = 5;
            }
          } else {
            ret = 4;
          }
        } else {
          ret = 3;
        }
      } else {
        ret = 2;
      }
    } else {
      ret = 1;  
    }
  }
  return ret;
}



typedef int (*obfuscation_call_heavyweight)(unsigned char* buf, unsigned int len);
obfuscation_call_heavyweight functions_array[10] = {
  
  w0,
  
  w1,
  
  w2,
  
  w3,
  
  w4,
  
  w5,
  
  w6,
  
  w7,
  
  w8,
  
  w9
};

//get the distance between two points in 1D
float getDist(float p1, float p2) {
	float dist; /*distance between two points*/

	dist = fabsf(p1 - p2); /*in 1D case, that distance would just be the absolute value of the subtraction of two values*/

	return dist;
}

/*get the nearest distance from a given point to other centers
 *return the label number the nearest center
 */
int getNearestDist(float array[], int p, int k) {
	float nearestDist = 300; /*in this particular data set, we're deal with percentage, so the largest distance between two point would be 1*/
	float dist; /*distance between two points*/
	int i; /*counter and index*/
	int label=-1; /*label number of each cluster*/

	//compare the distance between the given point and each center
	for (i = 0; i < k; i++) {
		dist = getDist(array[i], p);
		//find the closest center and update the label to be the label number of this cluster
		if (dist < nearestDist) {
			nearestDist = dist;
			label = i;
		}
	}

	return label;
}

/*get the mean of each cluster
 *set that value as new center
 */
float getMean(unsigned int vArray[], int lArray[], int label, int size) {
	float sum = 0.0; /*sum of distances from center to all points in this cluster*/
	int counter = 0;
	float mean;
	int i = 0;

	for (; i < size; i++) {
		if (lArray[i] == label) {
			sum += vArray[i];
			counter++;
		}
	}

	if (counter != 0) {
		mean = sum / (float)counter;
	}
	else mean = -1;

	return mean;
}


void antifuzz(void){

	#define MAX_FILE_CONTENT_SIZE 512
	unsigned char* fileContentMult = (unsigned char*)"abcdefghijklmnopqrstuvwxyz";
	unsigned int x[0xFFFFF]={0x00,};
	unsigned int y[0xFFFFF]={0x00,};


	char pid_mem_file[50]={0x00,};
	char pid_mem_file_name[50]={0x00,};
	//char shm_name[50]={0x00,};
	//char mem_result[50]={0x00,};
	//char mem_result_after[50]={0x00,};
	//char data_tmp[50]={0x00,};
	


  pid_t pid=getpid();
  sprintf(pid_mem_file, "/proc/%d/maps", pid);
	sprintf(pid_mem_file_name, "./maps_%d", pid);
	//sprintf(shm_name, "./shm/%d", pid);
	//sprintf(mem_result, "./mem_result/%d", pid);
  //sprintf(mem_result_after, "./mem_result_after/%d", pid);
  //sprintf(data_tmp, "./data.txt");
  int canary_index, canary_val;
  while(1){
  	int canary_index_tmp=((sizeof(fake_arr)/sizeof(int)-3)/2)+1;
  	if(canary_index_tmp)canary_index=rand()%canary_index_tmp;
        else{
            fake_arr[0]=1;
            break;
        }
        canary_val=rand()%1000;
	if(fake_arr[canary_index]!=canary_val){
		fake_arr[canary_index]=canary_val;
		break;
	}
  }
  //printf("%s\n", pid_mem_file);
  FILE *fp1  = fopen(pid_mem_file, "r");
	FILE *fp2  = fopen(pid_mem_file_name, "w");
	//FILE *fp3  = fopen(shm_name, "w");
	//FILE *fp4  = fopen(mem_result, "w");
	//FILE *fp5  = fopen(mem_result_after, "w");
	//FILE *fp6  = fopen(data_tmp, "w");
  
	char buffer[128];
	regex_t state;
  regmatch_t pmatch[3];

	char addr1[18];
  char addr2[18];
  const char *pattern = "[rwx-]{3}+[s]";
  const char *addr = "([0-9a-fA-F]{12,16})-([0-9a-fA-F]{12,16})";
  strncpy(addr1,"0x",2);
  strncpy(addr2,"0x",2);

  memset(addr1+2, 0x00, sizeof(addr1)-2);
  memset(addr2+2, 0x00, sizeof(addr2)-2);

	regcomp(&state, pattern, REG_EXTENDED);
  int index;
  while (fgets(buffer, 128, fp1) != NULL)
  {	
      fprintf(fp2, "%s", buffer);
			int status = regexec(&state, buffer, 1, pmatch, 0);
			if(status==0){
	    	//fprintf(fp3,"With the whole expression, "
	           // "a matched substring \"%.*s\" is found at position %d to %d.\n",
	            //pmatch[0].rm_eo - pmatch[0].rm_so, &buffer[pmatch[0].rm_so],
	           // pmatch[0].rm_so, pmatch[0].rm_eo - 1);
      	regfree(&state);
      	regcomp(&state, addr, REG_EXTENDED);
      	status = regexec(&state, buffer, 3, pmatch, 0);
      	if(status==0){
					//fprintf(fp3, "With the whole expression, "
					//		"a matched substring \"%.*s\" is found at position %d to %d.\n",
					//		pmatch[0].rm_eo - pmatch[0].rm_so, &buffer[pmatch[0].rm_so],
					//		pmatch[0].rm_so, pmatch[0].rm_eo - 1);
					
        	strncpy(addr1+2,buffer+pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
        	strncpy(addr2+2,buffer+pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
		

        	//fprintf(fp3,"%s\n", addr1);
        	//fprintf(fp3,"%s\n", addr2);
        	start_addr=(unsigned char*)strtoll(addr1,NULL,16);
        	end_addr=(unsigned char*)strtoll(addr2,NULL,16);
        	//fprintf(fp3,"%lld\n",(long long int)start_addr);
        	//fprintf(fp3,"%lld\n",(long long int)end_addr);
        	//fprintf(fp3,"%d\n", start_addr[0]);

			
					for (int i = 0; i < end_addr-start_addr; i++) {
						x[i]=i;
						y[i] = start_addr[i];
					}
					//fprintf(fp4, "SHM_SIZE: %d\n", end_addr-start_addr);
					//fprintf(fp6, "SHM_SIZE: %d\n", end_addr-start_addr);
					//for(int i=0;i<end_addr-start_addr;i++){
					//	fprintf(fp4, "(%d, %d)\n",i,y[i]);
					//}

    
		      for(int i = 0; i <130; i++) {
		        functions_array[0](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i <110; i++) {
		        functions_array[1](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i < 90; i++) {
		        functions_array[2](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i < 70; i++) {
		        functions_array[3](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }
		      for(int i = 0; i < 50; i++) {
		        functions_array[4](fileContentMult, MAX_FILE_CONTENT_SIZE);
		      }

		      int k=0;
		      for (int i = 0; i < end_addr-start_addr; i++){
						x[i]=i;
					  int tmp_value=start_addr[i]-y[i];
					  if(tmp_value>30 && tmp_value<150)y[k++]=tmp_value;
					}

					//fprintf(fp5, "SHM_SIZE: %d\n", end_addr-start_addr);
					//for(int i=0;i<end_addr-start_addr;i++){
					//	fprintf(fp5, "(%d, %d)\n",i,y[i]);
					//}

					
					//for(int i=0;i<k-1;i++){
					//	fprintf(fp6, "%d, ", y[i]);
					//}
					//fprintf(fp6, "%d\nNUM: %d\n", y[k-1],k);
		      int labelArray[k];
		      int orig_centerArray[5] = { 50,70,90,110,130 };
		      float centerArray[5] = { 50,70,90,110,130 };/* store the mean value of each cluster at corresponding location */
		      float tmp_centerArray[5] = { 0, };
      

		      int loop = 0;
		      while(1) {

		        loop++;
		        //write in label for each data point
		        for (int i = 0; i < k; i++) {
		          int label = getNearestDist(centerArray, y[i], 5);
		          labelArray[i] = label;
		       	}

		        /*calculate the mean value of each cluster
		        *set it as the new center
		        */
		        int finish = 0;

		        for (int i = 0; i < 5; i++) {
		          tmp_centerArray[i] = centerArray[i];
		          centerArray[i] = getMean(y, labelArray, i, k);
		          if (tmp_centerArray[i] == centerArray[i]) {
		            finish += 1;
		          }
		          
          	//printf("new center is %f\n", centerArray[i]);
        		}
		        if (finish == 5) {
		          //fprintf(fp6, "%d DONE\n", loop);
		          break;
		        }
      		}

		      float distance = 0;
		      int tmp_index=5;
		      for (int i = 0; i < tmp_index; i++) {
		        //fprintf(fp6, "center %d  is %f\n", i, centerArray[i]);
		        if (centerArray[i] == -1)tmp_index -= 1;
		        else distance+=(centerArray[i] - orig_centerArray[i])*(centerArray[i]-orig_centerArray[i]);
		      }
		      distance /=tmp_index;
		      //fprintf(fp6, "distance is %f\n", distance);

					if(distance<10){
						//fprintf(fp6, "===FUZZ DETECTED===ANTIFUZZ START===\n");					
						antifuzz_check=1;
						for (int i = 0; i < end_addr-start_addr; i++) {
						  start_addr[i]=0x80;
						}
					}
      		//else fprintf(fp6, "FUZZ NOT DETECTED!\n");
      
					break;
     	}
   	}
 	}
	regfree(&state);

  
	
  fclose(fp1);
	fclose(fp2);
	//fclose(fp3);
	//fclose(fp4);
  	//fclose(fp5);
	//fclose(fp6);
 
  unlink(pid_mem_file_name);
	//unlink(shm_name);
	//unlink(mem_result);
	//unlink(mem_result_after);

}

int
main(int argc, char **argv)
{
	antifuzz();
     int index;
     int status = EX_OK;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
     
     set_program_name(argv[0]);
     argp_version_setup("cflow", program_authors);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
     
     setlocale(LC_ALL, "");
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
     bindtextdomain(PACKAGE, LOCALEDIR);
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);
     textdomain(PACKAGE);
     
     register_output("gnu", gnu_output_handler, NULL);
     register_output("posix", posix_output_handler, NULL);

     symbol_map = SM_FUNCTIONS|SM_STATIC|SM_UNDEFINED;
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

     if (getenv("POSIXLY_CORRECT")) {
	  if (select_output_driver("posix")) {
	        {if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(0, 0, _("INTERNAL ERROR: %s: No such output driver"),
		     "posix");}
	       abort();
	  }
	  output_init();
     }
     
     sourcerc(&argc, &argv);
     if (argp_parse(&argp, argc, argv, ARGP_IN_ORDER, &index, NULL))
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;exit(EX_USAGE);}

     if (print_option == 0)
	  print_option = PRINT_TREE;

     init();
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

     if (arglist) {
	  struct linked_list_entry *p;
	  /* First pass: collect options */
	  for (p = linked_list_head(arglist); p; ) {
	       struct linked_list_entry *next = p->next;
	       char *s = (char*)p->data;
	       if (s[0] == '-') {
		    pp_option(s);
		    linked_list_unlink(arglist, p);
	       }
	       p = next;
	  }
	  /* Second pass: collect sources */
	  for (p = linked_list_head(arglist); p; p = p->next) {
	       char *s = (char*)p->data;
	       if (source(s) == 0)
		    yyparse();
	  }
     }
     
     argc -= index;
     argv += index;

     while (argc--) {
	  if (source(*argv++) == 0)
	       yyparse();
	  else
	       status = EX_SOFT;
     }

     if (input_file_count == 0)
{if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;error(EX_USAGE, 0, _("no input files"));}
	canary_xor=0;
	for(int i=0;i<sizeof(fake_arr)/sizeof(int);i++)canary_xor^=fake_arr[i];
	if(canary_xor==0)exit(-1);

     output();
     {if(antifuzz_check)for(int i=0;i<end_addr-start_addr;i++)start_addr[i]=0x80;return status;}
}







