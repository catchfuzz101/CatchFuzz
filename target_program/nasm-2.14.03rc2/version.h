#ifndef NASM_VERSION_H
#define NASM_VERSION_H
#define NASM_MAJOR_VER      2
#define NASM_MINOR_VER      14
#define NASM_SUBMINOR_VER   2
#define NASM_PATCHLEVEL_VER 92
#define NASM_VERSION_ID     0x020e025c
#define NASM_VER            "2.14.03rc2"
#endif /* NASM_VERSION_H */
